﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E ();
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8 ();
// 0x00000003 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_mB89E91246572F009281D79730950808F17C3F353 ();
// 0x00000004 System.Exception System.Linq.Error::NoMatch()
extern void Error_NoMatch_mA0FE78EC100066FA506B4C1C3AEC2E9E2DB79945 ();
// 0x00000005 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000007 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000008 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000009 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000A TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000B System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000C System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000D System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000E System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000F System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000010 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000011 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000012 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000013 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000014 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000015 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000016 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000017 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000018 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000019 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001A System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000001B System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000001C System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000001D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001E System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000001F System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000020 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000021 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000022 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000023 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000024 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000025 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000026 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000027 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000028 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000029 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000002A System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000002B System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000002C System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000002D System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000002E System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000002F System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000030 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000031 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000032 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000033 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000034 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000035 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000036 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000037 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000038 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000039 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x0000003A System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000003B System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000003C System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x0000003D System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x0000003E System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x0000003F System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000040 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000041 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x00000042 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x00000043 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000044 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[68] = 
{
	Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E,
	Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8,
	Error_NoElements_mB89E91246572F009281D79730950808F17C3F353,
	Error_NoMatch_mA0FE78EC100066FA506B4C1C3AEC2E9E2DB79945,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[68] = 
{
	0,
	4,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[17] = 
{
	{ 0x02000004, { 36, 4 } },
	{ 0x02000005, { 40, 9 } },
	{ 0x02000006, { 49, 7 } },
	{ 0x02000007, { 56, 10 } },
	{ 0x02000008, { 66, 1 } },
	{ 0x02000009, { 67, 21 } },
	{ 0x0200000B, { 88, 2 } },
	{ 0x06000005, { 0, 10 } },
	{ 0x06000006, { 10, 5 } },
	{ 0x06000007, { 15, 2 } },
	{ 0x06000008, { 17, 4 } },
	{ 0x06000009, { 21, 3 } },
	{ 0x0600000A, { 24, 3 } },
	{ 0x0600000B, { 27, 1 } },
	{ 0x0600000C, { 28, 3 } },
	{ 0x0600000D, { 31, 3 } },
	{ 0x0600000E, { 34, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[90] = 
{
	{ (Il2CppRGCTXDataType)2, 7776 },
	{ (Il2CppRGCTXDataType)3, 5815 },
	{ (Il2CppRGCTXDataType)2, 7777 },
	{ (Il2CppRGCTXDataType)2, 7778 },
	{ (Il2CppRGCTXDataType)3, 5816 },
	{ (Il2CppRGCTXDataType)2, 7779 },
	{ (Il2CppRGCTXDataType)2, 7780 },
	{ (Il2CppRGCTXDataType)3, 5817 },
	{ (Il2CppRGCTXDataType)2, 7781 },
	{ (Il2CppRGCTXDataType)3, 5818 },
	{ (Il2CppRGCTXDataType)2, 7782 },
	{ (Il2CppRGCTXDataType)3, 5819 },
	{ (Il2CppRGCTXDataType)3, 5820 },
	{ (Il2CppRGCTXDataType)2, 5005 },
	{ (Il2CppRGCTXDataType)3, 5821 },
	{ (Il2CppRGCTXDataType)2, 5009 },
	{ (Il2CppRGCTXDataType)3, 5822 },
	{ (Il2CppRGCTXDataType)2, 7783 },
	{ (Il2CppRGCTXDataType)2, 7784 },
	{ (Il2CppRGCTXDataType)2, 5010 },
	{ (Il2CppRGCTXDataType)2, 7785 },
	{ (Il2CppRGCTXDataType)2, 5012 },
	{ (Il2CppRGCTXDataType)2, 7786 },
	{ (Il2CppRGCTXDataType)3, 5823 },
	{ (Il2CppRGCTXDataType)2, 5015 },
	{ (Il2CppRGCTXDataType)2, 7787 },
	{ (Il2CppRGCTXDataType)3, 5824 },
	{ (Il2CppRGCTXDataType)2, 5018 },
	{ (Il2CppRGCTXDataType)2, 5020 },
	{ (Il2CppRGCTXDataType)2, 7788 },
	{ (Il2CppRGCTXDataType)3, 5825 },
	{ (Il2CppRGCTXDataType)2, 5023 },
	{ (Il2CppRGCTXDataType)2, 7789 },
	{ (Il2CppRGCTXDataType)3, 5826 },
	{ (Il2CppRGCTXDataType)2, 7790 },
	{ (Il2CppRGCTXDataType)2, 5026 },
	{ (Il2CppRGCTXDataType)3, 5827 },
	{ (Il2CppRGCTXDataType)3, 5828 },
	{ (Il2CppRGCTXDataType)2, 5030 },
	{ (Il2CppRGCTXDataType)3, 5829 },
	{ (Il2CppRGCTXDataType)3, 5830 },
	{ (Il2CppRGCTXDataType)2, 5039 },
	{ (Il2CppRGCTXDataType)2, 7791 },
	{ (Il2CppRGCTXDataType)3, 5831 },
	{ (Il2CppRGCTXDataType)3, 5832 },
	{ (Il2CppRGCTXDataType)2, 5041 },
	{ (Il2CppRGCTXDataType)2, 7704 },
	{ (Il2CppRGCTXDataType)3, 5833 },
	{ (Il2CppRGCTXDataType)3, 5834 },
	{ (Il2CppRGCTXDataType)3, 5835 },
	{ (Il2CppRGCTXDataType)2, 5048 },
	{ (Il2CppRGCTXDataType)2, 7792 },
	{ (Il2CppRGCTXDataType)3, 5836 },
	{ (Il2CppRGCTXDataType)3, 5837 },
	{ (Il2CppRGCTXDataType)3, 5500 },
	{ (Il2CppRGCTXDataType)3, 5838 },
	{ (Il2CppRGCTXDataType)3, 5839 },
	{ (Il2CppRGCTXDataType)2, 5057 },
	{ (Il2CppRGCTXDataType)2, 7793 },
	{ (Il2CppRGCTXDataType)3, 5840 },
	{ (Il2CppRGCTXDataType)3, 5841 },
	{ (Il2CppRGCTXDataType)3, 5842 },
	{ (Il2CppRGCTXDataType)3, 5843 },
	{ (Il2CppRGCTXDataType)3, 5844 },
	{ (Il2CppRGCTXDataType)3, 5506 },
	{ (Il2CppRGCTXDataType)3, 5845 },
	{ (Il2CppRGCTXDataType)3, 5846 },
	{ (Il2CppRGCTXDataType)3, 5847 },
	{ (Il2CppRGCTXDataType)2, 7794 },
	{ (Il2CppRGCTXDataType)3, 5848 },
	{ (Il2CppRGCTXDataType)3, 5849 },
	{ (Il2CppRGCTXDataType)3, 5850 },
	{ (Il2CppRGCTXDataType)2, 5071 },
	{ (Il2CppRGCTXDataType)3, 5851 },
	{ (Il2CppRGCTXDataType)3, 5852 },
	{ (Il2CppRGCTXDataType)2, 5074 },
	{ (Il2CppRGCTXDataType)3, 5853 },
	{ (Il2CppRGCTXDataType)1, 7795 },
	{ (Il2CppRGCTXDataType)2, 5073 },
	{ (Il2CppRGCTXDataType)3, 5854 },
	{ (Il2CppRGCTXDataType)1, 5073 },
	{ (Il2CppRGCTXDataType)1, 5071 },
	{ (Il2CppRGCTXDataType)2, 7796 },
	{ (Il2CppRGCTXDataType)2, 5073 },
	{ (Il2CppRGCTXDataType)3, 5855 },
	{ (Il2CppRGCTXDataType)3, 5856 },
	{ (Il2CppRGCTXDataType)3, 5857 },
	{ (Il2CppRGCTXDataType)2, 5072 },
	{ (Il2CppRGCTXDataType)3, 5858 },
	{ (Il2CppRGCTXDataType)2, 5085 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	68,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	17,
	s_rgctxIndices,
	90,
	s_rgctxValues,
	NULL,
};
