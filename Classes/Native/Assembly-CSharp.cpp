﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// Ball
struct Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924;
// BotAI
struct BotAI_t029234FCF3F2826C2123CD1BF8F65D212B156F80;
// Card
struct Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// UnityEngine.UI.FontData
struct FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738;
// GameManager
struct GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// InputManager
struct InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// Menu
struct Menu_t9BC67061F8954119BDB8CE5A0C0B6E1AA114C0D6;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// Player
struct Player_t5689617909B48F7640EA0892D85C92C13CC22C6F;
// PlayerController
struct PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9;
// PlayerData
struct PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5;
// Pocket
struct Pocket_t69E9E30897953A969E05E646946018B4DF9423D7;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD;
// Table
struct Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// BotAI/<>c
struct U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958;
// GameManager/<>c__DisplayClass30_0
struct U3CU3Ec__DisplayClass30_0_t9F9560A6034B2661D9C6E093FA21372992814025;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE;
// Table/<>c
struct U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38;
// System.Func`2<Ball,System.Boolean>
struct Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8;
// System.Collections.Generic.IEnumerable`1<Ball>
struct IEnumerable_1_t0F229514132C65EF3DC6C6A20CED0740F3FE2EE2;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.List`1<Ball>
struct List_1_t704724B962CF6005382C610663175BE693752D5D;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem>
struct List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t815A476B0A21E183042059E705F9E505478CD8AE;
// System.Collections.Generic.List`1<Pocket>
struct List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t34AA4AF4E7352129CA58045901530E41445AC16D;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t99B54448C695C6F7103A0DE4320F1A7EF7B30880;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,System.Int32Enum>
struct UnityAction_2_t808E43EBC9AA89CEA5830BD187EC213182A02B50;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906;
// UnityEngine.Events.UnityEvent`1<System.Single>
struct UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC;
// Ball[]
struct BallU5BU5D_t8B427CB36959E1DD7FA1CD925BB9655D131E899C;
// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// Card[]
struct CardU5BU5D_t32023BF72E2560CD1EA88DF7C04357E784A64CE0;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// UnityEngine.LineRenderer
struct LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// UnityEngine.MeshRenderer
struct MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// Pocket[]
struct PocketU5BU5D_tBA22525E227B67886467A1A06246191AF774978A;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C;
// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A;
// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535;
// UnityEngine.UI.Slider
struct Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F;
// UnityEngine.UI.Slider/SliderEvent
struct SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780;

IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t704724B962CF6005382C610663175BE693752D5D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass30_0_t9F9560A6034B2661D9C6E093FA21372992814025_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral53B6FFAF5394EC3DBC3422E88D0E40D256C996D6;
IL2CPP_EXTERN_C String_t* _stringLiteral730FB4333540EF71406C57A9DAA8464AE14157D3;
IL2CPP_EXTERN_C String_t* _stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E;
IL2CPP_EXTERN_C String_t* _stringLiteralA0BC275515E57D4FC52BBC80EB62131C416AA168;
IL2CPP_EXTERN_C String_t* _stringLiteralA9CA1FE109FB54B8BD267CFF6357F4284511A3CA;
IL2CPP_EXTERN_C String_t* _stringLiteralCBCC9AFE4096250DEAA855B0BAA98F52FE771F26;
IL2CPP_EXTERN_C const RuntimeMethod* BotAI_U3CCheckBallsU3Eb__8_1_m4176A9C096EA7AC51A35B69EA3BD9BDE3DBB9F84_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m564BDAF1415831A7751D8FFD48AEFAAEB60ACF9D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentsInChildren_TisCard_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A_m27B008E50F6CADD5BDD8F25A3D8D73709C8A1577_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_All_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_mD11F2F4E2660C09A1E8513BC3A3B926D37D22996_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Any_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_mB31A84E03C801D4A081D5A31CB5197BE99EE5B75_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Count_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m3F3E48596ECCC77DD571F837FE7C1AD0A9A43244_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_First_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m1AEA5E6A40B402B2518D20770F78C970461347D1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToList_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m8B14924409A741E59EEBD43F57AE8C533328E205_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Where_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m0D5793A8B17C8583062257D58FAB67E4EB4D15DD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m45DF0A38A6BF81DD15FFAE777BF93F6019D59E4D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m5FBC6AE392B5BDC7E57DA42B723E6819F1527B0D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m2503905BF86ABEB00CE962518451A3A95482500B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mF564EA6451E132F369252AD607F78B570253D320_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mB806EC855AC481BB6CB9DCF8A727E583A36F8817_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mC7B26CA370CBDEC8C71772629C62C5940FF5F825_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_2__ctor_m7141D4C33E399BD20EB8B17781BACB3B85C669C6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameManager_LevelLoaded_m8E33405B1DC924DFCA08EE0D40B8B1187FC0B5DB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m5A43FA83CABB71CFD6FA538507B7F1D460BA0A76_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_mE0F03A2E42E2F7F8A282AE01C12945F7379DC702_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m3049E16624476F2B193E0E3D8A77289A30598E05_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m389535AFAD3A7E50E8EBCC6C5AF2BEFBC4208E62_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_mE5E9D8A568D8853924165476644A9B523F98B554_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m50441437BE40ED72A3F37812FBDC7B631EDCE80D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mED876C00B350B9333120130A2EE72901165DA7CB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m2E8D9A40352859B1D5465D074732572FC364786E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_GetValueOrDefault_mE9A6BC0C579094139424C3A07555F05CFE4CA73B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1__ctor_mD66CEE733802251058E31FBEF0840C7A9D4DA406_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Nullable_1_get_HasValue_mCB87A6CB3CF78384E3F13C909FDB8AD3659DF6CD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m8F5F75D5872251B7E04B6D385D1BDD52DD9F3B5D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Player_CueHit_m5EB2329180A3544C0DC71D37CA3793D9FB446A97_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Player_CueMove_m99C0CC666F9E6AF125FC0866717BE649BA192961_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CCheckBallsU3Eb__8_0_mFB3E009B3F91CC5FF9957507C4FF2A7A82EC8953_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CCheckStateU3Eb__15_0_m21D38CEC1B329681D58C6BE3A0472190A631BD12_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CCheckStateU3Eb__15_1_m675B68772F4C3E060417EEA5746E1FEF51D99786_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CCheckStateU3Eb__15_2_m92301D8863096DB4E7559DA3BE989C24B09E6463_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass30_0_U3CRegisterBallU3Eb__0_mFD130C2AF37A75EA852CB27234BAE17B629F60C9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityAction_1__ctor_m8CACADCAC18230FB18DF7A6BEC3D9EAD93FEDC3B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityAction_2__ctor_mE0417B33CF845A6B9324E67D296ADEA562B91DE0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_AddListener_mA73838FBF3836695F5183B32B797E9499BA5E59C_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t Ball_Start_m218869060D38F7477CC98EBB540C6015DB639629_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t BotAI_CheckBalls_mCE475ACCFD1A63095C82F4106233D52E16371AB6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t BotAI_FixedUpdate_m73C76A9986C5874E7B70D34BC5561DFD5B0FAA89_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t BotAI_OnEnable_m1B28DC499C4A75541B0CFC34770D35B2AD35CCD9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Card_Click_mD38F1C9958610FFD5506DE06BAA9FF9995F22AE1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameManager_ChangePlayer_m0F30608B17D39CD119A0AC533BF3064E24D79BEF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameManager_EndGame_m34CB0E063C72D2D7BA815B5397C5DB865EE60810_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965AssemblyU2DCSharp_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameManager_RegisterBall_m33C55F5DAF74AF3A685FC4EC267C3CC4280A5F74_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameManager_ReturnToMenu_m361E877417F8294B9B32A6B0A27FE6EF1E77FBB6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameManager_get_Clock_m32723273A7CCC5919901AC0AA016C8F90E90A5D6AssemblyU2DCSharp_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameManager_get_Clock_m32723273A7CCC5919901AC0AA016C8F90E90A5D6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t GameManager_set_Clock_m6A52D50A47B356DE092A46E16D85258DAA4338C2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InputManager_Start_m99C6C0D9277906D13076BCD14E7BE2DDAF88FF08_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t InputManager_Update_mE17FD2A03E0E1BE94DFE0B0AB4B5B9C5F3EA285B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Menu_StartGame_m6B2E9CA9AE74433EA78DE12BADAAFA800B15DFC0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Player_CueHit_m5EB2329180A3544C0DC71D37CA3793D9FB446A97_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Player_CueRotate_m531643FEA219030BAD2FE999897D386DBD49FDD1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Pocket_OnTriggerEnter_m01FB6A867212759966A839FC3CE069882894D426_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Table_Awake_mDBD0366C97BE96CB92CDC3B4690D365FD4A24081_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Table_CheckState_mDF18708BD88BB608A70BF3CD22B86CF4BC0DF817_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Table_FixedUpdate_m7A17D40FF64C69746F30A6F2C540C58073B3FD07_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Table_RemoveBall_m02A9A5675D99552809A1869FF89AFE3E0FC9C6DE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__cctor_m67E201652407DD4E9631314EF20EF9D730F0301D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__cctor_mEE715A42F22ECC72A35F83EB838087ABC851CE7A_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct CardU5BU5D_t32023BF72E2560CD1EA88DF7C04357E784A64CE0;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// System.Object


// BotAI_<>c
struct  U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD_StaticFields
{
public:
	// BotAI_<>c BotAI_<>c::<>9
	U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD * ___U3CU3E9_0;
	// System.Func`2<Ball,System.Boolean> BotAI_<>c::<>9__8_0
	Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * ___U3CU3E9__8_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD_StaticFields, ___U3CU3E9__8_0_1)); }
	inline Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * get_U3CU3E9__8_0_1() const { return ___U3CU3E9__8_0_1; }
	inline Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 ** get_address_of_U3CU3E9__8_0_1() { return &___U3CU3E9__8_0_1; }
	inline void set_U3CU3E9__8_0_1(Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * value)
	{
		___U3CU3E9__8_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__8_0_1), (void*)value);
	}
};


// GameManager_<>c__DisplayClass30_0
struct  U3CU3Ec__DisplayClass30_0_t9F9560A6034B2661D9C6E093FA21372992814025  : public RuntimeObject
{
public:
	// System.Int32 GameManager_<>c__DisplayClass30_0::i
	int32_t ___i_0;

public:
	inline static int32_t get_offset_of_i_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t9F9560A6034B2661D9C6E093FA21372992814025, ___i_0)); }
	inline int32_t get_i_0() const { return ___i_0; }
	inline int32_t* get_address_of_i_0() { return &___i_0; }
	inline void set_i_0(int32_t value)
	{
		___i_0 = value;
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.List`1<Ball>
struct  List_1_t704724B962CF6005382C610663175BE693752D5D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	BallU5BU5D_t8B427CB36959E1DD7FA1CD925BB9655D131E899C* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t704724B962CF6005382C610663175BE693752D5D, ____items_1)); }
	inline BallU5BU5D_t8B427CB36959E1DD7FA1CD925BB9655D131E899C* get__items_1() const { return ____items_1; }
	inline BallU5BU5D_t8B427CB36959E1DD7FA1CD925BB9655D131E899C** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(BallU5BU5D_t8B427CB36959E1DD7FA1CD925BB9655D131E899C* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t704724B962CF6005382C610663175BE693752D5D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t704724B962CF6005382C610663175BE693752D5D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t704724B962CF6005382C610663175BE693752D5D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t704724B962CF6005382C610663175BE693752D5D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	BallU5BU5D_t8B427CB36959E1DD7FA1CD925BB9655D131E899C* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t704724B962CF6005382C610663175BE693752D5D_StaticFields, ____emptyArray_5)); }
	inline BallU5BU5D_t8B427CB36959E1DD7FA1CD925BB9655D131E899C* get__emptyArray_5() const { return ____emptyArray_5; }
	inline BallU5BU5D_t8B427CB36959E1DD7FA1CD925BB9655D131E899C** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(BallU5BU5D_t8B427CB36959E1DD7FA1CD925BB9655D131E899C* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Pocket>
struct  List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PocketU5BU5D_tBA22525E227B67886467A1A06246191AF774978A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2, ____items_1)); }
	inline PocketU5BU5D_tBA22525E227B67886467A1A06246191AF774978A* get__items_1() const { return ____items_1; }
	inline PocketU5BU5D_tBA22525E227B67886467A1A06246191AF774978A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PocketU5BU5D_tBA22525E227B67886467A1A06246191AF774978A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	PocketU5BU5D_tBA22525E227B67886467A1A06246191AF774978A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2_StaticFields, ____emptyArray_5)); }
	inline PocketU5BU5D_tBA22525E227B67886467A1A06246191AF774978A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline PocketU5BU5D_tBA22525E227B67886467A1A06246191AF774978A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(PocketU5BU5D_tBA22525E227B67886467A1A06246191AF774978A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct  List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____items_1)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_StaticFields, ____emptyArray_5)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get__emptyArray_5() const { return ____emptyArray_5; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct  List_1_t99B54448C695C6F7103A0DE4320F1A7EF7B30880  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t99B54448C695C6F7103A0DE4320F1A7EF7B30880, ____items_1)); }
	inline SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* get__items_1() const { return ____items_1; }
	inline SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t99B54448C695C6F7103A0DE4320F1A7EF7B30880, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t99B54448C695C6F7103A0DE4320F1A7EF7B30880, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t99B54448C695C6F7103A0DE4320F1A7EF7B30880, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t99B54448C695C6F7103A0DE4320F1A7EF7B30880_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t99B54448C695C6F7103A0DE4320F1A7EF7B30880_StaticFields, ____emptyArray_5)); }
	inline SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* get__emptyArray_5() const { return ____emptyArray_5; }
	inline SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(SpriteU5BU5D_t8DB77E112FFC97B722E701189DCB4059F943FD77* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____items_1)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_StaticFields, ____emptyArray_5)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// Table_<>c
struct  U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_StaticFields
{
public:
	// Table_<>c Table_<>c::<>9
	U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B * ___U3CU3E9_0;
	// System.Func`2<Ball,System.Boolean> Table_<>c::<>9__15_0
	Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * ___U3CU3E9__15_0_1;
	// System.Func`2<Ball,System.Boolean> Table_<>c::<>9__15_1
	Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * ___U3CU3E9__15_1_2;
	// System.Func`2<Ball,System.Boolean> Table_<>c::<>9__15_2
	Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * ___U3CU3E9__15_2_3;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_StaticFields, ___U3CU3E9__15_0_1)); }
	inline Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * get_U3CU3E9__15_0_1() const { return ___U3CU3E9__15_0_1; }
	inline Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 ** get_address_of_U3CU3E9__15_0_1() { return &___U3CU3E9__15_0_1; }
	inline void set_U3CU3E9__15_0_1(Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * value)
	{
		___U3CU3E9__15_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__15_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_StaticFields, ___U3CU3E9__15_1_2)); }
	inline Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * get_U3CU3E9__15_1_2() const { return ___U3CU3E9__15_1_2; }
	inline Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 ** get_address_of_U3CU3E9__15_1_2() { return &___U3CU3E9__15_1_2; }
	inline void set_U3CU3E9__15_1_2(Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * value)
	{
		___U3CU3E9__15_1_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__15_1_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_StaticFields, ___U3CU3E9__15_2_3)); }
	inline Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * get_U3CU3E9__15_2_3() const { return ___U3CU3E9__15_2_3; }
	inline Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 ** get_address_of_U3CU3E9__15_2_3() { return &___U3CU3E9__15_2_3; }
	inline void set_U3CU3E9__15_2_3(Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * value)
	{
		___U3CU3E9__15_2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__15_2_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<Ball>
struct  Enumerator_tD4E53BC75AF1AB2047F41AD591569A06B2EFC316 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t704724B962CF6005382C610663175BE693752D5D * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tD4E53BC75AF1AB2047F41AD591569A06B2EFC316, ___list_0)); }
	inline List_1_t704724B962CF6005382C610663175BE693752D5D * get_list_0() const { return ___list_0; }
	inline List_1_t704724B962CF6005382C610663175BE693752D5D ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t704724B962CF6005382C610663175BE693752D5D * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tD4E53BC75AF1AB2047F41AD591569A06B2EFC316, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tD4E53BC75AF1AB2047F41AD591569A06B2EFC316, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tD4E53BC75AF1AB2047F41AD591569A06B2EFC316, ___current_3)); }
	inline Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * get_current_3() const { return ___current_3; }
	inline Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<Pocket>
struct  Enumerator_t078AC40DBA49748835454B10BFD699FDDE55CB4C 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t078AC40DBA49748835454B10BFD699FDDE55CB4C, ___list_0)); }
	inline List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2 * get_list_0() const { return ___list_0; }
	inline List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t078AC40DBA49748835454B10BFD699FDDE55CB4C, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t078AC40DBA49748835454B10BFD699FDDE55CB4C, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t078AC40DBA49748835454B10BFD699FDDE55CB4C, ___current_3)); }
	inline Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 * get_current_3() const { return ___current_3; }
	inline Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Object>
struct  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<UnityEngine.GameObject>
struct  Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14, ___list_0)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_list_0() const { return ___list_0; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14, ___current_3)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_current_3() const { return ___current_3; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.Color
struct  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2__padding[1];
	};

public:
};


// UnityEngine.Events.UnityEvent
struct  UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.SceneManagement.Scene
struct  Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};


// UnityEngine.UI.SpriteState
struct  SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_HighlightedSprite_0)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_PressedSprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_SelectedSprite_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_DisabledSprite_3)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_pinvoke
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_com
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};

// UnityEngine.Vector2
struct  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct  Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// BallsType
struct  BallsType_tA18C03BE35D090078C176DA3A1C05988D51BDABB 
{
public:
	// System.Int32 BallsType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BallsType_tA18C03BE35D090078C176DA3A1C05988D51BDABB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GameState
struct  GameState_t220CA73AF42CC54408408696DBFFAB424F309FAE 
{
public:
	// System.Int32 GameState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GameState_t220CA73AF42CC54408408696DBFFAB424F309FAE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Int32Enum
struct  Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ForceMode
struct  ForceMode_t7778317A4C46140D50D98811D65A7B22E38163D5 
{
public:
	// System.Int32 UnityEngine.ForceMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ForceMode_t7778317A4C46140D50D98811D65A7B22E38163D5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Ray
struct  Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6, ___m_Origin_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6, ___m_Direction_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Direction_1 = value;
	}
};


// UnityEngine.RaycastHit
struct  RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Point_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Normal_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_UV_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// UnityEngine.SceneManagement.LoadSceneMode
struct  LoadSceneMode_tF5060E18B71D524860ECBF7B9B56193B1907E5CC 
{
public:
	// System.Int32 UnityEngine.SceneManagement.LoadSceneMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoadSceneMode_tF5060E18B71D524860ECBF7B9B56193B1907E5CC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TouchPhase
struct  TouchPhase_tB52B8A497547FB9575DE7975D13AC7D64C3A958A 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchPhase_tB52B8A497547FB9575DE7975D13AC7D64C3A958A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TouchType
struct  TouchType_t2EF726465ABD45681A6686BAC426814AA087C20F 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchType_t2EF726465ABD45681A6686BAC426814AA087C20F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Button_ButtonClickedEvent
struct  ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F  : public UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4
{
public:

public:
};


// UnityEngine.UI.ColorBlock
struct  ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_NormalColor_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_HighlightedColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_PressedColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_SelectedColor_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_DisabledColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};


// UnityEngine.UI.Image_FillMethod
struct  FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2 
{
public:
	// System.Int32 UnityEngine.UI.Image_FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image_Type
struct  Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12 
{
public:
	// System.Int32 UnityEngine.UI.Image_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Navigation_Mode
struct  Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F 
{
public:
	// System.Int32 UnityEngine.UI.Navigation_Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable_Transition
struct  Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14 
{
public:
	// System.Int32 UnityEngine.UI.Selectable_Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Slider_Direction
struct  Direction_tFC329DCFF9844C052301C90100CA0F5FA9C65961 
{
public:
	// System.Int32 UnityEngine.UI.Slider_Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_tFC329DCFF9844C052301C90100CA0F5FA9C65961, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Slider_SliderEvent
struct  SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780  : public UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC
{
public:

public:
};


// Player
struct  Player_t5689617909B48F7640EA0892D85C92C13CC22C6F  : public RuntimeObject
{
public:
	// PlayerData Player::data
	PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * ___data_0;
	// PlayerController Player::playerController
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * ___playerController_1;
	// BallsType Player::ballsType
	int32_t ___ballsType_2;
	// System.Single Player::forceMul
	float ___forceMul_3;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___data_0)); }
	inline PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * get_data_0() const { return ___data_0; }
	inline PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_0), (void*)value);
	}

	inline static int32_t get_offset_of_playerController_1() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___playerController_1)); }
	inline PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * get_playerController_1() const { return ___playerController_1; }
	inline PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 ** get_address_of_playerController_1() { return &___playerController_1; }
	inline void set_playerController_1(PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * value)
	{
		___playerController_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerController_1), (void*)value);
	}

	inline static int32_t get_offset_of_ballsType_2() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___ballsType_2)); }
	inline int32_t get_ballsType_2() const { return ___ballsType_2; }
	inline int32_t* get_address_of_ballsType_2() { return &___ballsType_2; }
	inline void set_ballsType_2(int32_t value)
	{
		___ballsType_2 = value;
	}

	inline static int32_t get_offset_of_forceMul_3() { return static_cast<int32_t>(offsetof(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F, ___forceMul_3)); }
	inline float get_forceMul_3() const { return ___forceMul_3; }
	inline float* get_address_of_forceMul_3() { return &___forceMul_3; }
	inline void set_forceMul_3(float value)
	{
		___forceMul_3 = value;
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.Nullable`1<BallsType>
struct  Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Nullable`1<System.Int32Enum>
struct  Nullable_1_t64244F99361E39CBE565C5E89436C898F18DF5DC 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t64244F99361E39CBE565C5E89436C898F18DF5DC, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t64244F99361E39CBE565C5E89436C898F18DF5DC, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Material
struct  Material_t8927C00353A72755313F046D0CE85178AE8218EE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.ScriptableObject
struct  ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// UnityEngine.Sprite
struct  Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Touch
struct  Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_Position_1)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_RawPosition_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_PositionDelta_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};


// UnityEngine.UI.Navigation
struct  Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A 
{
public:
	// UnityEngine.UI.Navigation_Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnUp_1)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnDown_2)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnLeft_3)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnRight_4)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_4), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_4;
};

// PlayerData
struct  PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.String PlayerData::Nickname
	String_t* ___Nickname_4;
	// UnityEngine.Sprite PlayerData::avatar
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___avatar_5;
	// System.Int32 PlayerData::Force
	int32_t ___Force_6;
	// System.Int32 PlayerData::Aim
	int32_t ___Aim_7;
	// System.Int32 PlayerData::Spin
	int32_t ___Spin_8;
	// System.Int32 PlayerData::Time
	int32_t ___Time_9;

public:
	inline static int32_t get_offset_of_Nickname_4() { return static_cast<int32_t>(offsetof(PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5, ___Nickname_4)); }
	inline String_t* get_Nickname_4() const { return ___Nickname_4; }
	inline String_t** get_address_of_Nickname_4() { return &___Nickname_4; }
	inline void set_Nickname_4(String_t* value)
	{
		___Nickname_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Nickname_4), (void*)value);
	}

	inline static int32_t get_offset_of_avatar_5() { return static_cast<int32_t>(offsetof(PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5, ___avatar_5)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_avatar_5() const { return ___avatar_5; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_avatar_5() { return &___avatar_5; }
	inline void set_avatar_5(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___avatar_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___avatar_5), (void*)value);
	}

	inline static int32_t get_offset_of_Force_6() { return static_cast<int32_t>(offsetof(PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5, ___Force_6)); }
	inline int32_t get_Force_6() const { return ___Force_6; }
	inline int32_t* get_address_of_Force_6() { return &___Force_6; }
	inline void set_Force_6(int32_t value)
	{
		___Force_6 = value;
	}

	inline static int32_t get_offset_of_Aim_7() { return static_cast<int32_t>(offsetof(PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5, ___Aim_7)); }
	inline int32_t get_Aim_7() const { return ___Aim_7; }
	inline int32_t* get_address_of_Aim_7() { return &___Aim_7; }
	inline void set_Aim_7(int32_t value)
	{
		___Aim_7 = value;
	}

	inline static int32_t get_offset_of_Spin_8() { return static_cast<int32_t>(offsetof(PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5, ___Spin_8)); }
	inline int32_t get_Spin_8() const { return ___Spin_8; }
	inline int32_t* get_address_of_Spin_8() { return &___Spin_8; }
	inline void set_Spin_8(int32_t value)
	{
		___Spin_8 = value;
	}

	inline static int32_t get_offset_of_Time_9() { return static_cast<int32_t>(offsetof(PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5, ___Time_9)); }
	inline int32_t get_Time_9() const { return ___Time_9; }
	inline int32_t* get_address_of_Time_9() { return &___Time_9; }
	inline void set_Time_9(int32_t value)
	{
		___Time_9 = value;
	}
};


// System.Func`2<Ball,System.Boolean>
struct  Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Collider
struct  Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Events.UnityAction
struct  UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.Single>
struct  UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct  UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Renderer
struct  Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Rigidbody
struct  Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Camera
struct  Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields
{
public:
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreCull_4;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreRender_5;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.Canvas
struct  Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_StaticFields
{
public:
	// UnityEngine.Canvas_WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * ___willRenderCanvases_4;

public:
	inline static int32_t get_offset_of_willRenderCanvases_4() { return static_cast<int32_t>(offsetof(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_StaticFields, ___willRenderCanvases_4)); }
	inline WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * get_willRenderCanvases_4() const { return ___willRenderCanvases_4; }
	inline WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 ** get_address_of_willRenderCanvases_4() { return &___willRenderCanvases_4; }
	inline void set_willRenderCanvases_4(WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * value)
	{
		___willRenderCanvases_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___willRenderCanvases_4), (void*)value);
	}
};


// UnityEngine.LineRenderer
struct  LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// UnityEngine.MeshRenderer
struct  MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.RectTransform
struct  RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072  : public Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1
{
public:

public:
};

struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields
{
public:
	// UnityEngine.RectTransform_ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reapplyDrivenProperties_4), (void*)value);
	}
};


// Ball
struct  Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 Ball::Number
	int32_t ___Number_4;
	// UnityEngine.Rigidbody Ball::Rigidbody
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___Rigidbody_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Ball::icons
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___icons_6;

public:
	inline static int32_t get_offset_of_Number_4() { return static_cast<int32_t>(offsetof(Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D, ___Number_4)); }
	inline int32_t get_Number_4() const { return ___Number_4; }
	inline int32_t* get_address_of_Number_4() { return &___Number_4; }
	inline void set_Number_4(int32_t value)
	{
		___Number_4 = value;
	}

	inline static int32_t get_offset_of_Rigidbody_5() { return static_cast<int32_t>(offsetof(Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D, ___Rigidbody_5)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_Rigidbody_5() const { return ___Rigidbody_5; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_Rigidbody_5() { return &___Rigidbody_5; }
	inline void set_Rigidbody_5(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___Rigidbody_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Rigidbody_5), (void*)value);
	}

	inline static int32_t get_offset_of_icons_6() { return static_cast<int32_t>(offsetof(Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D, ___icons_6)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_icons_6() const { return ___icons_6; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_icons_6() { return &___icons_6; }
	inline void set_icons_6(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___icons_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___icons_6), (void*)value);
	}
};


// Card
struct  Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// PlayerData Card::data
	PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * ___data_4;
	// System.Boolean Card::cue
	bool ___cue_5;
	// UnityEngine.UI.Text Card::title
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___title_6;
	// UnityEngine.UI.Image Card::force
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___force_7;
	// UnityEngine.UI.Image Card::aim
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___aim_8;
	// UnityEngine.UI.Image Card::spin
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___spin_9;
	// UnityEngine.UI.Image Card::time
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___time_10;
	// UnityEngine.UI.Image Card::icon
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___icon_11;

public:
	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A, ___data_4)); }
	inline PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * get_data_4() const { return ___data_4; }
	inline PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 ** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_4), (void*)value);
	}

	inline static int32_t get_offset_of_cue_5() { return static_cast<int32_t>(offsetof(Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A, ___cue_5)); }
	inline bool get_cue_5() const { return ___cue_5; }
	inline bool* get_address_of_cue_5() { return &___cue_5; }
	inline void set_cue_5(bool value)
	{
		___cue_5 = value;
	}

	inline static int32_t get_offset_of_title_6() { return static_cast<int32_t>(offsetof(Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A, ___title_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_title_6() const { return ___title_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_title_6() { return &___title_6; }
	inline void set_title_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___title_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___title_6), (void*)value);
	}

	inline static int32_t get_offset_of_force_7() { return static_cast<int32_t>(offsetof(Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A, ___force_7)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_force_7() const { return ___force_7; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_force_7() { return &___force_7; }
	inline void set_force_7(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___force_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___force_7), (void*)value);
	}

	inline static int32_t get_offset_of_aim_8() { return static_cast<int32_t>(offsetof(Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A, ___aim_8)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_aim_8() const { return ___aim_8; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_aim_8() { return &___aim_8; }
	inline void set_aim_8(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___aim_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___aim_8), (void*)value);
	}

	inline static int32_t get_offset_of_spin_9() { return static_cast<int32_t>(offsetof(Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A, ___spin_9)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_spin_9() const { return ___spin_9; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_spin_9() { return &___spin_9; }
	inline void set_spin_9(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___spin_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spin_9), (void*)value);
	}

	inline static int32_t get_offset_of_time_10() { return static_cast<int32_t>(offsetof(Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A, ___time_10)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_time_10() const { return ___time_10; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_time_10() { return &___time_10; }
	inline void set_time_10(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___time_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___time_10), (void*)value);
	}

	inline static int32_t get_offset_of_icon_11() { return static_cast<int32_t>(offsetof(Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A, ___icon_11)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_icon_11() const { return ___icon_11; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_icon_11() { return &___icon_11; }
	inline void set_icon_11(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___icon_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___icon_11), (void*)value);
	}
};


// GameManager
struct  GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single GameManager::Sensitivity
	float ___Sensitivity_7;
	// System.Single GameManager::BaseForce
	float ___BaseForce_8;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> GameManager::ballIcons
	List_1_t99B54448C695C6F7103A0DE4320F1A7EF7B30880 * ___ballIcons_9;
	// UnityEngine.UI.Image GameManager::ballPrefab
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___ballPrefab_10;
	// UnityEngine.Canvas GameManager::hud
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___hud_11;
	// UnityEngine.RectTransform GameManager::deckA
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___deckA_12;
	// UnityEngine.RectTransform GameManager::deckB
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___deckB_13;
	// UnityEngine.UI.Slider GameManager::slider
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * ___slider_14;
	// UnityEngine.UI.Button GameManager::clickHit
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___clickHit_15;
	// UnityEngine.UI.Button GameManager::clickReload
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___clickReload_16;
	// UnityEngine.RectTransform GameManager::goPanel
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___goPanel_17;
	// UnityEngine.UI.Text GameManager::goText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___goText_18;
	// UnityEngine.UI.Text GameManager::clockText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___clockText_19;
	// UnityEngine.UI.Text GameManager::debugText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___debugText_20;
	// UnityEngine.UI.Image GameManager::localAvatar
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___localAvatar_21;
	// UnityEngine.UI.Image GameManager::remoteAvatar
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___remoteAvatar_22;
	// Table GameManager::currentTable
	Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * ___currentTable_23;
	// Player GameManager::localPlayer
	Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * ___localPlayer_24;
	// Player GameManager::remotePlayer
	Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * ___remotePlayer_25;
	// Player GameManager::currentPlayer
	Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * ___currentPlayer_26;

public:
	inline static int32_t get_offset_of_Sensitivity_7() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___Sensitivity_7)); }
	inline float get_Sensitivity_7() const { return ___Sensitivity_7; }
	inline float* get_address_of_Sensitivity_7() { return &___Sensitivity_7; }
	inline void set_Sensitivity_7(float value)
	{
		___Sensitivity_7 = value;
	}

	inline static int32_t get_offset_of_BaseForce_8() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___BaseForce_8)); }
	inline float get_BaseForce_8() const { return ___BaseForce_8; }
	inline float* get_address_of_BaseForce_8() { return &___BaseForce_8; }
	inline void set_BaseForce_8(float value)
	{
		___BaseForce_8 = value;
	}

	inline static int32_t get_offset_of_ballIcons_9() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___ballIcons_9)); }
	inline List_1_t99B54448C695C6F7103A0DE4320F1A7EF7B30880 * get_ballIcons_9() const { return ___ballIcons_9; }
	inline List_1_t99B54448C695C6F7103A0DE4320F1A7EF7B30880 ** get_address_of_ballIcons_9() { return &___ballIcons_9; }
	inline void set_ballIcons_9(List_1_t99B54448C695C6F7103A0DE4320F1A7EF7B30880 * value)
	{
		___ballIcons_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ballIcons_9), (void*)value);
	}

	inline static int32_t get_offset_of_ballPrefab_10() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___ballPrefab_10)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_ballPrefab_10() const { return ___ballPrefab_10; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_ballPrefab_10() { return &___ballPrefab_10; }
	inline void set_ballPrefab_10(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___ballPrefab_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ballPrefab_10), (void*)value);
	}

	inline static int32_t get_offset_of_hud_11() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___hud_11)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_hud_11() const { return ___hud_11; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_hud_11() { return &___hud_11; }
	inline void set_hud_11(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___hud_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hud_11), (void*)value);
	}

	inline static int32_t get_offset_of_deckA_12() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___deckA_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_deckA_12() const { return ___deckA_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_deckA_12() { return &___deckA_12; }
	inline void set_deckA_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___deckA_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deckA_12), (void*)value);
	}

	inline static int32_t get_offset_of_deckB_13() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___deckB_13)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_deckB_13() const { return ___deckB_13; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_deckB_13() { return &___deckB_13; }
	inline void set_deckB_13(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___deckB_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deckB_13), (void*)value);
	}

	inline static int32_t get_offset_of_slider_14() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___slider_14)); }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * get_slider_14() const { return ___slider_14; }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A ** get_address_of_slider_14() { return &___slider_14; }
	inline void set_slider_14(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * value)
	{
		___slider_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___slider_14), (void*)value);
	}

	inline static int32_t get_offset_of_clickHit_15() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___clickHit_15)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_clickHit_15() const { return ___clickHit_15; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_clickHit_15() { return &___clickHit_15; }
	inline void set_clickHit_15(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___clickHit_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clickHit_15), (void*)value);
	}

	inline static int32_t get_offset_of_clickReload_16() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___clickReload_16)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_clickReload_16() const { return ___clickReload_16; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_clickReload_16() { return &___clickReload_16; }
	inline void set_clickReload_16(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___clickReload_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clickReload_16), (void*)value);
	}

	inline static int32_t get_offset_of_goPanel_17() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___goPanel_17)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_goPanel_17() const { return ___goPanel_17; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_goPanel_17() { return &___goPanel_17; }
	inline void set_goPanel_17(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___goPanel_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___goPanel_17), (void*)value);
	}

	inline static int32_t get_offset_of_goText_18() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___goText_18)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_goText_18() const { return ___goText_18; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_goText_18() { return &___goText_18; }
	inline void set_goText_18(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___goText_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___goText_18), (void*)value);
	}

	inline static int32_t get_offset_of_clockText_19() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___clockText_19)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_clockText_19() const { return ___clockText_19; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_clockText_19() { return &___clockText_19; }
	inline void set_clockText_19(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___clockText_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clockText_19), (void*)value);
	}

	inline static int32_t get_offset_of_debugText_20() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___debugText_20)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_debugText_20() const { return ___debugText_20; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_debugText_20() { return &___debugText_20; }
	inline void set_debugText_20(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___debugText_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___debugText_20), (void*)value);
	}

	inline static int32_t get_offset_of_localAvatar_21() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___localAvatar_21)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_localAvatar_21() const { return ___localAvatar_21; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_localAvatar_21() { return &___localAvatar_21; }
	inline void set_localAvatar_21(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___localAvatar_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___localAvatar_21), (void*)value);
	}

	inline static int32_t get_offset_of_remoteAvatar_22() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___remoteAvatar_22)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_remoteAvatar_22() const { return ___remoteAvatar_22; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_remoteAvatar_22() { return &___remoteAvatar_22; }
	inline void set_remoteAvatar_22(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___remoteAvatar_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___remoteAvatar_22), (void*)value);
	}

	inline static int32_t get_offset_of_currentTable_23() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___currentTable_23)); }
	inline Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * get_currentTable_23() const { return ___currentTable_23; }
	inline Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 ** get_address_of_currentTable_23() { return &___currentTable_23; }
	inline void set_currentTable_23(Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * value)
	{
		___currentTable_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentTable_23), (void*)value);
	}

	inline static int32_t get_offset_of_localPlayer_24() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___localPlayer_24)); }
	inline Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * get_localPlayer_24() const { return ___localPlayer_24; }
	inline Player_t5689617909B48F7640EA0892D85C92C13CC22C6F ** get_address_of_localPlayer_24() { return &___localPlayer_24; }
	inline void set_localPlayer_24(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * value)
	{
		___localPlayer_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___localPlayer_24), (void*)value);
	}

	inline static int32_t get_offset_of_remotePlayer_25() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___remotePlayer_25)); }
	inline Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * get_remotePlayer_25() const { return ___remotePlayer_25; }
	inline Player_t5689617909B48F7640EA0892D85C92C13CC22C6F ** get_address_of_remotePlayer_25() { return &___remotePlayer_25; }
	inline void set_remotePlayer_25(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * value)
	{
		___remotePlayer_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___remotePlayer_25), (void*)value);
	}

	inline static int32_t get_offset_of_currentPlayer_26() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1, ___currentPlayer_26)); }
	inline Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * get_currentPlayer_26() const { return ___currentPlayer_26; }
	inline Player_t5689617909B48F7640EA0892D85C92C13CC22C6F ** get_address_of_currentPlayer_26() { return &___currentPlayer_26; }
	inline void set_currentPlayer_26(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * value)
	{
		___currentPlayer_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentPlayer_26), (void*)value);
	}
};

struct GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields
{
public:
	// GameManager GameManager::_instance
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * ____instance_4;
	// GameState GameManager::state
	int32_t ___state_5;
	// System.Single GameManager::_timer
	float ____timer_6;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields, ____instance_4)); }
	inline GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * get__instance_4() const { return ____instance_4; }
	inline GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_4), (void*)value);
	}

	inline static int32_t get_offset_of_state_5() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields, ___state_5)); }
	inline int32_t get_state_5() const { return ___state_5; }
	inline int32_t* get_address_of_state_5() { return &___state_5; }
	inline void set_state_5(int32_t value)
	{
		___state_5 = value;
	}

	inline static int32_t get_offset_of__timer_6() { return static_cast<int32_t>(offsetof(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields, ____timer_6)); }
	inline float get__timer_6() const { return ____timer_6; }
	inline float* get_address_of__timer_6() { return &____timer_6; }
	inline void set__timer_6(float value)
	{
		____timer_6 = value;
	}
};


// Menu
struct  Menu_t9BC67061F8954119BDB8CE5A0C0B6E1AA114C0D6  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.RectTransform Menu::bots
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___bots_4;
	// UnityEngine.RectTransform Menu::cues
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___cues_5;

public:
	inline static int32_t get_offset_of_bots_4() { return static_cast<int32_t>(offsetof(Menu_t9BC67061F8954119BDB8CE5A0C0B6E1AA114C0D6, ___bots_4)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_bots_4() const { return ___bots_4; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_bots_4() { return &___bots_4; }
	inline void set_bots_4(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___bots_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bots_4), (void*)value);
	}

	inline static int32_t get_offset_of_cues_5() { return static_cast<int32_t>(offsetof(Menu_t9BC67061F8954119BDB8CE5A0C0B6E1AA114C0D6, ___cues_5)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_cues_5() const { return ___cues_5; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_cues_5() { return &___cues_5; }
	inline void set_cues_5(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___cues_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cues_5), (void*)value);
	}
};


// PlayerController
struct  PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Player PlayerController::_player
	Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * ____player_4;

public:
	inline static int32_t get_offset_of__player_4() { return static_cast<int32_t>(offsetof(PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9, ____player_4)); }
	inline Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * get__player_4() const { return ____player_4; }
	inline Player_t5689617909B48F7640EA0892D85C92C13CC22C6F ** get_address_of__player_4() { return &____player_4; }
	inline void set__player_4(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * value)
	{
		____player_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____player_4), (void*)value);
	}
};


// Pocket
struct  Pocket_t69E9E30897953A969E05E646946018B4DF9423D7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Table
struct  Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.RaycastHit Table::hit
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  ___hit_4;
	// UnityEngine.LineRenderer Table::phantomTracer
	LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * ___phantomTracer_5;
	// UnityEngine.LineRenderer Table::reflectionTracer
	LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * ___reflectionTracer_6;
	// UnityEngine.Transform Table::hitPoint
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___hitPoint_7;
	// UnityEngine.MeshRenderer Table::cueMesh
	MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * ___cueMesh_8;
	// Ball Table::CueBall
	Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * ___CueBall_9;
	// UnityEngine.Transform Table::phantom
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___phantom_10;
	// System.Collections.Generic.List`1<Ball> Table::Balls
	List_1_t704724B962CF6005382C610663175BE693752D5D * ___Balls_11;
	// System.Collections.Generic.List`1<Pocket> Table::pockets
	List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2 * ___pockets_12;
	// UnityEngine.Transform Table::deck
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___deck_13;
	// System.Boolean Table::canContinue
	bool ___canContinue_14;
	// UnityEngine.Material Table::phFriend
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___phFriend_15;
	// UnityEngine.Material Table::phFoe
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___phFoe_16;

public:
	inline static int32_t get_offset_of_hit_4() { return static_cast<int32_t>(offsetof(Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152, ___hit_4)); }
	inline RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  get_hit_4() const { return ___hit_4; }
	inline RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * get_address_of_hit_4() { return &___hit_4; }
	inline void set_hit_4(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  value)
	{
		___hit_4 = value;
	}

	inline static int32_t get_offset_of_phantomTracer_5() { return static_cast<int32_t>(offsetof(Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152, ___phantomTracer_5)); }
	inline LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * get_phantomTracer_5() const { return ___phantomTracer_5; }
	inline LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 ** get_address_of_phantomTracer_5() { return &___phantomTracer_5; }
	inline void set_phantomTracer_5(LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * value)
	{
		___phantomTracer_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___phantomTracer_5), (void*)value);
	}

	inline static int32_t get_offset_of_reflectionTracer_6() { return static_cast<int32_t>(offsetof(Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152, ___reflectionTracer_6)); }
	inline LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * get_reflectionTracer_6() const { return ___reflectionTracer_6; }
	inline LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 ** get_address_of_reflectionTracer_6() { return &___reflectionTracer_6; }
	inline void set_reflectionTracer_6(LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * value)
	{
		___reflectionTracer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reflectionTracer_6), (void*)value);
	}

	inline static int32_t get_offset_of_hitPoint_7() { return static_cast<int32_t>(offsetof(Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152, ___hitPoint_7)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_hitPoint_7() const { return ___hitPoint_7; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_hitPoint_7() { return &___hitPoint_7; }
	inline void set_hitPoint_7(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___hitPoint_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hitPoint_7), (void*)value);
	}

	inline static int32_t get_offset_of_cueMesh_8() { return static_cast<int32_t>(offsetof(Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152, ___cueMesh_8)); }
	inline MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * get_cueMesh_8() const { return ___cueMesh_8; }
	inline MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B ** get_address_of_cueMesh_8() { return &___cueMesh_8; }
	inline void set_cueMesh_8(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * value)
	{
		___cueMesh_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cueMesh_8), (void*)value);
	}

	inline static int32_t get_offset_of_CueBall_9() { return static_cast<int32_t>(offsetof(Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152, ___CueBall_9)); }
	inline Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * get_CueBall_9() const { return ___CueBall_9; }
	inline Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D ** get_address_of_CueBall_9() { return &___CueBall_9; }
	inline void set_CueBall_9(Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * value)
	{
		___CueBall_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CueBall_9), (void*)value);
	}

	inline static int32_t get_offset_of_phantom_10() { return static_cast<int32_t>(offsetof(Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152, ___phantom_10)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_phantom_10() const { return ___phantom_10; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_phantom_10() { return &___phantom_10; }
	inline void set_phantom_10(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___phantom_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___phantom_10), (void*)value);
	}

	inline static int32_t get_offset_of_Balls_11() { return static_cast<int32_t>(offsetof(Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152, ___Balls_11)); }
	inline List_1_t704724B962CF6005382C610663175BE693752D5D * get_Balls_11() const { return ___Balls_11; }
	inline List_1_t704724B962CF6005382C610663175BE693752D5D ** get_address_of_Balls_11() { return &___Balls_11; }
	inline void set_Balls_11(List_1_t704724B962CF6005382C610663175BE693752D5D * value)
	{
		___Balls_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Balls_11), (void*)value);
	}

	inline static int32_t get_offset_of_pockets_12() { return static_cast<int32_t>(offsetof(Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152, ___pockets_12)); }
	inline List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2 * get_pockets_12() const { return ___pockets_12; }
	inline List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2 ** get_address_of_pockets_12() { return &___pockets_12; }
	inline void set_pockets_12(List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2 * value)
	{
		___pockets_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pockets_12), (void*)value);
	}

	inline static int32_t get_offset_of_deck_13() { return static_cast<int32_t>(offsetof(Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152, ___deck_13)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_deck_13() const { return ___deck_13; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_deck_13() { return &___deck_13; }
	inline void set_deck_13(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___deck_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deck_13), (void*)value);
	}

	inline static int32_t get_offset_of_canContinue_14() { return static_cast<int32_t>(offsetof(Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152, ___canContinue_14)); }
	inline bool get_canContinue_14() const { return ___canContinue_14; }
	inline bool* get_address_of_canContinue_14() { return &___canContinue_14; }
	inline void set_canContinue_14(bool value)
	{
		___canContinue_14 = value;
	}

	inline static int32_t get_offset_of_phFriend_15() { return static_cast<int32_t>(offsetof(Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152, ___phFriend_15)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_phFriend_15() const { return ___phFriend_15; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_phFriend_15() { return &___phFriend_15; }
	inline void set_phFriend_15(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___phFriend_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___phFriend_15), (void*)value);
	}

	inline static int32_t get_offset_of_phFoe_16() { return static_cast<int32_t>(offsetof(Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152, ___phFoe_16)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_phFoe_16() const { return ___phFoe_16; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_phFoe_16() { return &___phFoe_16; }
	inline void set_phFoe_16(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___phFoe_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___phFoe_16), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// BotAI
struct  BotAI_t029234FCF3F2826C2123CD1BF8F65D212B156F80  : public PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9
{
public:
	// System.Single BotAI::dir
	float ___dir_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> BotAI::aims
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___aims_6;
	// System.Collections.Generic.List`1<Ball> BotAI::balls
	List_1_t704724B962CF6005382C610663175BE693752D5D * ___balls_7;
	// Pocket BotAI::targetP
	Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 * ___targetP_8;
	// Ball BotAI::targetB
	Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * ___targetB_9;

public:
	inline static int32_t get_offset_of_dir_5() { return static_cast<int32_t>(offsetof(BotAI_t029234FCF3F2826C2123CD1BF8F65D212B156F80, ___dir_5)); }
	inline float get_dir_5() const { return ___dir_5; }
	inline float* get_address_of_dir_5() { return &___dir_5; }
	inline void set_dir_5(float value)
	{
		___dir_5 = value;
	}

	inline static int32_t get_offset_of_aims_6() { return static_cast<int32_t>(offsetof(BotAI_t029234FCF3F2826C2123CD1BF8F65D212B156F80, ___aims_6)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_aims_6() const { return ___aims_6; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_aims_6() { return &___aims_6; }
	inline void set_aims_6(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___aims_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___aims_6), (void*)value);
	}

	inline static int32_t get_offset_of_balls_7() { return static_cast<int32_t>(offsetof(BotAI_t029234FCF3F2826C2123CD1BF8F65D212B156F80, ___balls_7)); }
	inline List_1_t704724B962CF6005382C610663175BE693752D5D * get_balls_7() const { return ___balls_7; }
	inline List_1_t704724B962CF6005382C610663175BE693752D5D ** get_address_of_balls_7() { return &___balls_7; }
	inline void set_balls_7(List_1_t704724B962CF6005382C610663175BE693752D5D * value)
	{
		___balls_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___balls_7), (void*)value);
	}

	inline static int32_t get_offset_of_targetP_8() { return static_cast<int32_t>(offsetof(BotAI_t029234FCF3F2826C2123CD1BF8F65D212B156F80, ___targetP_8)); }
	inline Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 * get_targetP_8() const { return ___targetP_8; }
	inline Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 ** get_address_of_targetP_8() { return &___targetP_8; }
	inline void set_targetP_8(Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 * value)
	{
		___targetP_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetP_8), (void*)value);
	}

	inline static int32_t get_offset_of_targetB_9() { return static_cast<int32_t>(offsetof(BotAI_t029234FCF3F2826C2123CD1BF8F65D212B156F80, ___targetB_9)); }
	inline Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * get_targetB_9() const { return ___targetB_9; }
	inline Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D ** get_address_of_targetB_9() { return &___targetB_9; }
	inline void set_targetB_9(Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * value)
	{
		___targetB_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetB_9), (void*)value);
	}
};


// InputManager
struct  InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A  : public PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9
{
public:

public:
};


// UnityEngine.EventSystems.EventSystem
struct  EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule> UnityEngine.EventSystems.EventSystem::m_SystemInputModules
	List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1 * ___m_SystemInputModules_4;
	// UnityEngine.EventSystems.BaseInputModule UnityEngine.EventSystems.EventSystem::m_CurrentInputModule
	BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924 * ___m_CurrentInputModule_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_FirstSelected
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_FirstSelected_7;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_sendNavigationEvents
	bool ___m_sendNavigationEvents_8;
	// System.Int32 UnityEngine.EventSystems.EventSystem::m_DragThreshold
	int32_t ___m_DragThreshold_9;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_CurrentSelected
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_CurrentSelected_10;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_HasFocus
	bool ___m_HasFocus_11;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_SelectionGuard
	bool ___m_SelectionGuard_12;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.EventSystem::m_DummyData
	BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * ___m_DummyData_13;

public:
	inline static int32_t get_offset_of_m_SystemInputModules_4() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_SystemInputModules_4)); }
	inline List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1 * get_m_SystemInputModules_4() const { return ___m_SystemInputModules_4; }
	inline List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1 ** get_address_of_m_SystemInputModules_4() { return &___m_SystemInputModules_4; }
	inline void set_m_SystemInputModules_4(List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1 * value)
	{
		___m_SystemInputModules_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SystemInputModules_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_CurrentInputModule_5() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_CurrentInputModule_5)); }
	inline BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924 * get_m_CurrentInputModule_5() const { return ___m_CurrentInputModule_5; }
	inline BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924 ** get_address_of_m_CurrentInputModule_5() { return &___m_CurrentInputModule_5; }
	inline void set_m_CurrentInputModule_5(BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924 * value)
	{
		___m_CurrentInputModule_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentInputModule_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_FirstSelected_7() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_FirstSelected_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_FirstSelected_7() const { return ___m_FirstSelected_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_FirstSelected_7() { return &___m_FirstSelected_7; }
	inline void set_m_FirstSelected_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_FirstSelected_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FirstSelected_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_sendNavigationEvents_8() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_sendNavigationEvents_8)); }
	inline bool get_m_sendNavigationEvents_8() const { return ___m_sendNavigationEvents_8; }
	inline bool* get_address_of_m_sendNavigationEvents_8() { return &___m_sendNavigationEvents_8; }
	inline void set_m_sendNavigationEvents_8(bool value)
	{
		___m_sendNavigationEvents_8 = value;
	}

	inline static int32_t get_offset_of_m_DragThreshold_9() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_DragThreshold_9)); }
	inline int32_t get_m_DragThreshold_9() const { return ___m_DragThreshold_9; }
	inline int32_t* get_address_of_m_DragThreshold_9() { return &___m_DragThreshold_9; }
	inline void set_m_DragThreshold_9(int32_t value)
	{
		___m_DragThreshold_9 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelected_10() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_CurrentSelected_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_CurrentSelected_10() const { return ___m_CurrentSelected_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_CurrentSelected_10() { return &___m_CurrentSelected_10; }
	inline void set_m_CurrentSelected_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_CurrentSelected_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentSelected_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_HasFocus_11() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_HasFocus_11)); }
	inline bool get_m_HasFocus_11() const { return ___m_HasFocus_11; }
	inline bool* get_address_of_m_HasFocus_11() { return &___m_HasFocus_11; }
	inline void set_m_HasFocus_11(bool value)
	{
		___m_HasFocus_11 = value;
	}

	inline static int32_t get_offset_of_m_SelectionGuard_12() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_SelectionGuard_12)); }
	inline bool get_m_SelectionGuard_12() const { return ___m_SelectionGuard_12; }
	inline bool* get_address_of_m_SelectionGuard_12() { return &___m_SelectionGuard_12; }
	inline void set_m_SelectionGuard_12(bool value)
	{
		___m_SelectionGuard_12 = value;
	}

	inline static int32_t get_offset_of_m_DummyData_13() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_DummyData_13)); }
	inline BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * get_m_DummyData_13() const { return ___m_DummyData_13; }
	inline BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E ** get_address_of_m_DummyData_13() { return &___m_DummyData_13; }
	inline void set_m_DummyData_13(BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * value)
	{
		___m_DummyData_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DummyData_13), (void*)value);
	}
};

struct EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem> UnityEngine.EventSystems.EventSystem::m_EventSystems
	List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733 * ___m_EventSystems_6;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::s_RaycastComparer
	Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38 * ___s_RaycastComparer_14;

public:
	inline static int32_t get_offset_of_m_EventSystems_6() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_StaticFields, ___m_EventSystems_6)); }
	inline List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733 * get_m_EventSystems_6() const { return ___m_EventSystems_6; }
	inline List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733 ** get_address_of_m_EventSystems_6() { return &___m_EventSystems_6; }
	inline void set_m_EventSystems_6(List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733 * value)
	{
		___m_EventSystems_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventSystems_6), (void*)value);
	}

	inline static int32_t get_offset_of_s_RaycastComparer_14() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_StaticFields, ___s_RaycastComparer_14)); }
	inline Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38 * get_s_RaycastComparer_14() const { return ___s_RaycastComparer_14; }
	inline Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38 ** get_address_of_s_RaycastComparer_14() { return &___s_RaycastComparer_14; }
	inline void set_s_RaycastComparer_14(Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38 * value)
	{
		___s_RaycastComparer_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_RaycastComparer_14), (void*)value);
	}
};


// UnityEngine.UI.Graphic
struct  Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct  Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  ___m_Navigation_6;
	// UnityEngine.UI.Selectable_Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_7;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___m_Colors_8;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  ___m_SpriteState_9;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * ___m_AnimationTriggers_10;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_11;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_TargetGraphic_12;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_13;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_14;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_17;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * ___m_CanvasGroupCache_18;

public:
	inline static int32_t get_offset_of_m_Navigation_6() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Navigation_6)); }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  get_m_Navigation_6() const { return ___m_Navigation_6; }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A * get_address_of_m_Navigation_6() { return &___m_Navigation_6; }
	inline void set_m_Navigation_6(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  value)
	{
		___m_Navigation_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_6))->___m_SelectOnUp_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_6))->___m_SelectOnDown_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_6))->___m_SelectOnLeft_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_6))->___m_SelectOnRight_4), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_7() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Transition_7)); }
	inline int32_t get_m_Transition_7() const { return ___m_Transition_7; }
	inline int32_t* get_address_of_m_Transition_7() { return &___m_Transition_7; }
	inline void set_m_Transition_7(int32_t value)
	{
		___m_Transition_7 = value;
	}

	inline static int32_t get_offset_of_m_Colors_8() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Colors_8)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_m_Colors_8() const { return ___m_Colors_8; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_m_Colors_8() { return &___m_Colors_8; }
	inline void set_m_Colors_8(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___m_Colors_8 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_9() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_SpriteState_9)); }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  get_m_SpriteState_9() const { return ___m_SpriteState_9; }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E * get_address_of_m_SpriteState_9() { return &___m_SpriteState_9; }
	inline void set_m_SpriteState_9(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  value)
	{
		___m_SpriteState_9 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_9))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_9))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_9))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_9))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_10() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_AnimationTriggers_10)); }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * get_m_AnimationTriggers_10() const { return ___m_AnimationTriggers_10; }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 ** get_address_of_m_AnimationTriggers_10() { return &___m_AnimationTriggers_10; }
	inline void set_m_AnimationTriggers_10(AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * value)
	{
		___m_AnimationTriggers_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_11() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Interactable_11)); }
	inline bool get_m_Interactable_11() const { return ___m_Interactable_11; }
	inline bool* get_address_of_m_Interactable_11() { return &___m_Interactable_11; }
	inline void set_m_Interactable_11(bool value)
	{
		___m_Interactable_11 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_12() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_TargetGraphic_12)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_TargetGraphic_12() const { return ___m_TargetGraphic_12; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_TargetGraphic_12() { return &___m_TargetGraphic_12; }
	inline void set_m_TargetGraphic_12(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_TargetGraphic_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_13() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_GroupsAllowInteraction_13)); }
	inline bool get_m_GroupsAllowInteraction_13() const { return ___m_GroupsAllowInteraction_13; }
	inline bool* get_address_of_m_GroupsAllowInteraction_13() { return &___m_GroupsAllowInteraction_13; }
	inline void set_m_GroupsAllowInteraction_13(bool value)
	{
		___m_GroupsAllowInteraction_13 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_14() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CurrentIndex_14)); }
	inline int32_t get_m_CurrentIndex_14() const { return ___m_CurrentIndex_14; }
	inline int32_t* get_address_of_m_CurrentIndex_14() { return &___m_CurrentIndex_14; }
	inline void set_m_CurrentIndex_14(int32_t value)
	{
		___m_CurrentIndex_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerInsideU3Ek__BackingField_15)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_15() const { return ___U3CisPointerInsideU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_15() { return &___U3CisPointerInsideU3Ek__BackingField_15; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_15(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerDownU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_16() const { return ___U3CisPointerDownU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_16() { return &___U3CisPointerDownU3Ek__BackingField_16; }
	inline void set_U3CisPointerDownU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3ChasSelectionU3Ek__BackingField_17)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_17() const { return ___U3ChasSelectionU3Ek__BackingField_17; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_17() { return &___U3ChasSelectionU3Ek__BackingField_17; }
	inline void set_U3ChasSelectionU3Ek__BackingField_17(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_18() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CanvasGroupCache_18)); }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * get_m_CanvasGroupCache_18() const { return ___m_CanvasGroupCache_18; }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D ** get_address_of_m_CanvasGroupCache_18() { return &___m_CanvasGroupCache_18; }
	inline void set_m_CanvasGroupCache_18(List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * value)
	{
		___m_CanvasGroupCache_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_18), (void*)value);
	}
};

struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// UnityEngine.UI.Button
struct  Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.UI.Button_ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * ___m_OnClick_19;

public:
	inline static int32_t get_offset_of_m_OnClick_19() { return static_cast<int32_t>(offsetof(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D, ___m_OnClick_19)); }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * get_m_OnClick_19() const { return ___m_OnClick_19; }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F ** get_address_of_m_OnClick_19() { return &___m_OnClick_19; }
	inline void set_m_OnClick_19(ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * value)
	{
		___m_OnClick_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnClick_19), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Slider
struct  Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_FillRect_19;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_HandleRect_20;
	// UnityEngine.UI.Slider_Direction UnityEngine.UI.Slider::m_Direction
	int32_t ___m_Direction_21;
	// System.Single UnityEngine.UI.Slider::m_MinValue
	float ___m_MinValue_22;
	// System.Single UnityEngine.UI.Slider::m_MaxValue
	float ___m_MaxValue_23;
	// System.Boolean UnityEngine.UI.Slider::m_WholeNumbers
	bool ___m_WholeNumbers_24;
	// System.Single UnityEngine.UI.Slider::m_Value
	float ___m_Value_25;
	// UnityEngine.UI.Slider_SliderEvent UnityEngine.UI.Slider::m_OnValueChanged
	SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * ___m_OnValueChanged_26;
	// UnityEngine.UI.Image UnityEngine.UI.Slider::m_FillImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___m_FillImage_27;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_FillTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_FillTransform_28;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillContainerRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_FillContainerRect_29;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_HandleTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_HandleTransform_30;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleContainerRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_HandleContainerRect_31;
	// UnityEngine.Vector2 UnityEngine.UI.Slider::m_Offset
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Offset_32;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Slider::m_Tracker
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  ___m_Tracker_33;
	// System.Boolean UnityEngine.UI.Slider::m_DelayedUpdateVisuals
	bool ___m_DelayedUpdateVisuals_34;

public:
	inline static int32_t get_offset_of_m_FillRect_19() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillRect_19)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_FillRect_19() const { return ___m_FillRect_19; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_FillRect_19() { return &___m_FillRect_19; }
	inline void set_m_FillRect_19(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_FillRect_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillRect_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_HandleRect_20() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_HandleRect_20)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_HandleRect_20() const { return ___m_HandleRect_20; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_HandleRect_20() { return &___m_HandleRect_20; }
	inline void set_m_HandleRect_20(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_HandleRect_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleRect_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_Direction_21() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Direction_21)); }
	inline int32_t get_m_Direction_21() const { return ___m_Direction_21; }
	inline int32_t* get_address_of_m_Direction_21() { return &___m_Direction_21; }
	inline void set_m_Direction_21(int32_t value)
	{
		___m_Direction_21 = value;
	}

	inline static int32_t get_offset_of_m_MinValue_22() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_MinValue_22)); }
	inline float get_m_MinValue_22() const { return ___m_MinValue_22; }
	inline float* get_address_of_m_MinValue_22() { return &___m_MinValue_22; }
	inline void set_m_MinValue_22(float value)
	{
		___m_MinValue_22 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_23() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_MaxValue_23)); }
	inline float get_m_MaxValue_23() const { return ___m_MaxValue_23; }
	inline float* get_address_of_m_MaxValue_23() { return &___m_MaxValue_23; }
	inline void set_m_MaxValue_23(float value)
	{
		___m_MaxValue_23 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_24() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_WholeNumbers_24)); }
	inline bool get_m_WholeNumbers_24() const { return ___m_WholeNumbers_24; }
	inline bool* get_address_of_m_WholeNumbers_24() { return &___m_WholeNumbers_24; }
	inline void set_m_WholeNumbers_24(bool value)
	{
		___m_WholeNumbers_24 = value;
	}

	inline static int32_t get_offset_of_m_Value_25() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Value_25)); }
	inline float get_m_Value_25() const { return ___m_Value_25; }
	inline float* get_address_of_m_Value_25() { return &___m_Value_25; }
	inline void set_m_Value_25(float value)
	{
		___m_Value_25 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_26() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_OnValueChanged_26)); }
	inline SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * get_m_OnValueChanged_26() const { return ___m_OnValueChanged_26; }
	inline SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 ** get_address_of_m_OnValueChanged_26() { return &___m_OnValueChanged_26; }
	inline void set_m_OnValueChanged_26(SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * value)
	{
		___m_OnValueChanged_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValueChanged_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_FillImage_27() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillImage_27)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_m_FillImage_27() const { return ___m_FillImage_27; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_m_FillImage_27() { return &___m_FillImage_27; }
	inline void set_m_FillImage_27(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___m_FillImage_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillImage_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_FillTransform_28() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillTransform_28)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_FillTransform_28() const { return ___m_FillTransform_28; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_FillTransform_28() { return &___m_FillTransform_28; }
	inline void set_m_FillTransform_28(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_FillTransform_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillTransform_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_FillContainerRect_29() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillContainerRect_29)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_FillContainerRect_29() const { return ___m_FillContainerRect_29; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_FillContainerRect_29() { return &___m_FillContainerRect_29; }
	inline void set_m_FillContainerRect_29(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_FillContainerRect_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillContainerRect_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_30() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_HandleTransform_30)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_HandleTransform_30() const { return ___m_HandleTransform_30; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_HandleTransform_30() { return &___m_HandleTransform_30; }
	inline void set_m_HandleTransform_30(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_HandleTransform_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleTransform_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_31() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_HandleContainerRect_31)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_HandleContainerRect_31() const { return ___m_HandleContainerRect_31; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_HandleContainerRect_31() { return &___m_HandleContainerRect_31; }
	inline void set_m_HandleContainerRect_31(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_HandleContainerRect_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleContainerRect_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_Offset_32() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Offset_32)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Offset_32() const { return ___m_Offset_32; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Offset_32() { return &___m_Offset_32; }
	inline void set_m_Offset_32(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Offset_32 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_33() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Tracker_33)); }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  get_m_Tracker_33() const { return ___m_Tracker_33; }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * get_address_of_m_Tracker_33() { return &___m_Tracker_33; }
	inline void set_m_Tracker_33(DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  value)
	{
		___m_Tracker_33 = value;
	}

	inline static int32_t get_offset_of_m_DelayedUpdateVisuals_34() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_DelayedUpdateVisuals_34)); }
	inline bool get_m_DelayedUpdateVisuals_34() const { return ___m_DelayedUpdateVisuals_34; }
	inline bool* get_address_of_m_DelayedUpdateVisuals_34() { return &___m_DelayedUpdateVisuals_34; }
	inline void set_m_DelayedUpdateVisuals_34(bool value)
	{
		___m_DelayedUpdateVisuals_34 = value;
	}
};


// UnityEngine.UI.Image
struct  Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Sprite_37;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_OverrideSprite_38;
	// UnityEngine.UI.Image_Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_39;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_40;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_41;
	// UnityEngine.UI.Image_FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_42;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_43;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_44;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_45;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_46;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_47;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_48;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_49;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_50;

public:
	inline static int32_t get_offset_of_m_Sprite_37() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Sprite_37)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Sprite_37() const { return ___m_Sprite_37; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Sprite_37() { return &___m_Sprite_37; }
	inline void set_m_Sprite_37(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Sprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_38() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_OverrideSprite_38)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_OverrideSprite_38() const { return ___m_OverrideSprite_38; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_OverrideSprite_38() { return &___m_OverrideSprite_38; }
	inline void set_m_OverrideSprite_38(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_OverrideSprite_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_39() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Type_39)); }
	inline int32_t get_m_Type_39() const { return ___m_Type_39; }
	inline int32_t* get_address_of_m_Type_39() { return &___m_Type_39; }
	inline void set_m_Type_39(int32_t value)
	{
		___m_Type_39 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_40() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PreserveAspect_40)); }
	inline bool get_m_PreserveAspect_40() const { return ___m_PreserveAspect_40; }
	inline bool* get_address_of_m_PreserveAspect_40() { return &___m_PreserveAspect_40; }
	inline void set_m_PreserveAspect_40(bool value)
	{
		___m_PreserveAspect_40 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_41() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillCenter_41)); }
	inline bool get_m_FillCenter_41() const { return ___m_FillCenter_41; }
	inline bool* get_address_of_m_FillCenter_41() { return &___m_FillCenter_41; }
	inline void set_m_FillCenter_41(bool value)
	{
		___m_FillCenter_41 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_42() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillMethod_42)); }
	inline int32_t get_m_FillMethod_42() const { return ___m_FillMethod_42; }
	inline int32_t* get_address_of_m_FillMethod_42() { return &___m_FillMethod_42; }
	inline void set_m_FillMethod_42(int32_t value)
	{
		___m_FillMethod_42 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_43() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillAmount_43)); }
	inline float get_m_FillAmount_43() const { return ___m_FillAmount_43; }
	inline float* get_address_of_m_FillAmount_43() { return &___m_FillAmount_43; }
	inline void set_m_FillAmount_43(float value)
	{
		___m_FillAmount_43 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_44() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillClockwise_44)); }
	inline bool get_m_FillClockwise_44() const { return ___m_FillClockwise_44; }
	inline bool* get_address_of_m_FillClockwise_44() { return &___m_FillClockwise_44; }
	inline void set_m_FillClockwise_44(bool value)
	{
		___m_FillClockwise_44 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_45() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillOrigin_45)); }
	inline int32_t get_m_FillOrigin_45() const { return ___m_FillOrigin_45; }
	inline int32_t* get_address_of_m_FillOrigin_45() { return &___m_FillOrigin_45; }
	inline void set_m_FillOrigin_45(int32_t value)
	{
		___m_FillOrigin_45 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_46() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_AlphaHitTestMinimumThreshold_46)); }
	inline float get_m_AlphaHitTestMinimumThreshold_46() const { return ___m_AlphaHitTestMinimumThreshold_46; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_46() { return &___m_AlphaHitTestMinimumThreshold_46; }
	inline void set_m_AlphaHitTestMinimumThreshold_46(float value)
	{
		___m_AlphaHitTestMinimumThreshold_46 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_47() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Tracked_47)); }
	inline bool get_m_Tracked_47() const { return ___m_Tracked_47; }
	inline bool* get_address_of_m_Tracked_47() { return &___m_Tracked_47; }
	inline void set_m_Tracked_47(bool value)
	{
		___m_Tracked_47 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_48() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_UseSpriteMesh_48)); }
	inline bool get_m_UseSpriteMesh_48() const { return ___m_UseSpriteMesh_48; }
	inline bool* get_address_of_m_UseSpriteMesh_48() { return &___m_UseSpriteMesh_48; }
	inline void set_m_UseSpriteMesh_48(bool value)
	{
		___m_UseSpriteMesh_48 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_49() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PixelsPerUnitMultiplier_49)); }
	inline float get_m_PixelsPerUnitMultiplier_49() const { return ___m_PixelsPerUnitMultiplier_49; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_49() { return &___m_PixelsPerUnitMultiplier_49; }
	inline void set_m_PixelsPerUnitMultiplier_49(float value)
	{
		___m_PixelsPerUnitMultiplier_49 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_50() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_CachedReferencePixelsPerUnit_50)); }
	inline float get_m_CachedReferencePixelsPerUnit_50() const { return ___m_CachedReferencePixelsPerUnit_50; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_50() { return &___m_CachedReferencePixelsPerUnit_50; }
	inline void set_m_CachedReferencePixelsPerUnit_50(float value)
	{
		___m_CachedReferencePixelsPerUnit_50 = value;
	}
};

struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_ETC1DefaultUI_36;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_VertScratch_51;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_UVScratch_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Xy_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Uv_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t815A476B0A21E183042059E705F9E505478CD8AE * ___m_TrackedTexturelessImages_55;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_56;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_36() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_ETC1DefaultUI_36)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_ETC1DefaultUI_36() const { return ___s_ETC1DefaultUI_36; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_ETC1DefaultUI_36() { return &___s_ETC1DefaultUI_36; }
	inline void set_s_ETC1DefaultUI_36(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_ETC1DefaultUI_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_51() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_VertScratch_51)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_VertScratch_51() const { return ___s_VertScratch_51; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_VertScratch_51() { return &___s_VertScratch_51; }
	inline void set_s_VertScratch_51(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_VertScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_52() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_UVScratch_52)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_UVScratch_52() const { return ___s_UVScratch_52; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_UVScratch_52() { return &___s_UVScratch_52; }
	inline void set_s_UVScratch_52(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_UVScratch_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_53() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Xy_53)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Xy_53() const { return ___s_Xy_53; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Xy_53() { return &___s_Xy_53; }
	inline void set_s_Xy_53(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Xy_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_53), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_54() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Uv_54)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Uv_54() const { return ___s_Uv_54; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Uv_54() { return &___s_Uv_54; }
	inline void set_s_Uv_54(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Uv_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_54), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_55() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___m_TrackedTexturelessImages_55)); }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE * get_m_TrackedTexturelessImages_55() const { return ___m_TrackedTexturelessImages_55; }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE ** get_address_of_m_TrackedTexturelessImages_55() { return &___m_TrackedTexturelessImages_55; }
	inline void set_m_TrackedTexturelessImages_55(List_1_t815A476B0A21E183042059E705F9E505478CD8AE * value)
	{
		___m_TrackedTexturelessImages_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_55), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_56() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Initialized_56)); }
	inline bool get_s_Initialized_56() const { return ___s_Initialized_56; }
	inline bool* get_address_of_s_Initialized_56() { return &___s_Initialized_56; }
	inline void set_s_Initialized_56(bool value)
	{
		___s_Initialized_56 = value;
	}
};


// UnityEngine.UI.Text
struct  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_TempVerts_42;

public:
	inline static int32_t get_offset_of_m_FontData_36() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_FontData_36)); }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * get_m_FontData_36() const { return ___m_FontData_36; }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 ** get_address_of_m_FontData_36() { return &___m_FontData_36; }
	inline void set_m_FontData_36(FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * value)
	{
		___m_FontData_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_37() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_Text_37)); }
	inline String_t* get_m_Text_37() const { return ___m_Text_37; }
	inline String_t** get_address_of_m_Text_37() { return &___m_Text_37; }
	inline void set_m_Text_37(String_t* value)
	{
		___m_Text_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_38() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCache_38)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCache_38() const { return ___m_TextCache_38; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCache_38() { return &___m_TextCache_38; }
	inline void set_m_TextCache_38(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCache_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_39() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCacheForLayout_39)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCacheForLayout_39() const { return ___m_TextCacheForLayout_39; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCacheForLayout_39() { return &___m_TextCacheForLayout_39; }
	inline void set_m_TextCacheForLayout_39(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCacheForLayout_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_41() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_DisableFontTextureRebuiltCallback_41)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_41() const { return ___m_DisableFontTextureRebuiltCallback_41; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_41() { return &___m_DisableFontTextureRebuiltCallback_41; }
	inline void set_m_DisableFontTextureRebuiltCallback_41(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_41 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_42() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TempVerts_42)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_TempVerts_42() const { return ___m_TempVerts_42; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_TempVerts_42() { return &___m_TempVerts_42; }
	inline void set_m_TempVerts_42(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_TempVerts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_42), (void*)value);
	}
};

struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultText_40;

public:
	inline static int32_t get_offset_of_s_DefaultText_40() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields, ___s_DefaultText_40)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultText_40() const { return ___s_DefaultText_40; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultText_40() { return &___s_DefaultText_40; }
	inline void set_s_DefaultText_40(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_40), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Card[]
struct CardU5BU5D_t32023BF72E2560CD1EA88DF7C04357E784A64CE0  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A * m_Items[1];

public:
	inline Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  m_Items[1];

public:
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_mE0F03A2E42E2F7F8A282AE01C12945F7379DC702_gshared (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_gshared (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Int32 System.Linq.Enumerable::Count<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Enumerable_Count_TisRuntimeObject_mF35F8B37C78D02C08BB4F806038CA6EDE548A6B5_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mCA84157864A199574AD0B7F3083F99B54DC1F98C_gshared (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Where_TisRuntimeObject_m51DA29A5CB10D532C42135ADA3270F6E695B9364_gshared (RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, const RuntimeMethod* method);
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * Enumerable_ToList_TisRuntimeObject_mA4E485F973C6DF746B8DB54CA6F54192D4231CA2_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_gshared (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___item0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,System.Int32Enum>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction_2__ctor_m50E7B823E46CB327D49A2D55A761F57472037634_gshared (UnityAction_2_t808E43EBC9AA89CEA5830BD187EC213182A02B50 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_mD211EB15E9E128684605B4CC7277F10840F8E8CF_gshared (RuntimeObject * ___original0, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// !!0 System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Enumerable_First_TisRuntimeObject_m24A685394AAEB22D562E51D9B0F4E6B5BAB2C03C_gshared (RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction_1__ctor_m8CACADCAC18230FB18DF7A6BEC3D9EAD93FEDC3B_gshared (UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1_AddListener_mA73838FBF3836695F5183B32B797E9499BA5E59C_gshared (UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC * __this, UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB * ___call0, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Component_GetComponentsInChildren_TisRuntimeObject_mCA5B356D4B0824C6DE60A8E90E6A6D4188C56C2F_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Nullable`1<System.Int32Enum>::.ctor(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Nullable_1__ctor_m904114DBE44D14D291456629D60BF66ECA75BB42_gshared (Nullable_1_t64244F99361E39CBE565C5E89436C898F18DF5DC * __this, int32_t ___value0, const RuntimeMethod* method);
// !0 System.Nullable`1<System.Int32Enum>::GetValueOrDefault()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Nullable_1_GetValueOrDefault_mA8DDAB2C6553ED7FFC9A55E1A92A96B3571000EC_gshared_inline (Nullable_1_t64244F99361E39CBE565C5E89436C898F18DF5DC * __this, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<System.Int32Enum>::get_HasValue()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m4C033F49F5318E94BC8CBA9CE5175EFDBFADEF9C_gshared_inline (Nullable_1_t64244F99361E39CBE565C5E89436C898F18DF5DC * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Boolean System.Linq.Enumerable::All<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerable_All_TisRuntimeObject_mAB7DDBA2A721E5E08769CDBFD1E9FA658DD14989_gshared (RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, const RuntimeMethod* method);
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerable_Any_TisRuntimeObject_m79E5BEA29E663B44BB9C1BDD62286D214D22E600_gshared (RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Remove_m753F7B4281CC4D02C07AE90726F51EF34B588DF7_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);

// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// GameManager GameManager::Instance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline (const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32 (Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void BotAI::CheckBalls()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BotAI_CheckBalls_mCE475ACCFD1A63095C82F4106233D52E16371AB6 (BotAI_t029234FCF3F2826C2123CD1BF8F65D212B156F80 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
inline int32_t List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, const RuntimeMethod*))List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_gshared_inline)(__this, method);
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A (int32_t ___min0, int32_t ___max1, const RuntimeMethod* method);
// System.Void Player::CueRotate(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_CueRotate_m531643FEA219030BAD2FE999897D386DBD49FDD1 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, float ___angle0, const RuntimeMethod* method);
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2 (float ___min0, float ___max1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, int32_t, const RuntimeMethod*))List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_gshared_inline)(__this, ___index0, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_LookRotation_m1B0BEBEBCC384324A6771B9EAC89761F73E1D6BF (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forward0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87 (float ___value0, float ___min1, float ___max2, const RuntimeMethod* method);
// System.Void Player::CueHit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_CueHit_m5EB2329180A3544C0DC71D37CA3793D9FB446A97 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear()
inline void List_1_Clear_mE0F03A2E42E2F7F8A282AE01C12945F7379DC702 (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, const RuntimeMethod*))List_1_Clear_mE0F03A2E42E2F7F8A282AE01C12945F7379DC702_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Ball>::Clear()
inline void List_1_Clear_m5A43FA83CABB71CFD6FA538507B7F1D460BA0A76 (List_1_t704724B962CF6005382C610663175BE693752D5D * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t704724B962CF6005382C610663175BE693752D5D *, const RuntimeMethod*))List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
inline void List_1__ctor_mF8F23D572031748AD428623AE16803455997E297 (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, const RuntimeMethod*))List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Ball>::.ctor()
inline void List_1__ctor_m50441437BE40ED72A3F37812FBDC7B631EDCE80D (List_1_t704724B962CF6005382C610663175BE693752D5D * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t704724B962CF6005382C610663175BE693752D5D *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Int32 System.Linq.Enumerable::Count<Ball>(System.Collections.Generic.IEnumerable`1<!!0>)
inline int32_t Enumerable_Count_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m3F3E48596ECCC77DD571F837FE7C1AD0A9A43244 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_Count_TisRuntimeObject_mF35F8B37C78D02C08BB4F806038CA6EDE548A6B5_gshared)(___source0, method);
}
// System.Void System.Func`2<Ball,System.Boolean>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m7141D4C33E399BD20EB8B17781BACB3B85C669C6 (Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_mCA84157864A199574AD0B7F3083F99B54DC1F98C_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<Ball>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
inline RuntimeObject* Enumerable_Where_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m0D5793A8B17C8583062257D58FAB67E4EB4D15DD (RuntimeObject* ___source0, Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * ___predicate1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 *, const RuntimeMethod*))Enumerable_Where_TisRuntimeObject_m51DA29A5CB10D532C42135ADA3270F6E695B9364_gshared)(___source0, ___predicate1, method);
}
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<Ball>(System.Collections.Generic.IEnumerable`1<!!0>)
inline List_1_t704724B962CF6005382C610663175BE693752D5D * Enumerable_ToList_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m8B14924409A741E59EEBD43F57AE8C533328E205 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  List_1_t704724B962CF6005382C610663175BE693752D5D * (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToList_TisRuntimeObject_mA4E485F973C6DF746B8DB54CA6F54192D4231CA2_gshared)(___source0, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Pocket>::GetEnumerator()
inline Enumerator_t078AC40DBA49748835454B10BFD699FDDE55CB4C  List_1_GetEnumerator_m389535AFAD3A7E50E8EBCC6C5AF2BEFBC4208E62 (List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t078AC40DBA49748835454B10BFD699FDDE55CB4C  (*) (List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2 *, const RuntimeMethod*))List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<Pocket>::get_Current()
inline Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 * Enumerator_get_Current_mB806EC855AC481BB6CB9DCF8A727E583A36F8817_inline (Enumerator_t078AC40DBA49748835454B10BFD699FDDE55CB4C * __this, const RuntimeMethod* method)
{
	return ((  Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 * (*) (Enumerator_t078AC40DBA49748835454B10BFD699FDDE55CB4C *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Ball>::GetEnumerator()
inline Enumerator_tD4E53BC75AF1AB2047F41AD591569A06B2EFC316  List_1_GetEnumerator_m3049E16624476F2B193E0E3D8A77289A30598E05 (List_1_t704724B962CF6005382C610663175BE693752D5D * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tD4E53BC75AF1AB2047F41AD591569A06B2EFC316  (*) (List_1_t704724B962CF6005382C610663175BE693752D5D *, const RuntimeMethod*))List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<Ball>::get_Current()
inline Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * Enumerator_get_Current_mC7B26CA370CBDEC8C71772629C62C5940FF5F825_inline (Enumerator_tD4E53BC75AF1AB2047F41AD591569A06B2EFC316 * __this, const RuntimeMethod* method)
{
	return ((  Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * (*) (Enumerator_tD4E53BC75AF1AB2047F41AD591569A06B2EFC316 *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_SphereCast_m2E200DFD71A597968D4F50B8A6284F2E622B4C57 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___origin0, float ___radius1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction2, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * ___hitInfo3, float ___maxDistance4, const RuntimeMethod* method);
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_UnaryNegation_m362EA356F4CADEDB39F965A0DBDED6EA890925F7_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_SphereCast_m49360D9368FF36611C0E5E73B56A8FDF9638E2E3 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___origin0, float ___radius1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction2, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * ___hitInfo3, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___exists0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Angle(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_Angle_m3715AB03A36C59D8CF08F8D71E2F46454EB884C1 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___from0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___to1, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_green()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_green_mFF9BD42534D385A0717B1EAD083ADF08712984B9 (const RuntimeMethod* method);
// System.Void UnityEngine.Debug::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_DrawLine_mA85118FC8F0D6DF41CBF0AC95DB19CE2F2DB5149 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___start0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___end1, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color2, float ___duration3, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_red()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8 (const RuntimeMethod* method);
// System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_DrawRay_m3954B3FFA675C0660ED438E8B705B45EDE393C60 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___start0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___dir1, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color2, float ___duration3, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
inline void List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59 (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , const RuntimeMethod*))List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_gshared)(__this, ___item0, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Ball>::MoveNext()
inline bool Enumerator_MoveNext_mF564EA6451E132F369252AD607F78B570253D320 (Enumerator_tD4E53BC75AF1AB2047F41AD591569A06B2EFC316 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tD4E53BC75AF1AB2047F41AD591569A06B2EFC316 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Ball>::Dispose()
inline void Enumerator_Dispose_m5FBC6AE392B5BDC7E57DA42B723E6819F1527B0D (Enumerator_tD4E53BC75AF1AB2047F41AD591569A06B2EFC316 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tD4E53BC75AF1AB2047F41AD591569A06B2EFC316 *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pocket>::MoveNext()
inline bool Enumerator_MoveNext_m2503905BF86ABEB00CE962518451A3A95482500B (Enumerator_t078AC40DBA49748835454B10BFD699FDDE55CB4C * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t078AC40DBA49748835454B10BFD699FDDE55CB4C *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Pocket>::Dispose()
inline void Enumerator_Dispose_m45DF0A38A6BF81DD15FFAE777BF93F6019D59E4D (Enumerator_t078AC40DBA49748835454B10BFD699FDDE55CB4C * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t078AC40DBA49748835454B10BFD699FDDE55CB4C *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// System.Void PlayerController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * __this, const RuntimeMethod* method);
// BallsType Ball::Type()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Ball_Type_mB4092D6071D5B95B0EB1FB883A3D2EC4A3663FF6 (Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_fillAmount(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Image_set_fillAmount_m1D28CFC9B15A45AB6C561AA42BD8F305605E9E3C (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4 (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * __this, Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___value0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_print_m4F113B89EC1C221CAC6EC64365E6DAD0AF86F090 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::.ctor(System.Object,System.IntPtr)
inline void UnityAction_2__ctor_mE0417B33CF845A6B9324E67D296ADEA562B91DE0 (UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906 *, RuntimeObject *, intptr_t, const RuntimeMethod*))UnityAction_2__ctor_m50E7B823E46CB327D49A2D55A761F57472037634_gshared)(__this, ___object0, ___method1, method);
}
// System.Void UnityEngine.SceneManagement.SceneManager::add_sceneLoaded(UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_add_sceneLoaded_m54990A485E2E66739E31090BDC3A4C01EF7729BA (UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___target0, const RuntimeMethod* method);
// System.Void Player::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method);
// System.Void GameManager::SetForce()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_SetForce_m2B0FF265E47640E330E5D78D23FCF62ADC99084A (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method);
// System.String System.Single::ToString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Single_ToString_m15F10F2AFF80750906CEFCFB456EBA84F9D2E8D7 (float* __this, String_t* ___format0, const RuntimeMethod* method);
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Scene_get_buildIndex_mE32CE766EA0790E4636A351BA353A7FD71A11DA4 (Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5 (int32_t ___sceneBuildIndex0, const RuntimeMethod* method);
// System.Void GameManager::RegisterBall(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_RegisterBall_m33C55F5DAF74AF3A685FC4EC267C3CC4280A5F74 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, int32_t ___i0, const RuntimeMethod* method);
// System.Void GameManager/<>c__DisplayClass30_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass30_0__ctor_m475CBDDDBF19B84D9ADCAA4A4DFE559EF79DD2CE (U3CU3Ec__DisplayClass30_0_t9F9560A6034B2661D9C6E093FA21372992814025 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.UI.Image>(!!0,UnityEngine.Transform)
inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * Object_Instantiate_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m8F5F75D5872251B7E04B6D385D1BDD52DD9F3B5D (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___original0, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent1, const RuntimeMethod* method)
{
	return ((  Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * (*) (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C *, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mD211EB15E9E128684605B4CC7277F10840F8E8CF_gshared)(___original0, ___parent1, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.Sprite>::get_Item(System.Int32)
inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * List_1_get_Item_m2E8D9A40352859B1D5465D074732572FC364786E_inline (List_1_t99B54448C695C6F7103A0DE4320F1A7EF7B30880 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * (*) (List_1_t99B54448C695C6F7103A0DE4320F1A7EF7B30880 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// !!0 System.Linq.Enumerable::First<Ball>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
inline Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * Enumerable_First_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m1AEA5E6A40B402B2518D20770F78C970461347D1 (RuntimeObject* ___source0, Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * ___predicate1, const RuntimeMethod* method)
{
	return ((  Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * (*) (RuntimeObject*, Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 *, const RuntimeMethod*))Enumerable_First_TisRuntimeObject_m24A685394AAEB22D562E51D9B0F4E6B5BAB2C03C_gshared)(___source0, ___predicate1, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Add(!0)
inline void List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3 (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Void GameManager::set_Clock(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_set_Clock_m6A52D50A47B356DE092A46E16D85258DAA4338C2 (float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// UnityEngine.UI.Slider/SliderEvent UnityEngine.UI.Slider::get_onValueChanged()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * Slider_get_onValueChanged_m7F480C569A6D668952BE1436691850D13825E129_inline (Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::.ctor(System.Object,System.IntPtr)
inline void UnityAction_1__ctor_m8CACADCAC18230FB18DF7A6BEC3D9EAD93FEDC3B (UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB *, RuntimeObject *, intptr_t, const RuntimeMethod*))UnityAction_1__ctor_m8CACADCAC18230FB18DF7A6BEC3D9EAD93FEDC3B_gshared)(__this, ___object0, ___method1, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
inline void UnityEvent_1_AddListener_mA73838FBF3836695F5183B32B797E9499BA5E59C (UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC * __this, UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB * ___call0, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC *, UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB *, const RuntimeMethod*))UnityEvent_1_AddListener_mA73838FBF3836695F5183B32B797E9499BA5E59C_gshared)(__this, ___call0, method);
}
// System.Single UnityEngine.Input::GetAxis(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326 (String_t* ___axisName0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Input::get_touchCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Input_get_touchCount_mE1A06AB1973E3456AE398B3CC5105F27CC7335D6 (const RuntimeMethod* method);
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C  Input_GetTouch_m6A2A31482B1A7D018C3AAC188C02F5D14500C81F (int32_t ___index0, const RuntimeMethod* method);
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Touch_get_phase_m576EA3F4FE1D12EB85510326AD8EC3C2EB267257 (Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C * __this, const RuntimeMethod* method);
// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.EventSystem::get_current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * EventSystem_get_current_m4B9C11F490297AE55428038DACD240596D6CE5F2 (const RuntimeMethod* method);
// System.Int32 UnityEngine.Touch::get_fingerId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Touch_get_fingerId_mCED0E66949120E69BFE9294DC0A11A6F9FDBD129 (Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.EventSystems.EventSystem::IsPointerOverGameObject(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool EventSystem_IsPointerOverGameObject_mE7043E54617B8289C81A1C7342FBE0AE448C9E3A (EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * __this, int32_t ___pointerId0, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Camera_WorldToScreenPoint_m44710195E7736CE9DE5A9B05E32059A9A950F95C (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Touch_get_position_mE32B04C6DA32A0965C403A31847ED7F1725EA1DE (Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Touch::get_deltaPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Touch_get_deltaPosition_mF9D60C253E41DC4E4F832F88A1041BE8A9E7C0FB (Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Subtraction_m6E536A8C72FEAA37FF8D5E26E11D6E71EB59599A_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::SignedAngle(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector2_SignedAngle_m007FAC4E36153EEBC62347D6FA67162238C34C69 (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___from0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___to1, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<Card>()
inline CardU5BU5D_t32023BF72E2560CD1EA88DF7C04357E784A64CE0* Component_GetComponentsInChildren_TisCard_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A_m27B008E50F6CADD5BDD8F25A3D8D73709C8A1577 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  CardU5BU5D_t32023BF72E2560CD1EA88DF7C04357E784A64CE0* (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_mCA5B356D4B0824C6DE60A8E90E6A6D4188C56C2F_gshared)(__this, method);
}
// System.Int32 UnityEngine.Transform::get_childCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Application::Quit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Application_Quit_m8D720E5092786C2EE32310D85FE61C253D3B1F2A (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddForce_m78B9D94F505E19F3C63461362AD6DE7EA0836700 (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___force0, int32_t ___mode1, const RuntimeMethod* method);
// System.Void Player::CueMove(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_CueMove_m99C0CC666F9E6AF125FC0866717BE649BA192961 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, float ___sl0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50 (const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_m2AA745C4A796363462642A13251E8971D5C7F4DC (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___axis0, float ___angle1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ScriptableObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableObject__ctor_m8DAE6CDCFA34E16F2543B02CC3669669FF203063 (ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Ball>()
inline Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * Component_GetComponent_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m564BDAF1415831A7751D8FFD48AEFAAEB60ACF9D (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Int32 System.Collections.Generic.List`1<Ball>::get_Count()
inline int32_t List_1_get_Count_mED876C00B350B9333120130A2EE72901165DA7CB_inline (List_1_t704724B962CF6005382C610663175BE693752D5D * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t704724B962CF6005382C610663175BE693752D5D *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Void GameManager::SetPlayers(Ball)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_SetPlayers_mCFA2D98B19876D41E1DC50B557F77DA74C5973DB (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * ___ball0, const RuntimeMethod* method);
// System.Void Table::RemoveBall(Ball)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Table_RemoveBall_m02A9A5675D99552809A1869FF89AFE3E0FC9C6DE (Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * __this, Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * ___ball0, const RuntimeMethod* method);
// UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * Collider_get_attachedRigidbody_m101FED12AD292F372F98E94A6D02A5E428AA896A (Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6 (const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline (Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___call0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::ProjectOnPlane(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_ProjectOnPlane_m066BDEFD60B2828C4B531CD96C4DBFADF6B0EF3B (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___vector0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___planeNormal1, const RuntimeMethod* method);
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ray__ctor_m75B1F651FF47EE6B887105101B7DA61CBF41F83C (Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___origin0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_SphereCast_mF32FCDFC17C9B4A1F9D55729A54504A82567F065 (Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6  ___ray0, float ___radius1, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * ___hitInfo2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  RaycastHit_get_normal_m2C813B25BAECD87FD9E9CB294278B291F4CC6674 (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::SignedAngle(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_SignedAngle_m816C32A674665A4C3C9D850FB0A107E69A4D3E0A (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___from0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___to1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___axis2, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Sign(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Sign_m01716387C82B9523CFFADED7B2037D75F57FE2FB (float ___f0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_AngleAxis_m4644D20F58ADF03E9EA297CB4A845E5BCDA1E398 (float ___angle0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___axis1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Quaternion_op_Multiply_mDC5F913E6B21FEC72AB2CF737D34CC6C7A69803D (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___point1, const RuntimeMethod* method);
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.LineRenderer::SetPosition(System.Int32,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LineRenderer_SetPosition_mD37DBE4B3E13A838FFD09289BC77DEDB423D620F (LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * __this, int32_t ___index0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, const RuntimeMethod* method);
// System.Void System.Nullable`1<BallsType>::.ctor(!0)
inline void Nullable_1__ctor_mD66CEE733802251058E31FBEF0840C7A9D4DA406 (Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB * __this, int32_t ___value0, const RuntimeMethod* method)
{
	((  void (*) (Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB *, int32_t, const RuntimeMethod*))Nullable_1__ctor_m904114DBE44D14D291456629D60BF66ECA75BB42_gshared)(__this, ___value0, method);
}
// !0 System.Nullable`1<BallsType>::GetValueOrDefault()
inline int32_t Nullable_1_GetValueOrDefault_mE9A6BC0C579094139424C3A07555F05CFE4CA73B_inline (Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB *, const RuntimeMethod*))Nullable_1_GetValueOrDefault_mA8DDAB2C6553ED7FFC9A55E1A92A96B3571000EC_gshared_inline)(__this, method);
}
// System.Boolean System.Nullable`1<BallsType>::get_HasValue()
inline bool Nullable_1_get_HasValue_mCB87A6CB3CF78384E3F13C909FDB8AD3659DF6CD_inline (Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB *, const RuntimeMethod*))Nullable_1_get_HasValue_m4C033F49F5318E94BC8CBA9CE5175EFDBFADEF9C_gshared_inline)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshRenderer>()
inline MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_set_material_m8DED7F4F7AF38755C3D7DAFDD613BBE1AAB941BA (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline (float ___d0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a1, const RuntimeMethod* method);
// System.Single GameManager::get_Clock()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float GameManager_get_Clock_m32723273A7CCC5919901AC0AA016C8F90E90A5D6_inline (const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_fixedDeltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_fixedDeltaTime_m8E94ECFF6A6A1D9B5D60BF82D116D540852484E5 (const RuntimeMethod* method);
// System.Void Table::CheckState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Table_CheckState_mDF18708BD88BB608A70BF3CD22B86CF4BC0DF817 (Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_angularVelocity(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_angularVelocity_m3A40B7F195E9E217AE29A0964D7E7540E2E23080 (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void GameManager::ChangePlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_ChangePlayer_m0F30608B17D39CD119A0AC533BF3064E24D79BEF (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Rigidbody_get_velocity_mCFB033F3BD14C2BA68E797DFA4950F9307EC8E2C (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, const RuntimeMethod* method);
// System.Boolean System.Linq.Enumerable::All<Ball>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
inline bool Enumerable_All_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_mD11F2F4E2660C09A1E8513BC3A3B926D37D22996 (RuntimeObject* ___source0, Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * ___predicate1, const RuntimeMethod* method)
{
	return ((  bool (*) (RuntimeObject*, Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 *, const RuntimeMethod*))Enumerable_All_TisRuntimeObject_mAB7DDBA2A721E5E08769CDBFD1E9FA658DD14989_gshared)(___source0, ___predicate1, method);
}
// System.Boolean System.Linq.Enumerable::Any<Ball>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
inline bool Enumerable_Any_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_mB31A84E03C801D4A081D5A31CB5197BE99EE5B75 (RuntimeObject* ___source0, Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * ___predicate1, const RuntimeMethod* method)
{
	return ((  bool (*) (RuntimeObject*, Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 *, const RuntimeMethod*))Enumerable_Any_TisRuntimeObject_m79E5BEA29E663B44BB9C1BDD62286D214D22E600_gshared)(___source0, ___predicate1, method);
}
// System.Void GameManager::EndGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_EndGame_m34CB0E063C72D2D7BA815B5397C5DB865EE60810 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<Ball>::Remove(!0)
inline bool List_1_Remove_mE5E9D8A568D8853924165476644A9B523F98B554 (List_1_t704724B962CF6005382C610663175BE693752D5D * __this, Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t704724B962CF6005382C610663175BE693752D5D *, Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D *, const RuntimeMethod*))List_1_Remove_m753F7B4281CC4D02C07AE90726F51EF34B588DF7_gshared)(__this, ___item0, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.GameObject>::GetEnumerator()
inline Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6 (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, const RuntimeMethod*))List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::get_Current()
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_inline (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 * __this, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::MoveNext()
inline bool Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7 (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::Dispose()
inline void Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9 (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// System.Void BotAI/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mE1CF8C72D5472ED37ED79206ECD6241080ECC695 (U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD * __this, const RuntimeMethod* method);
// System.Void Table/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m857A70135E5C6368471096430F715C195059B579 (U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// BallsType Ball::Type()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Ball_Type_mB4092D6071D5B95B0EB1FB883A3D2EC4A3663FF6 (Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * __this, const RuntimeMethod* method)
{
	{
		// if (Number < 8) return BallsType.Solids;
		int32_t L_0 = __this->get_Number_4();
		if ((((int32_t)L_0) >= ((int32_t)8)))
		{
			goto IL_000b;
		}
	}
	{
		// if (Number < 8) return BallsType.Solids;
		return (int32_t)(0);
	}

IL_000b:
	{
		// else if (Number > 8) return BallsType.Stripes;
		int32_t L_1 = __this->get_Number_4();
		if ((((int32_t)L_1) <= ((int32_t)8)))
		{
			goto IL_0016;
		}
	}
	{
		// else if (Number > 8) return BallsType.Stripes;
		return (int32_t)(1);
	}

IL_0016:
	{
		// else return BallsType.Eight;
		return (int32_t)(2);
	}
}
// System.Void Ball::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ball_Start_m218869060D38F7477CC98EBB540C6015DB639629 (Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ball_Start_m218869060D38F7477CC98EBB540C6015DB639629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Rigidbody = GetComponent<Rigidbody>();
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_0 = Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE_RuntimeMethod_var);
		__this->set_Rigidbody_5(L_0);
		// }
		return;
	}
}
// System.Void Ball::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ball__ctor_m76D6811A7C300B95F347A08A5731B731A60E1D25 (Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BotAI::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BotAI_Start_m59999AA275904347DE60C1C9B5C3FADA83BDD61A (BotAI_t029234FCF3F2826C2123CD1BF8F65D212B156F80 * __this, const RuntimeMethod* method)
{
	{
		// GameManager.Instance().remotePlayer.playerController = this;
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_1 = L_0->get_remotePlayer_25();
		NullCheck(L_1);
		L_1->set_playerController_1(__this);
		// _player = GameManager.Instance().remotePlayer;
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_2 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_2);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_3 = L_2->get_remotePlayer_25();
		((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 *)__this)->set__player_4(L_3);
		// if (GameManager.Instance().currentPlayer != _player)
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_4 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_4);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_5 = L_4->get_currentPlayer_26();
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_6 = ((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 *)__this)->get__player_4();
		if ((((RuntimeObject*)(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F *)L_5) == ((RuntimeObject*)(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F *)L_6)))
		{
			goto IL_0039;
		}
	}
	{
		// this.enabled = false;
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0039:
	{
		// }
		return;
	}
}
// System.Void BotAI::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BotAI_FixedUpdate_m73C76A9986C5874E7B70D34BC5561DFD5B0FAA89 (BotAI_t029234FCF3F2826C2123CD1BF8F65D212B156F80 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BotAI_FixedUpdate_m73C76A9986C5874E7B70D34BC5561DFD5B0FAA89_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		// if (GameManager.state == GameState.Aim)
		int32_t L_0 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get_state_5();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_013d;
		}
	}
	{
		// CheckBalls();
		BotAI_CheckBalls_mCE475ACCFD1A63095C82F4106233D52E16371AB6(__this, /*hidden argument*/NULL);
		// if (aims.Count == 0)
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_1 = __this->get_aims_6();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_1, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		if (L_2)
		{
			goto IL_0055;
		}
	}
	{
		// _player.CueRotate(Random.Range(10, 360));
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_3 = ((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 *)__this)->get__player_4();
		int32_t L_4 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(((int32_t)10), ((int32_t)360), /*hidden argument*/NULL);
		NullCheck(L_3);
		Player_CueRotate_m531643FEA219030BAD2FE999897D386DBD49FDD1(L_3, (((float)((float)L_4))), /*hidden argument*/NULL);
		// _player.forceMul = Random.Range(0.1f, 1f);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_5 = ((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 *)__this)->get__player_4();
		float L_6 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((0.100000001f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->set_forceMul_3(L_6);
		// }
		goto IL_011c;
	}

IL_0055:
	{
		// Vector3 aim = aims[Random.Range(0, aims.Count)];
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_7 = __this->get_aims_6();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_8 = __this->get_aims_6();
		NullCheck(L_8);
		int32_t L_9 = List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_inline(L_8, /*hidden argument*/List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_RuntimeMethod_var);
		int32_t L_10 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_inline(L_7, L_10, /*hidden argument*/List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_RuntimeMethod_var);
		V_0 = L_11;
		// GameManager.Instance().currentTable.hitPoint.rotation = Quaternion.LookRotation(aim);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_12 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_12);
		Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_13 = L_12->get_currentTable_23();
		NullCheck(L_13);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14 = L_13->get_hitPoint_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_il2cpp_TypeInfo_var);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_16 = Quaternion_LookRotation_m1B0BEBEBCC384324A6771B9EAC89761F73E1D6BF(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_14, L_16, /*hidden argument*/NULL);
		// float eps = (5 - _player.data.Aim) * 0.5f;
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_17 = ((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 *)__this)->get__player_4();
		NullCheck(L_17);
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_18 = L_17->get_data_0();
		NullCheck(L_18);
		int32_t L_19 = L_18->get_Aim_7();
		V_1 = ((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)5, (int32_t)L_19))))), (float)(0.5f)));
		// _player.CueRotate(Random.Range(-eps, eps));
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_20 = ((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 *)__this)->get__player_4();
		float L_21 = V_1;
		float L_22 = V_1;
		float L_23 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(((-L_21)), L_22, /*hidden argument*/NULL);
		NullCheck(L_20);
		Player_CueRotate_m531643FEA219030BAD2FE999897D386DBD49FDD1(L_20, L_23, /*hidden argument*/NULL);
		// float calculatedMul = (aim.magnitude / 25f) + Random.Range(-0.05f*(10 - _player.data.Force), 0.05f * (10 - _player.data.Force));
		float L_24 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_25 = ((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 *)__this)->get__player_4();
		NullCheck(L_25);
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_26 = L_25->get_data_0();
		NullCheck(L_26);
		int32_t L_27 = L_26->get_Force_6();
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_28 = ((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 *)__this)->get__player_4();
		NullCheck(L_28);
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_29 = L_28->get_data_0();
		NullCheck(L_29);
		int32_t L_30 = L_29->get_Force_6();
		float L_31 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(((float)il2cpp_codegen_multiply((float)(-0.0500000007f), (float)(((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)10), (int32_t)L_27))))))), ((float)il2cpp_codegen_multiply((float)(0.0500000007f), (float)(((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)10), (int32_t)L_30))))))), /*hidden argument*/NULL);
		V_2 = ((float)il2cpp_codegen_add((float)((float)((float)L_24/(float)(25.0f))), (float)L_31));
		// _player.forceMul = Mathf.Clamp(calculatedMul, 0.05f, 1f);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_32 = ((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 *)__this)->get__player_4();
		float L_33 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_il2cpp_TypeInfo_var);
		float L_34 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_33, (0.0500000007f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_32);
		L_32->set_forceMul_3(L_34);
	}

IL_011c:
	{
		// _player.CueHit();
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_35 = ((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 *)__this)->get__player_4();
		NullCheck(L_35);
		Player_CueHit_m5EB2329180A3544C0DC71D37CA3793D9FB446A97(L_35, /*hidden argument*/NULL);
		// aims.Clear();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_36 = __this->get_aims_6();
		NullCheck(L_36);
		List_1_Clear_mE0F03A2E42E2F7F8A282AE01C12945F7379DC702(L_36, /*hidden argument*/List_1_Clear_mE0F03A2E42E2F7F8A282AE01C12945F7379DC702_RuntimeMethod_var);
		// balls.Clear();
		List_1_t704724B962CF6005382C610663175BE693752D5D * L_37 = __this->get_balls_7();
		NullCheck(L_37);
		List_1_Clear_m5A43FA83CABB71CFD6FA538507B7F1D460BA0A76(L_37, /*hidden argument*/List_1_Clear_m5A43FA83CABB71CFD6FA538507B7F1D460BA0A76_RuntimeMethod_var);
	}

IL_013d:
	{
		// }
		return;
	}
}
// System.Void BotAI::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BotAI_OnEnable_m1B28DC499C4A75541B0CFC34770D35B2AD35CCD9 (BotAI_t029234FCF3F2826C2123CD1BF8F65D212B156F80 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BotAI_OnEnable_m1B28DC499C4A75541B0CFC34770D35B2AD35CCD9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// aims = new List<Vector3>();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_0 = (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 *)il2cpp_codegen_object_new(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181_il2cpp_TypeInfo_var);
		List_1__ctor_mF8F23D572031748AD428623AE16803455997E297(L_0, /*hidden argument*/List_1__ctor_mF8F23D572031748AD428623AE16803455997E297_RuntimeMethod_var);
		__this->set_aims_6(L_0);
		// balls = new List<Ball>();
		List_1_t704724B962CF6005382C610663175BE693752D5D * L_1 = (List_1_t704724B962CF6005382C610663175BE693752D5D *)il2cpp_codegen_object_new(List_1_t704724B962CF6005382C610663175BE693752D5D_il2cpp_TypeInfo_var);
		List_1__ctor_m50441437BE40ED72A3F37812FBDC7B631EDCE80D(L_1, /*hidden argument*/List_1__ctor_m50441437BE40ED72A3F37812FBDC7B631EDCE80D_RuntimeMethod_var);
		__this->set_balls_7(L_1);
		// CheckBalls();
		BotAI_CheckBalls_mCE475ACCFD1A63095C82F4106233D52E16371AB6(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BotAI::CheckBalls()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BotAI_CheckBalls_mCE475ACCFD1A63095C82F4106233D52E16371AB6 (BotAI_t029234FCF3F2826C2123CD1BF8F65D212B156F80 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BotAI_CheckBalls_mCE475ACCFD1A63095C82F4106233D52E16371AB6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Enumerator_t078AC40DBA49748835454B10BFD699FDDE55CB4C  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 * V_2 = NULL;
	Enumerator_tD4E53BC75AF1AB2047F41AD591569A06B2EFC316  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * V_4 = NULL;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_5;
	memset((&V_5), 0, sizeof(V_5));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_6;
	memset((&V_6), 0, sizeof(V_6));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_7;
	memset((&V_7), 0, sizeof(V_7));
	float V_8 = 0.0f;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * G_B3_0 = NULL;
	List_1_t704724B962CF6005382C610663175BE693752D5D * G_B3_1 = NULL;
	BotAI_t029234FCF3F2826C2123CD1BF8F65D212B156F80 * G_B3_2 = NULL;
	Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * G_B2_0 = NULL;
	List_1_t704724B962CF6005382C610663175BE693752D5D * G_B2_1 = NULL;
	BotAI_t029234FCF3F2826C2123CD1BF8F65D212B156F80 * G_B2_2 = NULL;
	{
		// aims.Clear();
		List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_0 = __this->get_aims_6();
		NullCheck(L_0);
		List_1_Clear_mE0F03A2E42E2F7F8A282AE01C12945F7379DC702(L_0, /*hidden argument*/List_1_Clear_mE0F03A2E42E2F7F8A282AE01C12945F7379DC702_RuntimeMethod_var);
		// balls.Clear();
		List_1_t704724B962CF6005382C610663175BE693752D5D * L_1 = __this->get_balls_7();
		NullCheck(L_1);
		List_1_Clear_m5A43FA83CABB71CFD6FA538507B7F1D460BA0A76(L_1, /*hidden argument*/List_1_Clear_m5A43FA83CABB71CFD6FA538507B7F1D460BA0A76_RuntimeMethod_var);
		// if (GameManager.Instance().currentTable.Balls.Count() == 15)
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_2 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_2);
		Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_3 = L_2->get_currentTable_23();
		NullCheck(L_3);
		List_1_t704724B962CF6005382C610663175BE693752D5D * L_4 = L_3->get_Balls_11();
		int32_t L_5 = Enumerable_Count_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m3F3E48596ECCC77DD571F837FE7C1AD0A9A43244(L_4, /*hidden argument*/Enumerable_Count_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m3F3E48596ECCC77DD571F837FE7C1AD0A9A43244_RuntimeMethod_var);
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_006e;
		}
	}
	{
		// balls = GameManager.Instance().currentTable.Balls.Where(b => b.Number != 8).ToList();
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_6 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_6);
		Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_7 = L_6->get_currentTable_23();
		NullCheck(L_7);
		List_1_t704724B962CF6005382C610663175BE693752D5D * L_8 = L_7->get_Balls_11();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD_il2cpp_TypeInfo_var);
		Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * L_9 = ((U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD_il2cpp_TypeInfo_var))->get_U3CU3E9__8_0_1();
		Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * L_10 = L_9;
		G_B2_0 = L_10;
		G_B2_1 = L_8;
		G_B2_2 = __this;
		if (L_10)
		{
			G_B3_0 = L_10;
			G_B3_1 = L_8;
			G_B3_2 = __this;
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD_il2cpp_TypeInfo_var);
		U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD * L_11 = ((U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * L_12 = (Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 *)il2cpp_codegen_object_new(Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44_il2cpp_TypeInfo_var);
		Func_2__ctor_m7141D4C33E399BD20EB8B17781BACB3B85C669C6(L_12, L_11, (intptr_t)((intptr_t)U3CU3Ec_U3CCheckBallsU3Eb__8_0_mFB3E009B3F91CC5FF9957507C4FF2A7A82EC8953_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m7141D4C33E399BD20EB8B17781BACB3B85C669C6_RuntimeMethod_var);
		Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * L_13 = L_12;
		((U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD_il2cpp_TypeInfo_var))->set_U3CU3E9__8_0_1(L_13);
		G_B3_0 = L_13;
		G_B3_1 = G_B2_1;
		G_B3_2 = G_B2_2;
	}

IL_005d:
	{
		RuntimeObject* L_14 = Enumerable_Where_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m0D5793A8B17C8583062257D58FAB67E4EB4D15DD(G_B3_1, G_B3_0, /*hidden argument*/Enumerable_Where_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m0D5793A8B17C8583062257D58FAB67E4EB4D15DD_RuntimeMethod_var);
		List_1_t704724B962CF6005382C610663175BE693752D5D * L_15 = Enumerable_ToList_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m8B14924409A741E59EEBD43F57AE8C533328E205(L_14, /*hidden argument*/Enumerable_ToList_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m8B14924409A741E59EEBD43F57AE8C533328E205_RuntimeMethod_var);
		NullCheck(G_B3_2);
		G_B3_2->set_balls_7(L_15);
		goto IL_0099;
	}

IL_006e:
	{
		// balls = GameManager.Instance().currentTable.Balls.Where(b => b.Type() == _player.ballsType).ToList();
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_16 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_16);
		Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_17 = L_16->get_currentTable_23();
		NullCheck(L_17);
		List_1_t704724B962CF6005382C610663175BE693752D5D * L_18 = L_17->get_Balls_11();
		Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * L_19 = (Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 *)il2cpp_codegen_object_new(Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44_il2cpp_TypeInfo_var);
		Func_2__ctor_m7141D4C33E399BD20EB8B17781BACB3B85C669C6(L_19, __this, (intptr_t)((intptr_t)BotAI_U3CCheckBallsU3Eb__8_1_m4176A9C096EA7AC51A35B69EA3BD9BDE3DBB9F84_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m7141D4C33E399BD20EB8B17781BACB3B85C669C6_RuntimeMethod_var);
		RuntimeObject* L_20 = Enumerable_Where_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m0D5793A8B17C8583062257D58FAB67E4EB4D15DD(L_18, L_19, /*hidden argument*/Enumerable_Where_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m0D5793A8B17C8583062257D58FAB67E4EB4D15DD_RuntimeMethod_var);
		List_1_t704724B962CF6005382C610663175BE693752D5D * L_21 = Enumerable_ToList_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m8B14924409A741E59EEBD43F57AE8C533328E205(L_20, /*hidden argument*/Enumerable_ToList_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m8B14924409A741E59EEBD43F57AE8C533328E205_RuntimeMethod_var);
		__this->set_balls_7(L_21);
	}

IL_0099:
	{
		// foreach (Pocket pocket in GameManager.Instance().currentTable.pockets)
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_22 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_22);
		Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_23 = L_22->get_currentTable_23();
		NullCheck(L_23);
		List_1_tACCE341B6C95A0371868772081F93BD709EA0FD2 * L_24 = L_23->get_pockets_12();
		NullCheck(L_24);
		Enumerator_t078AC40DBA49748835454B10BFD699FDDE55CB4C  L_25 = List_1_GetEnumerator_m389535AFAD3A7E50E8EBCC6C5AF2BEFBC4208E62(L_24, /*hidden argument*/List_1_GetEnumerator_m389535AFAD3A7E50E8EBCC6C5AF2BEFBC4208E62_RuntimeMethod_var);
		V_1 = L_25;
	}

IL_00ae:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0309;
		}

IL_00b3:
		{
			// foreach (Pocket pocket in GameManager.Instance().currentTable.pockets)
			Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 * L_26 = Enumerator_get_Current_mB806EC855AC481BB6CB9DCF8A727E583A36F8817_inline((Enumerator_t078AC40DBA49748835454B10BFD699FDDE55CB4C *)(&V_1), /*hidden argument*/Enumerator_get_Current_mB806EC855AC481BB6CB9DCF8A727E583A36F8817_RuntimeMethod_var);
			V_2 = L_26;
			// foreach (Ball ball in balls)
			List_1_t704724B962CF6005382C610663175BE693752D5D * L_27 = __this->get_balls_7();
			NullCheck(L_27);
			Enumerator_tD4E53BC75AF1AB2047F41AD591569A06B2EFC316  L_28 = List_1_GetEnumerator_m3049E16624476F2B193E0E3D8A77289A30598E05(L_27, /*hidden argument*/List_1_GetEnumerator_m3049E16624476F2B193E0E3D8A77289A30598E05_RuntimeMethod_var);
			V_3 = L_28;
		}

IL_00c7:
		try
		{ // begin try (depth: 2)
			{
				goto IL_02ed;
			}

IL_00cc:
			{
				// foreach (Ball ball in balls)
				Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_29 = Enumerator_get_Current_mC7B26CA370CBDEC8C71772629C62C5940FF5F825_inline((Enumerator_tD4E53BC75AF1AB2047F41AD591569A06B2EFC316 *)(&V_3), /*hidden argument*/Enumerator_get_Current_mC7B26CA370CBDEC8C71772629C62C5940FF5F825_RuntimeMethod_var);
				V_4 = L_29;
				// Physics.SphereCast(ball.transform.position, 0.285f, pocket.transform.position - ball.transform.position, out hit, (pocket.transform.position - ball.transform.position).magnitude);
				Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_30 = V_4;
				NullCheck(L_30);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_31 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_30, /*hidden argument*/NULL);
				NullCheck(L_31);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_31, /*hidden argument*/NULL);
				Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 * L_33 = V_2;
				NullCheck(L_33);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_34 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_33, /*hidden argument*/NULL);
				NullCheck(L_34);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_34, /*hidden argument*/NULL);
				Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_36 = V_4;
				NullCheck(L_36);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_37 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_36, /*hidden argument*/NULL);
				NullCheck(L_37);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_38 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_37, /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_39 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_35, L_38, /*hidden argument*/NULL);
				Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 * L_40 = V_2;
				NullCheck(L_40);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_41 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_40, /*hidden argument*/NULL);
				NullCheck(L_41);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_42 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_41, /*hidden argument*/NULL);
				Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_43 = V_4;
				NullCheck(L_43);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_44 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_43, /*hidden argument*/NULL);
				NullCheck(L_44);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_45 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_44, /*hidden argument*/NULL);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_46 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_42, L_45, /*hidden argument*/NULL);
				V_5 = L_46;
				float L_47 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_5), /*hidden argument*/NULL);
				Physics_SphereCast_m2E200DFD71A597968D4F50B8A6284F2E622B4C57(L_32, (0.284999996f), L_39, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), L_47, /*hidden argument*/NULL);
				// if (hit.collider == null)
				Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_48 = RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
				bool L_49 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_48, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
				if (!L_49)
				{
					goto IL_02ed;
				}
			}

IL_0141:
			{
				// Vector3 ph = ball.transform.position + (ball.transform.position - pocket.transform.position).normalized * 0.57f; // phantom COORDS for THAT ball-pocket hit
				Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_50 = V_4;
				NullCheck(L_50);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_51 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_50, /*hidden argument*/NULL);
				NullCheck(L_51);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_52 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_51, /*hidden argument*/NULL);
				Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_53 = V_4;
				NullCheck(L_53);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_54 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_53, /*hidden argument*/NULL);
				NullCheck(L_54);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_55 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_54, /*hidden argument*/NULL);
				Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 * L_56 = V_2;
				NullCheck(L_56);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_57 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_56, /*hidden argument*/NULL);
				NullCheck(L_57);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_58 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_57, /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_59 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_55, L_58, /*hidden argument*/NULL);
				V_5 = L_59;
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_60 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_5), /*hidden argument*/NULL);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_61 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_60, (0.569999993f), /*hidden argument*/NULL);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_62 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_52, L_61, /*hidden argument*/NULL);
				V_6 = L_62;
				// Vector3 cueToBallHit = -(GameManager.Instance().currentTable.CueBall.transform.position - ph);
				GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_63 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
				NullCheck(L_63);
				Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_64 = L_63->get_currentTable_23();
				NullCheck(L_64);
				Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_65 = L_64->get_CueBall_9();
				NullCheck(L_65);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_66 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_65, /*hidden argument*/NULL);
				NullCheck(L_66);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_67 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_66, /*hidden argument*/NULL);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_68 = V_6;
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_69 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_67, L_68, /*hidden argument*/NULL);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_70 = Vector3_op_UnaryNegation_m362EA356F4CADEDB39F965A0DBDED6EA890925F7_inline(L_69, /*hidden argument*/NULL);
				V_7 = L_70;
				// Physics.SphereCast(GameManager.Instance().currentTable.CueBall.transform.position, 0.285f, cueToBallHit, out hit);
				GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_71 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
				NullCheck(L_71);
				Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_72 = L_71->get_currentTable_23();
				NullCheck(L_72);
				Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_73 = L_72->get_CueBall_9();
				NullCheck(L_73);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_74 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_73, /*hidden argument*/NULL);
				NullCheck(L_74);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_75 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_74, /*hidden argument*/NULL);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_76 = V_7;
				Physics_SphereCast_m49360D9368FF36611C0E5E73B56A8FDF9638E2E3(L_75, (0.284999996f), L_76, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), /*hidden argument*/NULL);
				// if (hit.collider && hit.collider.gameObject == ball.gameObject && Vector3.Angle(GameManager.Instance().currentTable.CueBall.transform.position - ball.transform.position, pocket.transform.position - ball.transform.position) > 90f)
				Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_77 = RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
				bool L_78 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_77, /*hidden argument*/NULL);
				if (!L_78)
				{
					goto IL_02ed;
				}
			}

IL_01e3:
			{
				Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_79 = RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), /*hidden argument*/NULL);
				NullCheck(L_79);
				GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_80 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_79, /*hidden argument*/NULL);
				Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_81 = V_4;
				NullCheck(L_81);
				GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_82 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_81, /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
				bool L_83 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_80, L_82, /*hidden argument*/NULL);
				if (!L_83)
				{
					goto IL_02ed;
				}
			}

IL_0200:
			{
				GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_84 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
				NullCheck(L_84);
				Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_85 = L_84->get_currentTable_23();
				NullCheck(L_85);
				Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_86 = L_85->get_CueBall_9();
				NullCheck(L_86);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_87 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_86, /*hidden argument*/NULL);
				NullCheck(L_87);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_88 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_87, /*hidden argument*/NULL);
				Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_89 = V_4;
				NullCheck(L_89);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_90 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_89, /*hidden argument*/NULL);
				NullCheck(L_90);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_91 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_90, /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_92 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_88, L_91, /*hidden argument*/NULL);
				Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 * L_93 = V_2;
				NullCheck(L_93);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_94 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_93, /*hidden argument*/NULL);
				NullCheck(L_94);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_95 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_94, /*hidden argument*/NULL);
				Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_96 = V_4;
				NullCheck(L_96);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_97 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_96, /*hidden argument*/NULL);
				NullCheck(L_97);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_98 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_97, /*hidden argument*/NULL);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_99 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_95, L_98, /*hidden argument*/NULL);
				float L_100 = Vector3_Angle_m3715AB03A36C59D8CF08F8D71E2F46454EB884C1(L_92, L_99, /*hidden argument*/NULL);
				if ((!(((float)L_100) > ((float)(90.0f)))))
				{
					goto IL_02ed;
				}
			}

IL_0255:
			{
				// Debug.DrawLine(ball.transform.position, pocket.transform.position, Color.green, 10f);
				Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_101 = V_4;
				NullCheck(L_101);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_102 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_101, /*hidden argument*/NULL);
				NullCheck(L_102);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_103 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_102, /*hidden argument*/NULL);
				Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 * L_104 = V_2;
				NullCheck(L_104);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_105 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_104, /*hidden argument*/NULL);
				NullCheck(L_105);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_106 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_105, /*hidden argument*/NULL);
				Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_107 = Color_get_green_mFF9BD42534D385A0717B1EAD083ADF08712984B9(/*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
				Debug_DrawLine_mA85118FC8F0D6DF41CBF0AC95DB19CE2F2DB5149(L_103, L_106, L_107, (10.0f), /*hidden argument*/NULL);
				// Debug.DrawRay(GameManager.Instance().currentTable.CueBall.transform.position, cueToBallHit, Color.red, 10f);
				GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_108 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
				NullCheck(L_108);
				Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_109 = L_108->get_currentTable_23();
				NullCheck(L_109);
				Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_110 = L_109->get_CueBall_9();
				NullCheck(L_110);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_111 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_110, /*hidden argument*/NULL);
				NullCheck(L_111);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_112 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_111, /*hidden argument*/NULL);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_113 = V_7;
				Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_114 = Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8(/*hidden argument*/NULL);
				Debug_DrawRay_m3954B3FFA675C0660ED438E8B705B45EDE393C60(L_112, L_113, L_114, (10.0f), /*hidden argument*/NULL);
				// float mul = cueToBallHit.magnitude + (ball.transform.position - pocket.transform.position).magnitude;
				float L_115 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_7), /*hidden argument*/NULL);
				Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_116 = V_4;
				NullCheck(L_116);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_117 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_116, /*hidden argument*/NULL);
				NullCheck(L_117);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_118 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_117, /*hidden argument*/NULL);
				Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 * L_119 = V_2;
				NullCheck(L_119);
				Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_120 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_119, /*hidden argument*/NULL);
				NullCheck(L_120);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_121 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_120, /*hidden argument*/NULL);
				IL2CPP_RUNTIME_CLASS_INIT(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var);
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_122 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_118, L_121, /*hidden argument*/NULL);
				V_5 = L_122;
				float L_123 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_5), /*hidden argument*/NULL);
				V_8 = ((float)il2cpp_codegen_add((float)L_115, (float)L_123));
				// aims.Add(cueToBallHit.normalized * mul);
				List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * L_124 = __this->get_aims_6();
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_125 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_7), /*hidden argument*/NULL);
				float L_126 = V_8;
				Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_127 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_125, L_126, /*hidden argument*/NULL);
				NullCheck(L_124);
				List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59(L_124, L_127, /*hidden argument*/List_1_Add_mAE131B53917AD7132F6BA2C05D5D17C38C5A2E59_RuntimeMethod_var);
			}

IL_02ed:
			{
				// foreach (Ball ball in balls)
				bool L_128 = Enumerator_MoveNext_mF564EA6451E132F369252AD607F78B570253D320((Enumerator_tD4E53BC75AF1AB2047F41AD591569A06B2EFC316 *)(&V_3), /*hidden argument*/Enumerator_MoveNext_mF564EA6451E132F369252AD607F78B570253D320_RuntimeMethod_var);
				if (L_128)
				{
					goto IL_00cc;
				}
			}

IL_02f9:
			{
				IL2CPP_LEAVE(0x309, FINALLY_02fb);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_02fb;
		}

FINALLY_02fb:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m5FBC6AE392B5BDC7E57DA42B723E6819F1527B0D((Enumerator_tD4E53BC75AF1AB2047F41AD591569A06B2EFC316 *)(&V_3), /*hidden argument*/Enumerator_Dispose_m5FBC6AE392B5BDC7E57DA42B723E6819F1527B0D_RuntimeMethod_var);
			IL2CPP_END_FINALLY(763)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(763)
		{
			IL2CPP_JUMP_TBL(0x309, IL_0309)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_0309:
		{
			// foreach (Pocket pocket in GameManager.Instance().currentTable.pockets)
			bool L_129 = Enumerator_MoveNext_m2503905BF86ABEB00CE962518451A3A95482500B((Enumerator_t078AC40DBA49748835454B10BFD699FDDE55CB4C *)(&V_1), /*hidden argument*/Enumerator_MoveNext_m2503905BF86ABEB00CE962518451A3A95482500B_RuntimeMethod_var);
			if (L_129)
			{
				goto IL_00b3;
			}
		}

IL_0315:
		{
			IL2CPP_LEAVE(0x325, FINALLY_0317);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0317;
	}

FINALLY_0317:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m45DF0A38A6BF81DD15FFAE777BF93F6019D59E4D((Enumerator_t078AC40DBA49748835454B10BFD699FDDE55CB4C *)(&V_1), /*hidden argument*/Enumerator_Dispose_m45DF0A38A6BF81DD15FFAE777BF93F6019D59E4D_RuntimeMethod_var);
		IL2CPP_END_FINALLY(791)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(791)
	{
		IL2CPP_JUMP_TBL(0x325, IL_0325)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0325:
	{
		// }
		return;
	}
}
// System.Void BotAI::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BotAI__ctor_mCB1D349A87788C964257663B0024E89669DA1BE6 (BotAI_t029234FCF3F2826C2123CD1BF8F65D212B156F80 * __this, const RuntimeMethod* method)
{
	{
		PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean BotAI::<CheckBalls>b__8_1(Ball)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool BotAI_U3CCheckBallsU3Eb__8_1_m4176A9C096EA7AC51A35B69EA3BD9BDE3DBB9F84 (BotAI_t029234FCF3F2826C2123CD1BF8F65D212B156F80 * __this, Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * ___b0, const RuntimeMethod* method)
{
	{
		// balls = GameManager.Instance().currentTable.Balls.Where(b => b.Type() == _player.ballsType).ToList();
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_0 = ___b0;
		NullCheck(L_0);
		int32_t L_1 = Ball_Type_mB4092D6071D5B95B0EB1FB883A3D2EC4A3663FF6(L_0, /*hidden argument*/NULL);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_2 = ((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 *)__this)->get__player_4();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_ballsType_2();
		return (bool)((((int32_t)L_1) == ((int32_t)L_3))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Card::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Card_OnEnable_mAC0EE49A6D5A7C4CB47DCA801A74B14190E3BDE1 (Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A * __this, const RuntimeMethod* method)
{
	{
		// force.fillAmount = data.Force * 0.1f;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_0 = __this->get_force_7();
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_1 = __this->get_data_4();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_Force_6();
		NullCheck(L_0);
		Image_set_fillAmount_m1D28CFC9B15A45AB6C561AA42BD8F305605E9E3C(L_0, ((float)il2cpp_codegen_multiply((float)(((float)((float)L_2))), (float)(0.100000001f))), /*hidden argument*/NULL);
		// aim.fillAmount = data.Aim * 0.1f;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_3 = __this->get_aim_8();
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_4 = __this->get_data_4();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_Aim_7();
		NullCheck(L_3);
		Image_set_fillAmount_m1D28CFC9B15A45AB6C561AA42BD8F305605E9E3C(L_3, ((float)il2cpp_codegen_multiply((float)(((float)((float)L_5))), (float)(0.100000001f))), /*hidden argument*/NULL);
		// spin.fillAmount = data.Spin * 0.1f;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_6 = __this->get_spin_9();
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_7 = __this->get_data_4();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_Spin_8();
		NullCheck(L_6);
		Image_set_fillAmount_m1D28CFC9B15A45AB6C561AA42BD8F305605E9E3C(L_6, ((float)il2cpp_codegen_multiply((float)(((float)((float)L_8))), (float)(0.100000001f))), /*hidden argument*/NULL);
		// time.fillAmount = data.Time * 0.1f;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_9 = __this->get_time_10();
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_10 = __this->get_data_4();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_Time_9();
		NullCheck(L_9);
		Image_set_fillAmount_m1D28CFC9B15A45AB6C561AA42BD8F305605E9E3C(L_9, ((float)il2cpp_codegen_multiply((float)(((float)((float)L_11))), (float)(0.100000001f))), /*hidden argument*/NULL);
		// title.text = data.Nickname;
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_12 = __this->get_title_6();
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_13 = __this->get_data_4();
		NullCheck(L_13);
		String_t* L_14 = L_13->get_Nickname_4();
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_14);
		// icon.sprite = data.avatar;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_15 = __this->get_icon_11();
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_16 = __this->get_data_4();
		NullCheck(L_16);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_17 = L_16->get_avatar_5();
		NullCheck(L_15);
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_15, L_17, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Card::Click()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Card_Click_mD38F1C9958610FFD5506DE06BAA9FF9995F22AE1 (Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Card_Click_mD38F1C9958610FFD5506DE06BAA9FF9995F22AE1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// foreach (Transform card in transform.parent)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		RuntimeObject* L_2 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0041;
		}

IL_0013:
		{
			// foreach (Transform card in transform.parent)
			RuntimeObject* L_3 = V_0;
			NullCheck(L_3);
			RuntimeObject * L_4 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_3);
			// card.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
			NullCheck(((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_4, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var)));
			Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_5 = Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB(((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_4, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var)), /*hidden argument*/Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB_RuntimeMethod_var);
			Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_6;
			memset((&L_6), 0, sizeof(L_6));
			Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_6), (1.0f), (1.0f), (1.0f), (0.5f), /*hidden argument*/NULL);
			NullCheck(L_5);
			VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_5, L_6);
		}

IL_0041:
		{
			// foreach (Transform card in transform.parent)
			RuntimeObject* L_7 = V_0;
			NullCheck(L_7);
			bool L_8 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_7);
			if (L_8)
			{
				goto IL_0013;
			}
		}

IL_0049:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_004b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_004b;
	}

FINALLY_004b:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_9 = V_0;
			V_1 = ((RuntimeObject*)IsInst((RuntimeObject*)L_9, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_10 = V_1;
			if (!L_10)
			{
				goto IL_005b;
			}
		}

IL_0055:
		{
			RuntimeObject* L_11 = V_1;
			NullCheck(L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_11);
		}

IL_005b:
		{
			IL2CPP_END_FINALLY(75)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(75)
	{
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_005c:
	{
		// GetComponent<Image>().color = new Color(1, 1, 0, 0.5f);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_12 = Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB(__this, /*hidden argument*/Component_GetComponent_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m16EE05A2EC191674136625164C3D3B0162E2FBBB_RuntimeMethod_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_13;
		memset((&L_13), 0, sizeof(L_13));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_13), (1.0f), (1.0f), (0.0f), (0.5f), /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_12, L_13);
		// if (cue)
		bool L_14 = __this->get_cue_5();
		if (!L_14)
		{
			goto IL_009f;
		}
	}
	{
		// GameManager.Instance().localPlayer.data = data;
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_15 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_15);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_16 = L_15->get_localPlayer_24();
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_17 = __this->get_data_4();
		NullCheck(L_16);
		L_16->set_data_0(L_17);
		goto IL_00b4;
	}

IL_009f:
	{
		// GameManager.Instance().remotePlayer.data = data;
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_18 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_18);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_19 = L_18->get_remotePlayer_25();
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_20 = __this->get_data_4();
		NullCheck(L_19);
		L_19->set_data_0(L_20);
	}

IL_00b4:
	{
		// print(data.Nickname);
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_21 = __this->get_data_4();
		NullCheck(L_21);
		String_t* L_22 = L_21->get_Nickname_4();
		MonoBehaviour_print_m4F113B89EC1C221CAC6EC64365E6DAD0AF86F090(L_22, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Card::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Card__ctor_m2ADE17E90583AE44842C04CD1E69AF1CC03DC8A8 (Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// GameManager GameManager::Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static GameManager Instance() { return _instance; }
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get__instance_4();
		return L_0;
	}
}
// System.Void GameManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!_instance) _instance = this;
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get__instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		// if (!_instance) _instance = this;
		((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->set__instance_4(__this);
	}

IL_0012:
	{
		// SceneManager.sceneLoaded += LevelLoaded;
		UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906 * L_2 = (UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906 *)il2cpp_codegen_object_new(UnityAction_2_tDEF0DD47461C853F98CD2FF3CEC4F5EE13A19906_il2cpp_TypeInfo_var);
		UnityAction_2__ctor_mE0417B33CF845A6B9324E67D296ADEA562B91DE0(L_2, __this, (intptr_t)((intptr_t)GameManager_LevelLoaded_m8E33405B1DC924DFCA08EE0D40B8B1187FC0B5DB_RuntimeMethod_var), /*hidden argument*/UnityAction_2__ctor_mE0417B33CF845A6B9324E67D296ADEA562B91DE0_RuntimeMethod_var);
		SceneManager_add_sceneLoaded_m54990A485E2E66739E31090BDC3A4C01EF7729BA(L_2, /*hidden argument*/NULL);
		// DontDestroyOnLoad(this);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9(__this, /*hidden argument*/NULL);
		// DontDestroyOnLoad(hud);
		Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * L_3 = __this->get_hud_11();
		Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9(L_3, /*hidden argument*/NULL);
		// localPlayer = new Player();
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_4 = (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F *)il2cpp_codegen_object_new(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7(L_4, /*hidden argument*/NULL);
		__this->set_localPlayer_24(L_4);
		// remotePlayer = new Player();
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_5 = (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F *)il2cpp_codegen_object_new(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F_il2cpp_TypeInfo_var);
		Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7(L_5, /*hidden argument*/NULL);
		__this->set_remotePlayer_25(L_5);
		// SetForce();
		GameManager_SetForce_m2B0FF265E47640E330E5D78D23FCF62ADC99084A(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Single GameManager::get_Clock()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float GameManager_get_Clock_m32723273A7CCC5919901AC0AA016C8F90E90A5D6 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_get_Clock_m32723273A7CCC5919901AC0AA016C8F90E90A5D6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return _timer; }
		float L_0 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get__timer_6();
		return L_0;
	}
}
// System.Void GameManager::set_Clock(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_set_Clock_m6A52D50A47B356DE092A46E16D85258DAA4338C2 (float ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_set_Clock_m6A52D50A47B356DE092A46E16D85258DAA4338C2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (state == GameState.Aim || state == GameState.ChangeForce || state == GameState.Breaking || state == GameState.NeedChange) _timer = Mathf.Clamp(value, 0, 1000);
		int32_t L_0 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get_state_5();
		if ((((int32_t)L_0) == ((int32_t)3)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_1 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get_state_5();
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_2 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get_state_5();
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get_state_5();
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_0034;
		}
	}

IL_001f:
	{
		// if (state == GameState.Aim || state == GameState.ChangeForce || state == GameState.Breaking || state == GameState.NeedChange) _timer = Mathf.Clamp(value, 0, 1000);
		float L_4 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_il2cpp_TypeInfo_var);
		float L_5 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_4, (0.0f), (1000.0f), /*hidden argument*/NULL);
		((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->set__timer_6(L_5);
	}

IL_0034:
	{
		// if (_timer == 0) { state = GameState.NeedChange; print("by time"); }
		float L_6 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get__timer_6();
		if ((!(((float)L_6) == ((float)(0.0f)))))
		{
			goto IL_0050;
		}
	}
	{
		// if (_timer == 0) { state = GameState.NeedChange; print("by time"); }
		((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->set_state_5(2);
		// if (_timer == 0) { state = GameState.NeedChange; print("by time"); }
		MonoBehaviour_print_m4F113B89EC1C221CAC6EC64365E6DAD0AF86F090(_stringLiteralA9CA1FE109FB54B8BD267CFF6357F4284511A3CA, /*hidden argument*/NULL);
	}

IL_0050:
	{
		// Instance().clockText.text = _timer.ToString("00:00");
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_7 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_7);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_8 = L_7->get_clockText_19();
		String_t* L_9 = Single_ToString_m15F10F2AFF80750906CEFCFB456EBA84F9D2E8D7((float*)(((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get_address_of__timer_6()), _stringLiteral730FB4333540EF71406C57A9DAA8464AE14157D3, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, L_9);
		// }
		return;
	}
}
// System.Void GameManager::LevelLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_LevelLoaded_m8E33405B1DC924DFCA08EE0D40B8B1187FC0B5DB (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  ___scene0, int32_t ___mode1, const RuntimeMethod* method)
{
	{
		// if (scene.buildIndex == 0) SceneManager.LoadScene(1);
		int32_t L_0 = Scene_get_buildIndex_mE32CE766EA0790E4636A351BA353A7FD71A11DA4((Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE *)(&___scene0), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		// if (scene.buildIndex == 0) SceneManager.LoadScene(1);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(1, /*hidden argument*/NULL);
	}

IL_000f:
	{
		// hud.enabled = scene.buildIndex == 2;
		Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * L_1 = __this->get_hud_11();
		int32_t L_2 = Scene_get_buildIndex_mE32CE766EA0790E4636A351BA353A7FD71A11DA4((Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE *)(&___scene0), /*hidden argument*/NULL);
		NullCheck(L_1);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_1, (bool)((((int32_t)L_2) == ((int32_t)2))? 1 : 0), /*hidden argument*/NULL);
		// if (scene.buildIndex == 2)
		int32_t L_3 = Scene_get_buildIndex_mE32CE766EA0790E4636A351BA353A7FD71A11DA4((Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE *)(&___scene0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_0064;
		}
	}
	{
		// remoteAvatar.sprite = remotePlayer.data.avatar;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_4 = __this->get_remoteAvatar_22();
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_5 = __this->get_remotePlayer_25();
		NullCheck(L_5);
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_6 = L_5->get_data_0();
		NullCheck(L_6);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_7 = L_6->get_avatar_5();
		NullCheck(L_4);
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_4, L_7, /*hidden argument*/NULL);
		// localAvatar.sprite = localPlayer.data.avatar;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_8 = __this->get_localAvatar_21();
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_9 = __this->get_localPlayer_24();
		NullCheck(L_9);
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_10 = L_9->get_data_0();
		NullCheck(L_10);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_11 = L_10->get_avatar_5();
		NullCheck(L_8);
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_8, L_11, /*hidden argument*/NULL);
	}

IL_0064:
	{
		// }
		return;
	}
}
// System.Void GameManager::SetPlayers(Ball)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_SetPlayers_mCFA2D98B19876D41E1DC50B557F77DA74C5973DB (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * ___ball0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * G_B3_0 = NULL;
	Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * G_B4_1 = NULL;
	Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * G_B7_0 = NULL;
	Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * G_B6_0 = NULL;
	int32_t G_B8_0 = 0;
	Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * G_B8_1 = NULL;
	{
		// currentPlayer.ballsType = ball.Type();
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_0 = __this->get_currentPlayer_26();
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_1 = ___ball0;
		NullCheck(L_1);
		int32_t L_2 = Ball_Type_mB4092D6071D5B95B0EB1FB883A3D2EC4A3663FF6(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->set_ballsType_2(L_2);
		// if (currentPlayer == remotePlayer)
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_3 = __this->get_currentPlayer_26();
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_4 = __this->get_remotePlayer_25();
		if ((!(((RuntimeObject*)(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F *)L_3) == ((RuntimeObject*)(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F *)L_4))))
		{
			goto IL_0038;
		}
	}
	{
		// localPlayer.ballsType = ball.Type() == BallsType.Solids ? BallsType.Stripes : BallsType.Solids;
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_5 = __this->get_localPlayer_24();
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_6 = ___ball0;
		NullCheck(L_6);
		int32_t L_7 = Ball_Type_mB4092D6071D5B95B0EB1FB883A3D2EC4A3663FF6(L_6, /*hidden argument*/NULL);
		G_B2_0 = L_5;
		if (!L_7)
		{
			G_B3_0 = L_5;
			goto IL_0030;
		}
	}
	{
		G_B4_0 = 0;
		G_B4_1 = G_B2_0;
		goto IL_0031;
	}

IL_0030:
	{
		G_B4_0 = 1;
		G_B4_1 = G_B3_0;
	}

IL_0031:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_ballsType_2(G_B4_0);
		goto IL_004f;
	}

IL_0038:
	{
		// remotePlayer.ballsType = ball.Type() == BallsType.Solids ? BallsType.Stripes : BallsType.Solids;
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_8 = __this->get_remotePlayer_25();
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_9 = ___ball0;
		NullCheck(L_9);
		int32_t L_10 = Ball_Type_mB4092D6071D5B95B0EB1FB883A3D2EC4A3663FF6(L_9, /*hidden argument*/NULL);
		G_B6_0 = L_8;
		if (!L_10)
		{
			G_B7_0 = L_8;
			goto IL_0049;
		}
	}
	{
		G_B8_0 = 0;
		G_B8_1 = G_B6_0;
		goto IL_004a;
	}

IL_0049:
	{
		G_B8_0 = 1;
		G_B8_1 = G_B7_0;
	}

IL_004a:
	{
		NullCheck(G_B8_1);
		G_B8_1->set_ballsType_2(G_B8_0);
	}

IL_004f:
	{
		// for (int i = 1; i < 16; i++)
		V_0 = 1;
		goto IL_0062;
	}

IL_0053:
	{
		// if (i != 8) RegisterBall(i);
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) == ((int32_t)8)))
		{
			goto IL_005e;
		}
	}
	{
		// if (i != 8) RegisterBall(i);
		int32_t L_12 = V_0;
		GameManager_RegisterBall_m33C55F5DAF74AF3A685FC4EC267C3CC4280A5F74(__this, L_12, /*hidden argument*/NULL);
	}

IL_005e:
	{
		// for (int i = 1; i < 16; i++)
		int32_t L_13 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0062:
	{
		// for (int i = 1; i < 16; i++)
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) < ((int32_t)((int32_t)16))))
		{
			goto IL_0053;
		}
	}
	{
		// }
		return;
	}
}
// System.Void GameManager::RegisterBall(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_RegisterBall_m33C55F5DAF74AF3A685FC4EC267C3CC4280A5F74 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, int32_t ___i0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_RegisterBall_m33C55F5DAF74AF3A685FC4EC267C3CC4280A5F74_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass30_0_t9F9560A6034B2661D9C6E093FA21372992814025 * V_0 = NULL;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * V_1 = NULL;
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * V_2 = NULL;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B4_0 = NULL;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B8_0 = NULL;
	{
		U3CU3Ec__DisplayClass30_0_t9F9560A6034B2661D9C6E093FA21372992814025 * L_0 = (U3CU3Ec__DisplayClass30_0_t9F9560A6034B2661D9C6E093FA21372992814025 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass30_0_t9F9560A6034B2661D9C6E093FA21372992814025_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass30_0__ctor_m475CBDDDBF19B84D9ADCAA4A4DFE559EF79DD2CE(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass30_0_t9F9560A6034B2661D9C6E093FA21372992814025 * L_1 = V_0;
		int32_t L_2 = ___i0;
		NullCheck(L_1);
		L_1->set_i_0(L_2);
		// if (i == 8)
		U3CU3Ec__DisplayClass30_0_t9F9560A6034B2661D9C6E093FA21372992814025 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_i_0();
		if ((!(((uint32_t)L_4) == ((uint32_t)8))))
		{
			goto IL_0041;
		}
	}
	{
		// currentPlayer.ballsType = BallsType.Eight;
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_5 = __this->get_currentPlayer_26();
		NullCheck(L_5);
		L_5->set_ballsType_2(2);
		// par = currentPlayer == localPlayer ? deckA : deckB;
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_6 = __this->get_currentPlayer_26();
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_7 = __this->get_localPlayer_24();
		if ((((RuntimeObject*)(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F *)L_6) == ((RuntimeObject*)(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F *)L_7)))
		{
			goto IL_0038;
		}
	}
	{
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_8 = __this->get_deckB_13();
		G_B4_0 = L_8;
		goto IL_003e;
	}

IL_0038:
	{
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_9 = __this->get_deckA_12();
		G_B4_0 = L_9;
	}

IL_003e:
	{
		V_1 = G_B4_0;
		// }
		goto IL_006a;
	}

IL_0041:
	{
		// par = (localPlayer.ballsType == BallsType.Solids ^ i < 8) ? deckB : deckA;
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_10 = __this->get_localPlayer_24();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_ballsType_2();
		U3CU3Ec__DisplayClass30_0_t9F9560A6034B2661D9C6E093FA21372992814025 * L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = L_12->get_i_0();
		if (((int32_t)((int32_t)((((int32_t)L_11) == ((int32_t)0))? 1 : 0)^(int32_t)((((int32_t)L_13) < ((int32_t)8))? 1 : 0))))
		{
			goto IL_0063;
		}
	}
	{
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_14 = __this->get_deckA_12();
		G_B8_0 = L_14;
		goto IL_0069;
	}

IL_0063:
	{
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_15 = __this->get_deckB_13();
		G_B8_0 = L_15;
	}

IL_0069:
	{
		V_1 = G_B8_0;
	}

IL_006a:
	{
		// Image img = Image.Instantiate(ballPrefab, par);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_16 = __this->get_ballPrefab_10();
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_17 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_18 = Object_Instantiate_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m8F5F75D5872251B7E04B6D385D1BDD52DD9F3B5D(L_16, L_17, /*hidden argument*/Object_Instantiate_TisImage_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_m8F5F75D5872251B7E04B6D385D1BDD52DD9F3B5D_RuntimeMethod_var);
		V_2 = L_18;
		// img.sprite = ballIcons[i];
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_19 = V_2;
		List_1_t99B54448C695C6F7103A0DE4320F1A7EF7B30880 * L_20 = __this->get_ballIcons_9();
		U3CU3Ec__DisplayClass30_0_t9F9560A6034B2661D9C6E093FA21372992814025 * L_21 = V_0;
		NullCheck(L_21);
		int32_t L_22 = L_21->get_i_0();
		NullCheck(L_20);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_23 = List_1_get_Item_m2E8D9A40352859B1D5465D074732572FC364786E_inline(L_20, L_22, /*hidden argument*/List_1_get_Item_m2E8D9A40352859B1D5465D074732572FC364786E_RuntimeMethod_var);
		NullCheck(L_19);
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_19, L_23, /*hidden argument*/NULL);
		// currentTable.Balls.First(b => b.Number == i).icons.Add(img.gameObject);
		Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_24 = __this->get_currentTable_23();
		NullCheck(L_24);
		List_1_t704724B962CF6005382C610663175BE693752D5D * L_25 = L_24->get_Balls_11();
		U3CU3Ec__DisplayClass30_0_t9F9560A6034B2661D9C6E093FA21372992814025 * L_26 = V_0;
		Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * L_27 = (Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 *)il2cpp_codegen_object_new(Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44_il2cpp_TypeInfo_var);
		Func_2__ctor_m7141D4C33E399BD20EB8B17781BACB3B85C669C6(L_27, L_26, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass30_0_U3CRegisterBallU3Eb__0_mFD130C2AF37A75EA852CB27234BAE17B629F60C9_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m7141D4C33E399BD20EB8B17781BACB3B85C669C6_RuntimeMethod_var);
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_28 = Enumerable_First_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m1AEA5E6A40B402B2518D20770F78C970461347D1(L_25, L_27, /*hidden argument*/Enumerable_First_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m1AEA5E6A40B402B2518D20770F78C970461347D1_RuntimeMethod_var);
		NullCheck(L_28);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_29 = L_28->get_icons_6();
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_30 = V_2;
		NullCheck(L_30);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_31 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3(L_29, L_31, /*hidden argument*/List_1_Add_m3DD76DE838FA83DF972E0486A296345EB3A7DDF3_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void GameManager::ChangePlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_ChangePlayer_m0F30608B17D39CD119A0AC533BF3064E24D79BEF (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_ChangePlayer_m0F30608B17D39CD119A0AC533BF3064E24D79BEF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * G_B2_0 = NULL;
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * G_B1_0 = NULL;
	Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * G_B3_0 = NULL;
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * G_B3_1 = NULL;
	{
		// print("change");
		MonoBehaviour_print_m4F113B89EC1C221CAC6EC64365E6DAD0AF86F090(_stringLiteralCBCC9AFE4096250DEAA855B0BAA98F52FE771F26, /*hidden argument*/NULL);
		// currentPlayer.playerController.enabled = false;
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_0 = __this->get_currentPlayer_26();
		NullCheck(L_0);
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_1 = L_0->get_playerController_1();
		NullCheck(L_1);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_1, (bool)0, /*hidden argument*/NULL);
		// currentPlayer = currentPlayer == remotePlayer ? localPlayer : remotePlayer;
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_2 = __this->get_currentPlayer_26();
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_3 = __this->get_remotePlayer_25();
		G_B1_0 = __this;
		if ((((RuntimeObject*)(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F *)L_2) == ((RuntimeObject*)(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F *)L_3)))
		{
			G_B2_0 = __this;
			goto IL_0032;
		}
	}
	{
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_4 = __this->get_remotePlayer_25();
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		goto IL_0038;
	}

IL_0032:
	{
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_5 = __this->get_localPlayer_24();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
	}

IL_0038:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_currentPlayer_26(G_B3_0);
		// currentPlayer.playerController.enabled = true;
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_6 = __this->get_currentPlayer_26();
		NullCheck(L_6);
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_7 = L_6->get_playerController_1();
		NullCheck(L_7);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_7, (bool)1, /*hidden argument*/NULL);
		// state = GameState.Aim;
		((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->set_state_5(3);
		// Clock = 30 * (1 + 0.02f * currentPlayer.data.Time);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_8 = __this->get_currentPlayer_26();
		NullCheck(L_8);
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_9 = L_8->get_data_0();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_Time_9();
		GameManager_set_Clock_m6A52D50A47B356DE092A46E16D85258DAA4338C2(((float)il2cpp_codegen_multiply((float)(30.0f), (float)((float)il2cpp_codegen_add((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)(0.0199999996f), (float)(((float)((float)L_10))))))))), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameManager::EndGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_EndGame_m34CB0E063C72D2D7BA815B5397C5DB865EE60810 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_EndGame_m34CB0E063C72D2D7BA815B5397C5DB865EE60810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// goPanel.gameObject.SetActive(true);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_0 = __this->get_goPanel_17();
		NullCheck(L_0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)1, /*hidden argument*/NULL);
		// if (currentPlayer == localPlayer) goText.text = "YOU WIN!";
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_2 = __this->get_currentPlayer_26();
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_3 = __this->get_localPlayer_24();
		if ((!(((RuntimeObject*)(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F *)L_2) == ((RuntimeObject*)(Player_t5689617909B48F7640EA0892D85C92C13CC22C6F *)L_3))))
		{
			goto IL_0031;
		}
	}
	{
		// if (currentPlayer == localPlayer) goText.text = "YOU WIN!";
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_4 = __this->get_goText_18();
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, _stringLiteralA0BC275515E57D4FC52BBC80EB62131C416AA168);
		goto IL_0041;
	}

IL_0031:
	{
		// else goText.text = "YOU LOSE";
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_5 = __this->get_goText_18();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteral53B6FFAF5394EC3DBC3422E88D0E40D256C996D6);
	}

IL_0041:
	{
		// currentTable.enabled = false;
		Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_6 = __this->get_currentTable_23();
		NullCheck(L_6);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_6, (bool)0, /*hidden argument*/NULL);
		// remotePlayer.playerController.enabled = false;
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_7 = __this->get_remotePlayer_25();
		NullCheck(L_7);
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_8 = L_7->get_playerController_1();
		NullCheck(L_8);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_8, (bool)0, /*hidden argument*/NULL);
		// localPlayer.playerController.enabled = false;
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_9 = __this->get_localPlayer_24();
		NullCheck(L_9);
		PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * L_10 = L_9->get_playerController_1();
		NullCheck(L_10);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_10, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameManager::ReturnToMenu()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_ReturnToMenu_m361E877417F8294B9B32A6B0A27FE6EF1E77FBB6 (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_ReturnToMenu_m361E877417F8294B9B32A6B0A27FE6EF1E77FBB6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// state = GameState.Aim;
		((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->set_state_5(3);
		// goPanel.gameObject.SetActive(false);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_0 = __this->get_goPanel_17();
		NullCheck(L_0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)0, /*hidden argument*/NULL);
		// foreach (Transform t in deckA) Destroy(t.gameObject);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_2 = __this->get_deckA_12();
		NullCheck(L_2);
		RuntimeObject* L_3 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0023:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003a;
		}

IL_0025:
		{
			// foreach (Transform t in deckA) Destroy(t.gameObject);
			RuntimeObject* L_4 = V_0;
			NullCheck(L_4);
			RuntimeObject * L_5 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_4);
			// foreach (Transform t in deckA) Destroy(t.gameObject);
			NullCheck(((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_5, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var)));
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_5, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
			Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_6, /*hidden argument*/NULL);
		}

IL_003a:
		{
			// foreach (Transform t in deckA) Destroy(t.gameObject);
			RuntimeObject* L_7 = V_0;
			NullCheck(L_7);
			bool L_8 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_7);
			if (L_8)
			{
				goto IL_0025;
			}
		}

IL_0042:
		{
			IL2CPP_LEAVE(0x55, FINALLY_0044);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_9 = V_0;
			V_1 = ((RuntimeObject*)IsInst((RuntimeObject*)L_9, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_10 = V_1;
			if (!L_10)
			{
				goto IL_0054;
			}
		}

IL_004e:
		{
			RuntimeObject* L_11 = V_1;
			NullCheck(L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_11);
		}

IL_0054:
		{
			IL2CPP_END_FINALLY(68)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x55, IL_0055)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0055:
	{
		// foreach (Transform t in deckB) Destroy(t.gameObject);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_12 = __this->get_deckB_13();
		NullCheck(L_12);
		RuntimeObject* L_13 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_12, /*hidden argument*/NULL);
		V_0 = L_13;
	}

IL_0061:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0078;
		}

IL_0063:
		{
			// foreach (Transform t in deckB) Destroy(t.gameObject);
			RuntimeObject* L_14 = V_0;
			NullCheck(L_14);
			RuntimeObject * L_15 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_14);
			// foreach (Transform t in deckB) Destroy(t.gameObject);
			NullCheck(((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_15, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var)));
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_16 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_15, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
			Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_16, /*hidden argument*/NULL);
		}

IL_0078:
		{
			// foreach (Transform t in deckB) Destroy(t.gameObject);
			RuntimeObject* L_17 = V_0;
			NullCheck(L_17);
			bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_0063;
			}
		}

IL_0080:
		{
			IL2CPP_LEAVE(0x93, FINALLY_0082);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0082;
	}

FINALLY_0082:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_19 = V_0;
			V_1 = ((RuntimeObject*)IsInst((RuntimeObject*)L_19, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_20 = V_1;
			if (!L_20)
			{
				goto IL_0092;
			}
		}

IL_008c:
		{
			RuntimeObject* L_21 = V_1;
			NullCheck(L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_21);
		}

IL_0092:
		{
			IL2CPP_END_FINALLY(130)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(130)
	{
		IL2CPP_JUMP_TBL(0x93, IL_0093)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0093:
	{
		// localPlayer.data = null;
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_22 = __this->get_localPlayer_24();
		NullCheck(L_22);
		L_22->set_data_0((PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 *)NULL);
		// remotePlayer.data = null;
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_23 = __this->get_remotePlayer_25();
		NullCheck(L_23);
		L_23->set_data_0((PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 *)NULL);
		// SceneManager.LoadScene(1);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameManager::SetForce()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager_SetForce_m2B0FF265E47640E330E5D78D23FCF62ADC99084A (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	{
		// localPlayer.forceMul = slider.value;
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_0 = __this->get_localPlayer_24();
		Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * L_1 = __this->get_slider_14();
		NullCheck(L_1);
		float L_2 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_1);
		NullCheck(L_0);
		L_0->set_forceMul_3(L_2);
		// }
		return;
	}
}
// System.Void GameManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InputManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager_Start_m99C6C0D9277906D13076BCD14E7BE2DDAF88FF08 (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InputManager_Start_m99C6C0D9277906D13076BCD14E7BE2DDAF88FF08_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _player = GameManager.Instance().localPlayer;
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_1 = L_0->get_localPlayer_24();
		((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 *)__this)->set__player_4(L_1);
		// _player.playerController = this;
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_2 = ((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 *)__this)->get__player_4();
		NullCheck(L_2);
		L_2->set_playerController_1(__this);
		// GameManager.Instance().currentPlayer = GameManager.Instance().localPlayer;
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_3 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_4 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_4);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_5 = L_4->get_localPlayer_24();
		NullCheck(L_3);
		L_3->set_currentPlayer_26(L_5);
		// GameManager.Instance().slider.onValueChanged.AddListener(_player.CueMove);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_6 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_6);
		Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * L_7 = L_6->get_slider_14();
		NullCheck(L_7);
		SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * L_8 = Slider_get_onValueChanged_m7F480C569A6D668952BE1436691850D13825E129_inline(L_7, /*hidden argument*/NULL);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_9 = ((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 *)__this)->get__player_4();
		UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB * L_10 = (UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB *)il2cpp_codegen_object_new(UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m8CACADCAC18230FB18DF7A6BEC3D9EAD93FEDC3B(L_10, L_9, (intptr_t)((intptr_t)Player_CueMove_m99C0CC666F9E6AF125FC0866717BE649BA192961_RuntimeMethod_var), /*hidden argument*/UnityAction_1__ctor_m8CACADCAC18230FB18DF7A6BEC3D9EAD93FEDC3B_RuntimeMethod_var);
		NullCheck(L_8);
		UnityEvent_1_AddListener_mA73838FBF3836695F5183B32B797E9499BA5E59C(L_8, L_10, /*hidden argument*/UnityEvent_1_AddListener_mA73838FBF3836695F5183B32B797E9499BA5E59C_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void InputManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager_Update_mE17FD2A03E0E1BE94DFE0B0AB4B5B9C5F3EA285B (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InputManager_Update_mE17FD2A03E0E1BE94DFE0B0AB4B5B9C5F3EA285B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_3;
	memset((&V_3), 0, sizeof(V_3));
	float V_4 = 0.0f;
	{
		// if (GameManager.state == GameState.Aim || GameManager.state == GameState.Breaking)
		int32_t L_0 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get_state_5();
		if ((((int32_t)L_0) == ((int32_t)3)))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get_state_5();
		if (L_1)
		{
			goto IL_00b9;
		}
	}

IL_0012:
	{
		// float x = Input.GetAxis("Horizontal");
		float L_2 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E, /*hidden argument*/NULL);
		V_0 = L_2;
		// _player.CueRotate(x);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_3 = ((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 *)__this)->get__player_4();
		float L_4 = V_0;
		NullCheck(L_3);
		Player_CueRotate_m531643FEA219030BAD2FE999897D386DBD49FDD1(L_3, L_4, /*hidden argument*/NULL);
		// if (Input.touchCount > 0)
		int32_t L_5 = Input_get_touchCount_mE1A06AB1973E3456AE398B3CC5105F27CC7335D6(/*hidden argument*/NULL);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_00b9;
		}
	}
	{
		// Touch touch = Input.GetTouch(0);
		Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C  L_6 = Input_GetTouch_m6A2A31482B1A7D018C3AAC188C02F5D14500C81F(0, /*hidden argument*/NULL);
		V_1 = L_6;
		// if (touch.phase == TouchPhase.Moved && !EventSystem.current.IsPointerOverGameObject(touch.fingerId))
		int32_t L_7 = Touch_get_phase_m576EA3F4FE1D12EB85510326AD8EC3C2EB267257((Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C *)(&V_1), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_00b9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_il2cpp_TypeInfo_var);
		EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * L_8 = EventSystem_get_current_m4B9C11F490297AE55428038DACD240596D6CE5F2(/*hidden argument*/NULL);
		int32_t L_9 = Touch_get_fingerId_mCED0E66949120E69BFE9294DC0A11A6F9FDBD129((Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C *)(&V_1), /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_10 = EventSystem_IsPointerOverGameObject_mE7043E54617B8289C81A1C7342FBE0AE448C9E3A(L_8, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_00b9;
		}
	}
	{
		// Vector2 c = Camera.main.WorldToScreenPoint(GameManager.Instance().currentTable.hitPoint.position);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_11 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_12 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_12);
		Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_13 = L_12->get_currentTable_23();
		NullCheck(L_13);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14 = L_13->get_hitPoint_7();
		NullCheck(L_14);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16 = Camera_WorldToScreenPoint_m44710195E7736CE9DE5A9B05E32059A9A950F95C(L_11, L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_il2cpp_TypeInfo_var);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_17 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		// Vector2 ca = touch.position - touch.deltaPosition - c;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_18 = Touch_get_position_mE32B04C6DA32A0965C403A31847ED7F1725EA1DE((Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C *)(&V_1), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_19 = Touch_get_deltaPosition_mF9D60C253E41DC4E4F832F88A1041BE8A9E7C0FB((Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C *)(&V_1), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_20 = Vector2_op_Subtraction_m6E536A8C72FEAA37FF8D5E26E11D6E71EB59599A_inline(L_18, L_19, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_21 = V_2;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_22 = Vector2_op_Subtraction_m6E536A8C72FEAA37FF8D5E26E11D6E71EB59599A_inline(L_20, L_21, /*hidden argument*/NULL);
		// Vector2 cb = touch.position - c;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_23 = Touch_get_position_mE32B04C6DA32A0965C403A31847ED7F1725EA1DE((Touch_tDEFED247540BCFA4AD452F1D37EEF4E09B4ACD8C *)(&V_1), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_24 = V_2;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_25 = Vector2_op_Subtraction_m6E536A8C72FEAA37FF8D5E26E11D6E71EB59599A_inline(L_23, L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		// float alp = -Vector2.SignedAngle(ca, cb);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_26 = V_3;
		float L_27 = Vector2_SignedAngle_m007FAC4E36153EEBC62347D6FA67162238C34C69(L_22, L_26, /*hidden argument*/NULL);
		V_4 = ((-L_27));
		// _player.CueRotate(alp);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_28 = ((PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 *)__this)->get__player_4();
		float L_29 = V_4;
		NullCheck(L_28);
		Player_CueRotate_m531643FEA219030BAD2FE999897D386DBD49FDD1(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00b9:
	{
		// }
		return;
	}
}
// System.Void InputManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager__ctor_mB533F16325A793C9274F6CA3804EBCE27AD700A7 (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method)
{
	{
		PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Menu::StartGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menu_StartGame_m6B2E9CA9AE74433EA78DE12BADAAFA800B15DFC0 (Menu_t9BC67061F8954119BDB8CE5A0C0B6E1AA114C0D6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Menu_StartGame_m6B2E9CA9AE74433EA78DE12BADAAFA800B15DFC0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!GameManager.Instance().localPlayer.data)
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_1 = L_0->get_localPlayer_24();
		NullCheck(L_1);
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_2 = L_1->get_data_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0047;
		}
	}
	{
		// GameManager.Instance().localPlayer.data = cues.GetComponentsInChildren<Card>()[Random.Range(0, cues.childCount)].data;
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_4 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_4);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_5 = L_4->get_localPlayer_24();
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_6 = __this->get_cues_5();
		NullCheck(L_6);
		CardU5BU5D_t32023BF72E2560CD1EA88DF7C04357E784A64CE0* L_7 = Component_GetComponentsInChildren_TisCard_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A_m27B008E50F6CADD5BDD8F25A3D8D73709C8A1577(L_6, /*hidden argument*/Component_GetComponentsInChildren_TisCard_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A_m27B008E50F6CADD5BDD8F25A3D8D73709C8A1577_RuntimeMethod_var);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_8 = __this->get_cues_5();
		NullCheck(L_8);
		int32_t L_9 = Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05(L_8, /*hidden argument*/NULL);
		int32_t L_10 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_11 = L_10;
		Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A * L_12 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_13 = L_12->get_data_4();
		NullCheck(L_5);
		L_5->set_data_0(L_13);
	}

IL_0047:
	{
		// if (!GameManager.Instance().remotePlayer.data)
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_14 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_14);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_15 = L_14->get_remotePlayer_25();
		NullCheck(L_15);
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_16 = L_15->get_data_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_008e;
		}
	}
	{
		// GameManager.Instance().remotePlayer.data = bots.GetComponentsInChildren<Card>()[Random.Range(0, bots.childCount)].data;
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_18 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_18);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_19 = L_18->get_remotePlayer_25();
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_20 = __this->get_bots_4();
		NullCheck(L_20);
		CardU5BU5D_t32023BF72E2560CD1EA88DF7C04357E784A64CE0* L_21 = Component_GetComponentsInChildren_TisCard_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A_m27B008E50F6CADD5BDD8F25A3D8D73709C8A1577(L_20, /*hidden argument*/Component_GetComponentsInChildren_TisCard_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A_m27B008E50F6CADD5BDD8F25A3D8D73709C8A1577_RuntimeMethod_var);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_22 = __this->get_bots_4();
		NullCheck(L_22);
		int32_t L_23 = Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05(L_22, /*hidden argument*/NULL);
		int32_t L_24 = Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A(0, L_23, /*hidden argument*/NULL);
		NullCheck(L_21);
		int32_t L_25 = L_24;
		Card_tB23FD1CFF0E901BF020E0A7ADA98E5CEF3D1B98A * L_26 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		NullCheck(L_26);
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_27 = L_26->get_data_4();
		NullCheck(L_19);
		L_19->set_data_0(L_27);
	}

IL_008e:
	{
		// UnityEngine.SceneManagement.SceneManager.LoadScene(2);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Menu::Exit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menu_Exit_m2C8C35A855E5512CF373EB29E4251CA091642707 (Menu_t9BC67061F8954119BDB8CE5A0C0B6E1AA114C0D6 * __this, const RuntimeMethod* method)
{
	{
		// Application.Quit();
		Application_Quit_m8D720E5092786C2EE32310D85FE61C253D3B1F2A(/*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Menu::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menu__ctor_m7EE9043A2E9DD126B6F69DD8AE3BC9CBDF6E2134 (Menu_t9BC67061F8954119BDB8CE5A0C0B6E1AA114C0D6 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Player::CueHit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_CueHit_m5EB2329180A3544C0DC71D37CA3793D9FB446A97 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player_CueHit_m5EB2329180A3544C0DC71D37CA3793D9FB446A97_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (GameManager.state == GameState.Moving) return;
		int32_t L_0 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get_state_5();
		if ((!(((uint32_t)L_0) == ((uint32_t)6))))
		{
			goto IL_0009;
		}
	}
	{
		// if (GameManager.state == GameState.Moving) return;
		return;
	}

IL_0009:
	{
		// GameManager.Instance().currentTable.CueBall.Rigidbody
		//     .AddForce(GameManager.Instance().currentTable.hitPoint.forward *
		//         (GameManager.Instance().BaseForce * (1 + data.Force * 0.02f))
		//         * forceMul, ForceMode.Impulse);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_1 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_1);
		Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_2 = L_1->get_currentTable_23();
		NullCheck(L_2);
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_3 = L_2->get_CueBall_9();
		NullCheck(L_3);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_4 = L_3->get_Rigidbody_5();
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_5 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_5);
		Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_6 = L_5->get_currentTable_23();
		NullCheck(L_6);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7 = L_6->get_hitPoint_7();
		NullCheck(L_7);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_7, /*hidden argument*/NULL);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_9 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_9);
		float L_10 = L_9->get_BaseForce_8();
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_11 = __this->get_data_0();
		NullCheck(L_11);
		int32_t L_12 = L_11->get_Force_6();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_8, ((float)il2cpp_codegen_multiply((float)L_10, (float)((float)il2cpp_codegen_add((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_12))), (float)(0.0199999996f))))))), /*hidden argument*/NULL);
		float L_14 = __this->get_forceMul_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_4);
		Rigidbody_AddForce_m78B9D94F505E19F3C63461362AD6DE7EA0836700(L_4, L_15, 1, /*hidden argument*/NULL);
		// GameManager.state = GameState.Hit;
		((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->set_state_5(5);
		// CueMove(-0.25f);
		Player_CueMove_m99C0CC666F9E6AF125FC0866717BE649BA192961(__this, (-0.25f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Player::CueRotate(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_CueRotate_m531643FEA219030BAD2FE999897D386DBD49FDD1 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, float ___angle0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player_CueRotate_m531643FEA219030BAD2FE999897D386DBD49FDD1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GameManager.Instance().currentTable.hitPoint.Rotate(Vector3.up, angle * Time.deltaTime * GameManager.Instance().Sensitivity);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_1 = L_0->get_currentTable_23();
		NullCheck(L_1);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = L_1->get_hitPoint_7();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		float L_4 = ___angle0;
		float L_5 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_6 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_6);
		float L_7 = L_6->get_Sensitivity_7();
		NullCheck(L_2);
		Transform_Rotate_m2AA745C4A796363462642A13251E8971D5C7F4DC(L_2, L_3, ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), (float)L_7)), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Player::CueMove(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player_CueMove_m99C0CC666F9E6AF125FC0866717BE649BA192961 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, float ___sl0, const RuntimeMethod* method)
{
	{
		// GameManager.Instance().currentTable.cueMesh.transform.localPosition = new Vector3(0, 0, -2 * sl);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_1 = L_0->get_currentTable_23();
		NullCheck(L_1);
		MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * L_2 = L_1->get_cueMesh_8();
		NullCheck(L_2);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_2, /*hidden argument*/NULL);
		float L_4 = ___sl0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		memset((&L_5), 0, sizeof(L_5));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_5), (0.0f), (0.0f), ((float)il2cpp_codegen_multiply((float)(-2.0f), (float)L_4)), /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_3, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Player::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7 (Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3 (PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void PlayerController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerData__ctor_mC2DA6B32832AB92B19E799148968590FF1F28C1B (PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * __this, const RuntimeMethod* method)
{
	{
		ScriptableObject__ctor_m8DAE6CDCFA34E16F2543B02CC3669669FF203063(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Pocket::OnTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pocket_OnTriggerEnter_m01FB6A867212759966A839FC3CE069882894D426 (Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pocket_OnTriggerEnter_m01FB6A867212759966A839FC3CE069882894D426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * V_0 = NULL;
	{
		// Ball ball = other.GetComponent<Ball>();
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		NullCheck(L_0);
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_1 = Component_GetComponent_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m564BDAF1415831A7751D8FFD48AEFAAEB60ACF9D(L_0, /*hidden argument*/Component_GetComponent_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m564BDAF1415831A7751D8FFD48AEFAAEB60ACF9D_RuntimeMethod_var);
		V_0 = L_1;
		// if (ball)
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00cc;
		}
	}
	{
		// if (other.GetComponent<Ball>().Number == 0)
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_4 = ___other0;
		NullCheck(L_4);
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_5 = Component_GetComponent_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m564BDAF1415831A7751D8FFD48AEFAAEB60ACF9D(L_4, /*hidden argument*/Component_GetComponent_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m564BDAF1415831A7751D8FFD48AEFAAEB60ACF9D_RuntimeMethod_var);
		NullCheck(L_5);
		int32_t L_6 = L_5->get_Number_4();
		if (L_6)
		{
			goto IL_0026;
		}
	}
	{
		// GameManager.state = GameState.Foul;
		((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->set_state_5(1);
		// return;
		return;
	}

IL_0026:
	{
		// if (GameManager.Instance().currentTable.Balls.Count == 15)
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_7 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_7);
		Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_8 = L_7->get_currentTable_23();
		NullCheck(L_8);
		List_1_t704724B962CF6005382C610663175BE693752D5D * L_9 = L_8->get_Balls_11();
		NullCheck(L_9);
		int32_t L_10 = List_1_get_Count_mED876C00B350B9333120130A2EE72901165DA7CB_inline(L_9, /*hidden argument*/List_1_get_Count_mED876C00B350B9333120130A2EE72901165DA7CB_RuntimeMethod_var);
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_0049;
		}
	}
	{
		// GameManager.Instance().SetPlayers(ball);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_11 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_12 = V_0;
		NullCheck(L_11);
		GameManager_SetPlayers_mCFA2D98B19876D41E1DC50B557F77DA74C5973DB(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0049:
	{
		// if (other.GetComponent<Ball>().Number == 8)
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_13 = ___other0;
		NullCheck(L_13);
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_14 = Component_GetComponent_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m564BDAF1415831A7751D8FFD48AEFAAEB60ACF9D(L_13, /*hidden argument*/Component_GetComponent_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m564BDAF1415831A7751D8FFD48AEFAAEB60ACF9D_RuntimeMethod_var);
		NullCheck(L_14);
		int32_t L_15 = L_14->get_Number_4();
		if ((!(((uint32_t)L_15) == ((uint32_t)8))))
		{
			goto IL_005f;
		}
	}
	{
		// GameManager.state = GameState.EndGame;
		((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->set_state_5(8);
		// }
		goto IL_008d;
	}

IL_005f:
	{
		// else if (!(ball.Number < 8 ^ GameManager.Instance().currentPlayer.ballsType == BallsType.Solids))
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = L_16->get_Number_4();
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_18 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_18);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_19 = L_18->get_currentPlayer_26();
		NullCheck(L_19);
		int32_t L_20 = L_19->get_ballsType_2();
		if (((int32_t)((int32_t)((((int32_t)L_17) < ((int32_t)8))? 1 : 0)^(int32_t)((((int32_t)L_20) == ((int32_t)0))? 1 : 0))))
		{
			goto IL_008d;
		}
	}
	{
		// GameManager.Instance().currentTable.canContinue = true;
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_21 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_21);
		Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_22 = L_21->get_currentTable_23();
		NullCheck(L_22);
		L_22->set_canContinue_14((bool)1);
	}

IL_008d:
	{
		// GameManager.Instance().currentTable.RemoveBall(ball);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_23 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_23);
		Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_24 = L_23->get_currentTable_23();
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_25 = V_0;
		NullCheck(L_24);
		Table_RemoveBall_m02A9A5675D99552809A1869FF89AFE3E0FC9C6DE(L_24, L_25, /*hidden argument*/NULL);
		// other.attachedRigidbody.velocity = Vector3.zero;
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_26 = ___other0;
		NullCheck(L_26);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_27 = Collider_get_attachedRigidbody_m101FED12AD292F372F98E94A6D02A5E428AA896A(L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		NullCheck(L_27);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_27, L_28, /*hidden argument*/NULL);
		// other.transform.position = GameManager.Instance().currentTable.deck.position;
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_29 = ___other0;
		NullCheck(L_29);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_30 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_29, /*hidden argument*/NULL);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_31 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_31);
		Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * L_32 = L_31->get_currentTable_23();
		NullCheck(L_32);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_33 = L_32->get_deck_13();
		NullCheck(L_33);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_34 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_33, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_30, L_34, /*hidden argument*/NULL);
	}

IL_00cc:
	{
		// }
		return;
	}
}
// System.Void Pocket::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pocket__ctor_mC248FF4513B38C1E217885E9A6743060D856FC8B (Pocket_t69E9E30897953A969E05E646946018B4DF9423D7 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Table::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Table_Awake_mDBD0366C97BE96CB92CDC3B4690D365FD4A24081 (Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Table_Awake_mDBD0366C97BE96CB92CDC3B4690D365FD4A24081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GameManager.Instance().currentTable = this;
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->set_currentTable_23(__this);
		// GameManager.Instance().clickHit.onClick.AddListener(GameManager.Instance().localPlayer.CueHit);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_1 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_1);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_2 = L_1->get_clickHit_15();
		NullCheck(L_2);
		ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * L_3 = Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline(L_2, /*hidden argument*/NULL);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_4 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_4);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_5 = L_4->get_localPlayer_24();
		UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * L_6 = (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 *)il2cpp_codegen_object_new(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA(L_6, L_5, (intptr_t)((intptr_t)Player_CueHit_m5EB2329180A3544C0DC71D37CA3793D9FB446A97_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_3);
		UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D(L_3, L_6, /*hidden argument*/NULL);
		// GameManager.Clock = 30 * (1 + 0.02f * GameManager.Instance().localPlayer.data.Time);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_7 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_7);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_8 = L_7->get_localPlayer_24();
		NullCheck(L_8);
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_9 = L_8->get_data_0();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_Time_9();
		GameManager_set_Clock_m6A52D50A47B356DE092A46E16D85258DAA4338C2(((float)il2cpp_codegen_multiply((float)(30.0f), (float)((float)il2cpp_codegen_add((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)(0.0199999996f), (float)(((float)((float)L_10))))))))), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Table::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Table_FixedUpdate_m7A17D40FF64C69746F30A6F2C540C58073B3FD07 (Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Table_FixedUpdate_m7A17D40FF64C69746F30A6F2C540C58073B3FD07_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB  V_3;
	memset((&V_3), 0, sizeof(V_3));
	int32_t V_4 = 0;
	Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB  V_5;
	memset((&V_5), 0, sizeof(V_5));
	Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * G_B3_0 = NULL;
	Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * G_B2_0 = NULL;
	Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * G_B4_0 = NULL;
	Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * G_B7_0 = NULL;
	Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * G_B6_0 = NULL;
	Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB  G_B10_0;
	memset((&G_B10_0), 0, sizeof(G_B10_0));
	Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * G_B9_0 = NULL;
	Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * G_B8_0 = NULL;
	Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * G_B15_0 = NULL;
	Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * G_B14_0 = NULL;
	Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * G_B16_0 = NULL;
	{
		// Vector3 normalized = Vector3.ProjectOnPlane(hitPoint.forward, Vector3.up).normalized;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_hitPoint_7();
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = Vector3_ProjectOnPlane_m066BDEFD60B2828C4B531CD96C4DBFADF6B0EF3B(L_1, L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_2), /*hidden argument*/NULL);
		V_0 = L_4;
		// Ray ray = new Ray(hitPoint.position, normalized * 10f);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5 = __this->get_hitPoint_7();
		NullCheck(L_5);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_5, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_7, (10.0f), /*hidden argument*/NULL);
		Ray_t2E9E67CC8B03EE6ED2BBF3D2C9C96DDF70E1D5E6  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Ray__ctor_m75B1F651FF47EE6B887105101B7DA61CBF41F83C((&L_9), L_6, L_8, /*hidden argument*/NULL);
		// Physics.SphereCast(ray, 0.285f, out hit);
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_10 = __this->get_address_of_hit_4();
		Physics_SphereCast_mF32FCDFC17C9B4A1F9D55729A54504A82567F065(L_9, (0.284999996f), (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_10, /*hidden argument*/NULL);
		// float f = Mathf.Sign(Vector3.SignedAngle(normalized, hit.normal, Vector3.up));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = V_0;
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_12 = __this->get_address_of_hit_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = RaycastHit_get_normal_m2C813B25BAECD87FD9E9CB294278B291F4CC6674((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_12, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		float L_15 = Vector3_SignedAngle_m816C32A674665A4C3C9D850FB0A107E69A4D3E0A(L_11, L_13, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_il2cpp_TypeInfo_var);
		float L_16 = Mathf_Sign_m01716387C82B9523CFFADED7B2037D75F57FE2FB(L_15, /*hidden argument*/NULL);
		// Vector3 cueReflect = Quaternion.AngleAxis(f * 30, Vector3.up) * normalized;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_il2cpp_TypeInfo_var);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_18 = Quaternion_AngleAxis_m4644D20F58ADF03E9EA297CB4A845E5BCDA1E398(((float)il2cpp_codegen_multiply((float)L_16, (float)(30.0f))), L_17, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = Quaternion_op_Multiply_mDC5F913E6B21FEC72AB2CF737D34CC6C7A69803D(L_18, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		// phantomTracer.enabled = GameManager.state != GameState.Moving;
		LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * L_21 = __this->get_phantomTracer_5();
		int32_t L_22 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get_state_5();
		NullCheck(L_21);
		Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B(L_21, (bool)((((int32_t)((((int32_t)L_22) == ((int32_t)6))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		// phantom.position = hit.point + hit.normal * 0.285f;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_23 = __this->get_phantom_10();
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_24 = __this->get_address_of_hit_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25 = RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_24, /*hidden argument*/NULL);
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_26 = __this->get_address_of_hit_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27 = RaycastHit_get_normal_m2C813B25BAECD87FD9E9CB294278B291F4CC6674((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_26, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_27, (0.284999996f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_25, L_28, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_23, L_29, /*hidden argument*/NULL);
		// phantomTracer.SetPosition(0, hitPoint.position);
		LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * L_30 = __this->get_phantomTracer_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_31 = __this->get_hitPoint_7();
		NullCheck(L_31);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		LineRenderer_SetPosition_mD37DBE4B3E13A838FFD09289BC77DEDB423D620F(L_30, 0, L_32, /*hidden argument*/NULL);
		// phantomTracer.SetPosition(1, phantom.position);
		LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * L_33 = __this->get_phantomTracer_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_34 = __this->get_phantom_10();
		NullCheck(L_34);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		LineRenderer_SetPosition_mD37DBE4B3E13A838FFD09289BC77DEDB423D620F(L_33, 1, L_35, /*hidden argument*/NULL);
		// phantomTracer.SetPosition(2, phantom.position + cueReflect);
		LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * L_36 = __this->get_phantomTracer_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_37 = __this->get_phantom_10();
		NullCheck(L_37);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_38 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_37, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_39 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_40 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_38, L_39, /*hidden argument*/NULL);
		NullCheck(L_36);
		LineRenderer_SetPosition_mD37DBE4B3E13A838FFD09289BC77DEDB423D620F(L_36, 2, L_40, /*hidden argument*/NULL);
		// if (Balls.Count == 15 || !hit.collider?.GetComponent<Ball>() || hit.collider?.GetComponent<Ball>()?.Type() == GameManager.Instance().currentPlayer.ballsType)
		List_1_t704724B962CF6005382C610663175BE693752D5D * L_41 = __this->get_Balls_11();
		NullCheck(L_41);
		int32_t L_42 = List_1_get_Count_mED876C00B350B9333120130A2EE72901165DA7CB_inline(L_41, /*hidden argument*/List_1_get_Count_mED876C00B350B9333120130A2EE72901165DA7CB_RuntimeMethod_var);
		if ((((int32_t)L_42) == ((int32_t)((int32_t)15))))
		{
			goto IL_019b;
		}
	}
	{
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_43 = __this->get_address_of_hit_4();
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_44 = RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_43, /*hidden argument*/NULL);
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_45 = L_44;
		G_B2_0 = L_45;
		if (L_45)
		{
			G_B3_0 = L_45;
			goto IL_012e;
		}
	}
	{
		G_B4_0 = ((Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D *)(NULL));
		goto IL_0133;
	}

IL_012e:
	{
		NullCheck(G_B3_0);
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_46 = Component_GetComponent_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m564BDAF1415831A7751D8FFD48AEFAAEB60ACF9D(G_B3_0, /*hidden argument*/Component_GetComponent_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m564BDAF1415831A7751D8FFD48AEFAAEB60ACF9D_RuntimeMethod_var);
		G_B4_0 = L_46;
	}

IL_0133:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_47 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(G_B4_0, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_019b;
		}
	}
	{
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_48 = __this->get_address_of_hit_4();
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_49 = RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_48, /*hidden argument*/NULL);
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_50 = L_49;
		G_B6_0 = L_50;
		if (L_50)
		{
			G_B7_0 = L_50;
			goto IL_0155;
		}
	}
	{
		il2cpp_codegen_initobj((&V_5), sizeof(Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB ));
		Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB  L_51 = V_5;
		G_B10_0 = L_51;
		goto IL_0174;
	}

IL_0155:
	{
		NullCheck(G_B7_0);
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_52 = Component_GetComponent_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m564BDAF1415831A7751D8FFD48AEFAAEB60ACF9D(G_B7_0, /*hidden argument*/Component_GetComponent_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m564BDAF1415831A7751D8FFD48AEFAAEB60ACF9D_RuntimeMethod_var);
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_53 = L_52;
		G_B8_0 = L_53;
		if (L_53)
		{
			G_B9_0 = L_53;
			goto IL_016a;
		}
	}
	{
		il2cpp_codegen_initobj((&V_5), sizeof(Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB ));
		Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB  L_54 = V_5;
		G_B10_0 = L_54;
		goto IL_0174;
	}

IL_016a:
	{
		NullCheck(G_B9_0);
		int32_t L_55 = Ball_Type_mB4092D6071D5B95B0EB1FB883A3D2EC4A3663FF6(G_B9_0, /*hidden argument*/NULL);
		Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB  L_56;
		memset((&L_56), 0, sizeof(L_56));
		Nullable_1__ctor_mD66CEE733802251058E31FBEF0840C7A9D4DA406((&L_56), L_55, /*hidden argument*/Nullable_1__ctor_mD66CEE733802251058E31FBEF0840C7A9D4DA406_RuntimeMethod_var);
		G_B10_0 = L_56;
	}

IL_0174:
	{
		V_3 = G_B10_0;
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_57 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_57);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_58 = L_57->get_currentPlayer_26();
		NullCheck(L_58);
		int32_t L_59 = L_58->get_ballsType_2();
		V_4 = L_59;
		int32_t L_60 = Nullable_1_GetValueOrDefault_mE9A6BC0C579094139424C3A07555F05CFE4CA73B_inline((Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB *)(&V_3), /*hidden argument*/Nullable_1_GetValueOrDefault_mE9A6BC0C579094139424C3A07555F05CFE4CA73B_RuntimeMethod_var);
		int32_t L_61 = V_4;
		bool L_62 = Nullable_1_get_HasValue_mCB87A6CB3CF78384E3F13C909FDB8AD3659DF6CD_inline((Nullable_1_tC3AD784C837B78751964E8F69BE43AC9659DCACB *)(&V_3), /*hidden argument*/Nullable_1_get_HasValue_mCB87A6CB3CF78384E3F13C909FDB8AD3659DF6CD_RuntimeMethod_var);
		if (!((int32_t)((int32_t)((((int32_t)L_60) == ((int32_t)L_61))? 1 : 0)&(int32_t)L_62)))
		{
			goto IL_01b8;
		}
	}

IL_019b:
	{
		// phantom.gameObject.GetComponent<MeshRenderer>().material = phFriend;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_63 = __this->get_phantom_10();
		NullCheck(L_63);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_64 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_63, /*hidden argument*/NULL);
		NullCheck(L_64);
		MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * L_65 = GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B(L_64, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B_RuntimeMethod_var);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_66 = __this->get_phFriend_15();
		NullCheck(L_65);
		Renderer_set_material_m8DED7F4F7AF38755C3D7DAFDD613BBE1AAB941BA(L_65, L_66, /*hidden argument*/NULL);
		goto IL_01d3;
	}

IL_01b8:
	{
		// phantom.gameObject.GetComponent<MeshRenderer>().material = phFoe;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_67 = __this->get_phantom_10();
		NullCheck(L_67);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_68 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_67, /*hidden argument*/NULL);
		NullCheck(L_68);
		MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * L_69 = GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B(L_68, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_m4E244CD0EBBF9E0A3A73AF14F6EC434CA82E6F4B_RuntimeMethod_var);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_70 = __this->get_phFoe_16();
		NullCheck(L_69);
		Renderer_set_material_m8DED7F4F7AF38755C3D7DAFDD613BBE1AAB941BA(L_69, L_70, /*hidden argument*/NULL);
	}

IL_01d3:
	{
		// if (hit.collider?.GetComponent<Ball>() && GameManager.state != GameState.Moving)
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_71 = __this->get_address_of_hit_4();
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_72 = RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_71, /*hidden argument*/NULL);
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_73 = L_72;
		G_B14_0 = L_73;
		if (L_73)
		{
			G_B15_0 = L_73;
			goto IL_01e5;
		}
	}
	{
		G_B16_0 = ((Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D *)(NULL));
		goto IL_01ea;
	}

IL_01e5:
	{
		NullCheck(G_B15_0);
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_74 = Component_GetComponent_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m564BDAF1415831A7751D8FFD48AEFAAEB60ACF9D(G_B15_0, /*hidden argument*/Component_GetComponent_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_m564BDAF1415831A7751D8FFD48AEFAAEB60ACF9D_RuntimeMethod_var);
		G_B16_0 = L_74;
	}

IL_01ea:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_75 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(G_B16_0, /*hidden argument*/NULL);
		if (!L_75)
		{
			goto IL_029f;
		}
	}
	{
		int32_t L_76 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get_state_5();
		if ((((int32_t)L_76) == ((int32_t)6)))
		{
			goto IL_029f;
		}
	}
	{
		// reflectionTracer.enabled = true;
		LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * L_77 = __this->get_reflectionTracer_6();
		NullCheck(L_77);
		Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B(L_77, (bool)1, /*hidden argument*/NULL);
		// reflectionTracer.SetPosition(0, hit.collider.transform.position);
		LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * L_78 = __this->get_reflectionTracer_6();
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_79 = __this->get_address_of_hit_4();
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_80 = RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_79, /*hidden argument*/NULL);
		NullCheck(L_80);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_81 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_80, /*hidden argument*/NULL);
		NullCheck(L_81);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_82 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_81, /*hidden argument*/NULL);
		NullCheck(L_78);
		LineRenderer_SetPosition_mD37DBE4B3E13A838FFD09289BC77DEDB423D620F(L_78, 0, L_82, /*hidden argument*/NULL);
		// reflectionTracer.SetPosition(1, hit.collider.transform.position + GameManager.Instance().slider.value * (normalized - cueReflect) *
		//     4 * (1 + 0.08f * GameManager.Instance().currentPlayer.data.Aim)
		// );
		LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * L_83 = __this->get_reflectionTracer_6();
		RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * L_84 = __this->get_address_of_hit_4();
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_85 = RaycastHit_get_collider_m13A3DE16FBC631E0A1F987E0B22CE70AF8AB539E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)L_84, /*hidden argument*/NULL);
		NullCheck(L_85);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_86 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_85, /*hidden argument*/NULL);
		NullCheck(L_86);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_87 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_86, /*hidden argument*/NULL);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_88 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_88);
		Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * L_89 = L_88->get_slider_14();
		NullCheck(L_89);
		float L_90 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_89);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_91 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_92 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_93 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_91, L_92, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_94 = Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline(L_90, L_93, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_95 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_94, (4.0f), /*hidden argument*/NULL);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_96 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_96);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_97 = L_96->get_currentPlayer_26();
		NullCheck(L_97);
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_98 = L_97->get_data_0();
		NullCheck(L_98);
		int32_t L_99 = L_98->get_Aim_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_100 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_95, ((float)il2cpp_codegen_add((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)(0.0799999982f), (float)(((float)((float)L_99))))))), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_101 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_87, L_100, /*hidden argument*/NULL);
		NullCheck(L_83);
		LineRenderer_SetPosition_mD37DBE4B3E13A838FFD09289BC77DEDB423D620F(L_83, 1, L_101, /*hidden argument*/NULL);
		// }
		goto IL_02ab;
	}

IL_029f:
	{
		// reflectionTracer.enabled = false;
		LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967 * L_102 = __this->get_reflectionTracer_6();
		NullCheck(L_102);
		Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B(L_102, (bool)0, /*hidden argument*/NULL);
	}

IL_02ab:
	{
		// if (CueBall) hitPoint.position = CueBall.transform.position;
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_103 = __this->get_CueBall_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_104 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_103, /*hidden argument*/NULL);
		if (!L_104)
		{
			goto IL_02d3;
		}
	}
	{
		// if (CueBall) hitPoint.position = CueBall.transform.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_105 = __this->get_hitPoint_7();
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_106 = __this->get_CueBall_9();
		NullCheck(L_106);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_107 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_106, /*hidden argument*/NULL);
		NullCheck(L_107);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_108 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_107, /*hidden argument*/NULL);
		NullCheck(L_105);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_105, L_108, /*hidden argument*/NULL);
	}

IL_02d3:
	{
		// GameManager.Clock -= Time.fixedDeltaTime;
		float L_109 = GameManager_get_Clock_m32723273A7CCC5919901AC0AA016C8F90E90A5D6_inline(/*hidden argument*/NULL);
		float L_110 = Time_get_fixedDeltaTime_m8E94ECFF6A6A1D9B5D60BF82D116D540852484E5(/*hidden argument*/NULL);
		GameManager_set_Clock_m6A52D50A47B356DE092A46E16D85258DAA4338C2(((float)il2cpp_codegen_subtract((float)L_109, (float)L_110)), /*hidden argument*/NULL);
		// CheckState();
		Table_CheckState_mDF18708BD88BB608A70BF3CD22B86CF4BC0DF817(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Table::CheckState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Table_CheckState_mDF18708BD88BB608A70BF3CD22B86CF4BC0DF817 (Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Table_CheckState_mDF18708BD88BB608A70BF3CD22B86CF4BC0DF817_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * G_B8_0 = NULL;
	List_1_t704724B962CF6005382C610663175BE693752D5D * G_B8_1 = NULL;
	Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * G_B7_0 = NULL;
	List_1_t704724B962CF6005382C610663175BE693752D5D * G_B7_1 = NULL;
	Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * G_B13_0 = NULL;
	List_1_t704724B962CF6005382C610663175BE693752D5D * G_B13_1 = NULL;
	Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * G_B12_0 = NULL;
	List_1_t704724B962CF6005382C610663175BE693752D5D * G_B12_1 = NULL;
	Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * G_B17_0 = NULL;
	List_1_t704724B962CF6005382C610663175BE693752D5D * G_B17_1 = NULL;
	Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * G_B16_0 = NULL;
	List_1_t704724B962CF6005382C610663175BE693752D5D * G_B16_1 = NULL;
	int32_t G_B22_0 = 0;
	{
		// switch (GameManager.state)
		int32_t L_0 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get_state_5();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0256;
			}
			case 1:
			{
				goto IL_0031;
			}
			case 2:
			{
				goto IL_0086;
			}
			case 3:
			{
				goto IL_0256;
			}
			case 4:
			{
				goto IL_0256;
			}
			case 5:
			{
				goto IL_00ae;
			}
			case 6:
			{
				goto IL_00d9;
			}
			case 7:
			{
				goto IL_0131;
			}
			case 8:
			{
				goto IL_0230;
			}
		}
	}
	{
		return;
	}

IL_0031:
	{
		// CueBall.Rigidbody.velocity = Vector3.zero;
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_2 = __this->get_CueBall_9();
		NullCheck(L_2);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_3 = L_2->get_Rigidbody_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_il2cpp_TypeInfo_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		NullCheck(L_3);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_3, L_4, /*hidden argument*/NULL);
		// CueBall.Rigidbody.angularVelocity = Vector3.zero;
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_5 = __this->get_CueBall_9();
		NullCheck(L_5);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_6 = L_5->get_Rigidbody_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		NullCheck(L_6);
		Rigidbody_set_angularVelocity_m3A40B7F195E9E217AE29A0964D7E7540E2E23080(L_6, L_7, /*hidden argument*/NULL);
		// CueBall.transform.position = new Vector3(0, 0.5f, -7);
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_8 = __this->get_CueBall_9();
		NullCheck(L_8);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_8, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_10), (0.0f), (0.5f), (-7.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_9, L_10, /*hidden argument*/NULL);
		// GameManager.state = GameState.NeedChange;
		((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->set_state_5(2);
		// break;
		return;
	}

IL_0086:
	{
		// cueMesh.enabled = true;
		MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * L_11 = __this->get_cueMesh_8();
		NullCheck(L_11);
		Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B(L_11, (bool)1, /*hidden argument*/NULL);
		// phantom.gameObject.SetActive(true);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12 = __this->get_phantom_10();
		NullCheck(L_12);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_13 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_13, (bool)1, /*hidden argument*/NULL);
		// GameManager.Instance().ChangePlayer();
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_14 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_14);
		GameManager_ChangePlayer_m0F30608B17D39CD119A0AC533BF3064E24D79BEF(L_14, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_00ae:
	{
		// canContinue = false;
		__this->set_canContinue_14((bool)0);
		// cueMesh.enabled = false;
		MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * L_15 = __this->get_cueMesh_8();
		NullCheck(L_15);
		Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B(L_15, (bool)0, /*hidden argument*/NULL);
		// phantom.gameObject.SetActive(false);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16 = __this->get_phantom_10();
		NullCheck(L_16);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_17, (bool)0, /*hidden argument*/NULL);
		// GameManager.state = GameState.Moving;
		((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->set_state_5(6);
		// break;
		return;
	}

IL_00d9:
	{
		// if (CueBall.Rigidbody.velocity.magnitude < 0.1f && Balls.All(b => b.Rigidbody.velocity.magnitude < 0.1f))
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_18 = __this->get_CueBall_9();
		NullCheck(L_18);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_19 = L_18->get_Rigidbody_5();
		NullCheck(L_19);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = Rigidbody_get_velocity_mCFB033F3BD14C2BA68E797DFA4950F9307EC8E2C(L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		float L_21 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_1), /*hidden argument*/NULL);
		if ((!(((float)L_21) < ((float)(0.100000001f)))))
		{
			goto IL_0256;
		}
	}
	{
		List_1_t704724B962CF6005382C610663175BE693752D5D * L_22 = __this->get_Balls_11();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_il2cpp_TypeInfo_var);
		Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * L_23 = ((U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_il2cpp_TypeInfo_var))->get_U3CU3E9__15_0_1();
		Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * L_24 = L_23;
		G_B7_0 = L_24;
		G_B7_1 = L_22;
		if (L_24)
		{
			G_B8_0 = L_24;
			G_B8_1 = L_22;
			goto IL_0120;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_il2cpp_TypeInfo_var);
		U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B * L_25 = ((U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * L_26 = (Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 *)il2cpp_codegen_object_new(Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44_il2cpp_TypeInfo_var);
		Func_2__ctor_m7141D4C33E399BD20EB8B17781BACB3B85C669C6(L_26, L_25, (intptr_t)((intptr_t)U3CU3Ec_U3CCheckStateU3Eb__15_0_m21D38CEC1B329681D58C6BE3A0472190A631BD12_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m7141D4C33E399BD20EB8B17781BACB3B85C669C6_RuntimeMethod_var);
		Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * L_27 = L_26;
		((U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_il2cpp_TypeInfo_var))->set_U3CU3E9__15_0_1(L_27);
		G_B8_0 = L_27;
		G_B8_1 = G_B7_1;
	}

IL_0120:
	{
		bool L_28 = Enumerable_All_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_mD11F2F4E2660C09A1E8513BC3A3B926D37D22996(G_B8_1, G_B8_0, /*hidden argument*/Enumerable_All_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_mD11F2F4E2660C09A1E8513BC3A3B926D37D22996_RuntimeMethod_var);
		if (!L_28)
		{
			goto IL_0256;
		}
	}
	{
		// GameManager.state = GameState.MoveEnd;
		((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->set_state_5(7);
		// break;
		return;
	}

IL_0131:
	{
		// if (
		//     GameManager.Instance().currentPlayer.ballsType == BallsType.Solids && !Balls.Any(b => b.Number < 8) ||
		//     GameManager.Instance().currentPlayer.ballsType == BallsType.Stripes && !Balls.Any(b => b.Number > 8)
		//     )
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_29 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_29);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_30 = L_29->get_currentPlayer_26();
		NullCheck(L_30);
		int32_t L_31 = L_30->get_ballsType_2();
		if (L_31)
		{
			goto IL_016e;
		}
	}
	{
		List_1_t704724B962CF6005382C610663175BE693752D5D * L_32 = __this->get_Balls_11();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_il2cpp_TypeInfo_var);
		Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * L_33 = ((U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_il2cpp_TypeInfo_var))->get_U3CU3E9__15_1_2();
		Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * L_34 = L_33;
		G_B12_0 = L_34;
		G_B12_1 = L_32;
		if (L_34)
		{
			G_B13_0 = L_34;
			G_B13_1 = L_32;
			goto IL_0167;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_il2cpp_TypeInfo_var);
		U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B * L_35 = ((U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * L_36 = (Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 *)il2cpp_codegen_object_new(Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44_il2cpp_TypeInfo_var);
		Func_2__ctor_m7141D4C33E399BD20EB8B17781BACB3B85C669C6(L_36, L_35, (intptr_t)((intptr_t)U3CU3Ec_U3CCheckStateU3Eb__15_1_m675B68772F4C3E060417EEA5746E1FEF51D99786_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m7141D4C33E399BD20EB8B17781BACB3B85C669C6_RuntimeMethod_var);
		Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * L_37 = L_36;
		((U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_il2cpp_TypeInfo_var))->set_U3CU3E9__15_1_2(L_37);
		G_B13_0 = L_37;
		G_B13_1 = G_B12_1;
	}

IL_0167:
	{
		bool L_38 = Enumerable_Any_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_mB31A84E03C801D4A081D5A31CB5197BE99EE5B75(G_B13_1, G_B13_0, /*hidden argument*/Enumerable_Any_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_mB31A84E03C801D4A081D5A31CB5197BE99EE5B75_RuntimeMethod_var);
		if (!L_38)
		{
			goto IL_01ac;
		}
	}

IL_016e:
	{
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_39 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_39);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_40 = L_39->get_currentPlayer_26();
		NullCheck(L_40);
		int32_t L_41 = L_40->get_ballsType_2();
		if ((!(((uint32_t)L_41) == ((uint32_t)1))))
		{
			goto IL_01b7;
		}
	}
	{
		List_1_t704724B962CF6005382C610663175BE693752D5D * L_42 = __this->get_Balls_11();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_il2cpp_TypeInfo_var);
		Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * L_43 = ((U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_il2cpp_TypeInfo_var))->get_U3CU3E9__15_2_3();
		Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * L_44 = L_43;
		G_B16_0 = L_44;
		G_B16_1 = L_42;
		if (L_44)
		{
			G_B17_0 = L_44;
			G_B17_1 = L_42;
			goto IL_01a5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_il2cpp_TypeInfo_var);
		U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B * L_45 = ((U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * L_46 = (Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 *)il2cpp_codegen_object_new(Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44_il2cpp_TypeInfo_var);
		Func_2__ctor_m7141D4C33E399BD20EB8B17781BACB3B85C669C6(L_46, L_45, (intptr_t)((intptr_t)U3CU3Ec_U3CCheckStateU3Eb__15_2_m92301D8863096DB4E7559DA3BE989C24B09E6463_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m7141D4C33E399BD20EB8B17781BACB3B85C669C6_RuntimeMethod_var);
		Func_2_t9D883DAE82460316314F5432F321CE8C085C4B44 * L_47 = L_46;
		((U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_il2cpp_TypeInfo_var))->set_U3CU3E9__15_2_3(L_47);
		G_B17_0 = L_47;
		G_B17_1 = G_B16_1;
	}

IL_01a5:
	{
		bool L_48 = Enumerable_Any_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_mB31A84E03C801D4A081D5A31CB5197BE99EE5B75(G_B17_1, G_B17_0, /*hidden argument*/Enumerable_Any_TisBall_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D_mB31A84E03C801D4A081D5A31CB5197BE99EE5B75_RuntimeMethod_var);
		if (L_48)
		{
			goto IL_01b7;
		}
	}

IL_01ac:
	{
		// GameManager.Instance().RegisterBall(8);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_49 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_49);
		GameManager_RegisterBall_m33C55F5DAF74AF3A685FC4EC267C3CC4280A5F74(L_49, 8, /*hidden argument*/NULL);
	}

IL_01b7:
	{
		// cueMesh.enabled = true;
		MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B * L_50 = __this->get_cueMesh_8();
		NullCheck(L_50);
		Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B(L_50, (bool)1, /*hidden argument*/NULL);
		// phantom.gameObject.SetActive(true);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_51 = __this->get_phantom_10();
		NullCheck(L_51);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_52 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_51, /*hidden argument*/NULL);
		NullCheck(L_52);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_52, (bool)1, /*hidden argument*/NULL);
		// GameManager.state = canContinue ? GameState.Aim : GameState.NeedChange;
		bool L_53 = __this->get_canContinue_14();
		if (L_53)
		{
			goto IL_01df;
		}
	}
	{
		G_B22_0 = 2;
		goto IL_01e0;
	}

IL_01df:
	{
		G_B22_0 = 3;
	}

IL_01e0:
	{
		((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->set_state_5(G_B22_0);
		// GameManager.Clock = 30 * (1 + 0.02f * GameManager.Instance().currentPlayer.data.Time);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_54 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_54);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_55 = L_54->get_currentPlayer_26();
		NullCheck(L_55);
		PlayerData_t8CF0E5D9EE572FFF7E0B41DA391376CF0AB8EBB5 * L_56 = L_55->get_data_0();
		NullCheck(L_56);
		int32_t L_57 = L_56->get_Time_9();
		GameManager_set_Clock_m6A52D50A47B356DE092A46E16D85258DAA4338C2(((float)il2cpp_codegen_multiply((float)(30.0f), (float)((float)il2cpp_codegen_add((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)(0.0199999996f), (float)(((float)((float)L_57))))))))), /*hidden argument*/NULL);
		// GameManager.Instance().currentPlayer.CueMove(GameManager.Instance().slider.value);
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_58 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_58);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_59 = L_58->get_currentPlayer_26();
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_60 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_60);
		Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * L_61 = L_60->get_slider_14();
		NullCheck(L_61);
		float L_62 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_61);
		NullCheck(L_59);
		Player_CueMove_m99C0CC666F9E6AF125FC0866717BE649BA192961(L_59, L_62, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_0230:
	{
		// if (GameManager.Instance().currentPlayer.ballsType != BallsType.Eight)
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_63 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_63);
		Player_t5689617909B48F7640EA0892D85C92C13CC22C6F * L_64 = L_63->get_currentPlayer_26();
		NullCheck(L_64);
		int32_t L_65 = L_64->get_ballsType_2();
		if ((((int32_t)L_65) == ((int32_t)2)))
		{
			goto IL_024c;
		}
	}
	{
		// GameManager.Instance().ChangePlayer();
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_66 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_66);
		GameManager_ChangePlayer_m0F30608B17D39CD119A0AC533BF3064E24D79BEF(L_66, /*hidden argument*/NULL);
	}

IL_024c:
	{
		// GameManager.Instance().EndGame();
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_67 = GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline(/*hidden argument*/NULL);
		NullCheck(L_67);
		GameManager_EndGame_m34CB0E063C72D2D7BA815B5397C5DB865EE60810(L_67, /*hidden argument*/NULL);
	}

IL_0256:
	{
		// }
		return;
	}
}
// System.Void Table::RemoveBall(Ball)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Table_RemoveBall_m02A9A5675D99552809A1869FF89AFE3E0FC9C6DE (Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * __this, Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * ___ball0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Table_RemoveBall_m02A9A5675D99552809A1869FF89AFE3E0FC9C6DE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// Balls.Remove(ball);
		List_1_t704724B962CF6005382C610663175BE693752D5D * L_0 = __this->get_Balls_11();
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_1 = ___ball0;
		NullCheck(L_0);
		List_1_Remove_mE5E9D8A568D8853924165476644A9B523F98B554(L_0, L_1, /*hidden argument*/List_1_Remove_mE5E9D8A568D8853924165476644A9B523F98B554_RuntimeMethod_var);
		// foreach (GameObject icon in ball.icons)
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_2 = ___ball0;
		NullCheck(L_2);
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_3 = L_2->get_icons_6();
		NullCheck(L_3);
		Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  L_4 = List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6(L_3, /*hidden argument*/List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6_RuntimeMethod_var);
		V_0 = L_4;
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0027;
		}

IL_001b:
		{
			// foreach (GameObject icon in ball.icons)
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_inline((Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *)(&V_0), /*hidden argument*/Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_RuntimeMethod_var);
			// Destroy(icon);
			IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
			Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_5, /*hidden argument*/NULL);
		}

IL_0027:
		{
			// foreach (GameObject icon in ball.icons)
			bool L_6 = Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7((Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7_RuntimeMethod_var);
			if (L_6)
			{
				goto IL_001b;
			}
		}

IL_0030:
		{
			IL2CPP_LEAVE(0x40, FINALLY_0032);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0032;
	}

FINALLY_0032:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9((Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *)(&V_0), /*hidden argument*/Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9_RuntimeMethod_var);
		IL2CPP_END_FINALLY(50)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(50)
	{
		IL2CPP_JUMP_TBL(0x40, IL_0040)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0040:
	{
		// }
		return;
	}
}
// System.Void Table::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Table__ctor_mFF1E270F8889171380AB2858EF750107E27034E2 (Table_t9A8C2E537C6C806DC656ADE42A12D58BA7009152 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BotAI_<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m67E201652407DD4E9631314EF20EF9D730F0301D (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_m67E201652407DD4E9631314EF20EF9D730F0301D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD * L_0 = (U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD *)il2cpp_codegen_object_new(U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mE1CF8C72D5472ED37ED79206ECD6241080ECC695(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void BotAI_<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mE1CF8C72D5472ED37ED79206ECD6241080ECC695 (U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean BotAI_<>c::<CheckBalls>b__8_0(Ball)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CCheckBallsU3Eb__8_0_mFB3E009B3F91CC5FF9957507C4FF2A7A82EC8953 (U3CU3Ec_tD8D04D5F3B55A8830447D96241D47365D540ADBD * __this, Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * ___b0, const RuntimeMethod* method)
{
	{
		// balls = GameManager.Instance().currentTable.Balls.Where(b => b.Number != 8).ToList();
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_0 = ___b0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_Number_4();
		return (bool)((((int32_t)((((int32_t)L_1) == ((int32_t)8))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameManager_<>c__DisplayClass30_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass30_0__ctor_m475CBDDDBF19B84D9ADCAA4A4DFE559EF79DD2CE (U3CU3Ec__DisplayClass30_0_t9F9560A6034B2661D9C6E093FA21372992814025 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GameManager_<>c__DisplayClass30_0::<RegisterBall>b__0(Ball)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass30_0_U3CRegisterBallU3Eb__0_mFD130C2AF37A75EA852CB27234BAE17B629F60C9 (U3CU3Ec__DisplayClass30_0_t9F9560A6034B2661D9C6E093FA21372992814025 * __this, Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * ___b0, const RuntimeMethod* method)
{
	{
		// currentTable.Balls.First(b => b.Number == i).icons.Add(img.gameObject);
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_0 = ___b0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_Number_4();
		int32_t L_2 = __this->get_i_0();
		return (bool)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Table_<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mEE715A42F22ECC72A35F83EB838087ABC851CE7A (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_mEE715A42F22ECC72A35F83EB838087ABC851CE7A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B * L_0 = (U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B *)il2cpp_codegen_object_new(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m857A70135E5C6368471096430F715C195059B579(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Table_<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m857A70135E5C6368471096430F715C195059B579 (U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Table_<>c::<CheckState>b__15_0(Ball)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CCheckStateU3Eb__15_0_m21D38CEC1B329681D58C6BE3A0472190A631BD12 (U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B * __this, Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * ___b0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (CueBall.Rigidbody.velocity.magnitude < 0.1f && Balls.All(b => b.Rigidbody.velocity.magnitude < 0.1f))
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_0 = ___b0;
		NullCheck(L_0);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_1 = L_0->get_Rigidbody_5();
		NullCheck(L_1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = Rigidbody_get_velocity_mCFB033F3BD14C2BA68E797DFA4950F9307EC8E2C(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		return (bool)((((float)L_3) < ((float)(0.100000001f)))? 1 : 0);
	}
}
// System.Boolean Table_<>c::<CheckState>b__15_1(Ball)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CCheckStateU3Eb__15_1_m675B68772F4C3E060417EEA5746E1FEF51D99786 (U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B * __this, Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * ___b0, const RuntimeMethod* method)
{
	{
		// GameManager.Instance().currentPlayer.ballsType == BallsType.Solids && !Balls.Any(b => b.Number < 8) ||
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_0 = ___b0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_Number_4();
		return (bool)((((int32_t)L_1) < ((int32_t)8))? 1 : 0);
	}
}
// System.Boolean Table_<>c::<CheckState>b__15_2(Ball)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CCheckStateU3Eb__15_2_m92301D8863096DB4E7559DA3BE989C24B09E6463 (U3CU3Ec_t74E0BBFDF0C4E37D4EDF62581064B1F01EE9C45B * __this, Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * ___b0, const RuntimeMethod* method)
{
	{
		// GameManager.Instance().currentPlayer.ballsType == BallsType.Stripes && !Balls.Any(b => b.Number > 8)
		Ball_tB960B03B3710B825AA3B6862E08BB100AC9ECE0D * L_0 = ___b0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_Number_4();
		return (bool)((((int32_t)L_1) > ((int32_t)8))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_Instance_m89852A2ABED14DAA177B7A2C45DB5063D3B99965AssemblyU2DCSharp_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static GameManager Instance() { return _instance; }
		GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1 * L_0 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get__instance_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_UnaryNegation_m362EA356F4CADEDB39F965A0DBDED6EA890925F7_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___a0;
		float L_3 = L_2.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_6), ((-L_1)), ((-L_3)), ((-L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_001e;
	}

IL_001e:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * Slider_get_onValueChanged_m7F480C569A6D668952BE1436691850D13825E129_inline (Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * __this, const RuntimeMethod* method)
{
	{
		// public SliderEvent onValueChanged { get { return m_OnValueChanged; } set { m_OnValueChanged = value; } }
		SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * L_0 = __this->get_m_OnValueChanged_26();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___v0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___v0;
		float L_3 = L_2.get_y_3();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_4), L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Subtraction_m6E536A8C72FEAA37FF8D5E26E11D6E71EB59599A_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___b1, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___a0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___b1;
		float L_3 = L_2.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = ___a0;
		float L_5 = L_4.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6 = ___b1;
		float L_7 = L_6.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_8), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0023;
	}

IL_0023:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4_inline (Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * __this, const RuntimeMethod* method)
{
	{
		// get { return m_OnClick; }
		ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * L_0 = __this->get_m_OnClick_19();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline (float ___d0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a1;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a1;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a1;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float GameManager_get_Clock_m32723273A7CCC5919901AC0AA016C8F90E90A5D6_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_get_Clock_m32723273A7CCC5919901AC0AA016C8F90E90A5D6AssemblyU2DCSharp_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return _timer; }
		float L_0 = ((GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_StaticFields*)il2cpp_codegen_static_fields_for(GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_il2cpp_TypeInfo_var))->get__timer_6();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m320FF0DD39F83A684F9E277C6A0D07BC3CEDA7D9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  List_1_get_Item_m554804AC447B0BA1F93C64E863FF7DB0527AFED9_gshared_inline (List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_2 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)__this->get__items_1();
		int32_t L_3 = ___index0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)L_2, (int32_t)L_3);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Nullable_1_GetValueOrDefault_mA8DDAB2C6553ED7FFC9A55E1A92A96B3571000EC_gshared_inline (Nullable_1_t64244F99361E39CBE565C5E89436C898F18DF5DC * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m4C033F49F5318E94BC8CBA9CE5175EFDBFADEF9C_gshared_inline (Nullable_1_t64244F99361E39CBE565C5E89436C898F18DF5DC * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
